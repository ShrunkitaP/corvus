//
//  DatabaseHandler.swift
//  CORVUS
//
//  Created by HPL on 06/09/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit
import CoreData
class DatabaseHandler: NSObject
{
   static let sharedInstance = DatabaseHandler()
    
    
     func managedObjectContext() -> NSManagedObjectContext
     {
        var context: NSManagedObjectContext? = nil
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if (delegate.performSelector(#selector(self.managedObjectContext)) != nil) {
            context = delegate.managedObjectContext
        }
        return context!
    }
    
    
    
    
    func saveUserData(f_name:String,l_name:String,u_age:String,u_height:String,u_weight:String,u_EyeColor:String,u_hairColor:String,u_score:String,u_birthPlace:String,u_citizenship:String,u_sex:String,u_enrollDt:String,u_newImage:String,u_oldImage:String,u_googleMap:String,u_race:String,u_dob:String,latt:Double,long:Double,lattSrc:Double,longSrc:Double,searchDate:String,sign:String) -> Void
    {
        let manage_context: NSManagedObjectContext = self.managedObjectContext()
        // Create a new managed object
        
         let obj_userData = NSEntityDescription.insertNewObjectForEntityForName("UserData", inManagedObjectContext: manage_context)
        
        obj_userData.setValue(String(obj_userData.objectID), forKey: "obj_Id")
        obj_userData.setValue(f_name, forKey: "firstName")
        obj_userData.setValue(l_name, forKey: "lastName")
        obj_userData.setValue(u_age, forKey: "age")
        obj_userData.setValue(u_height, forKey: "height")
        obj_userData.setValue(u_weight, forKey: "weight")
        obj_userData.setValue(u_EyeColor, forKey: "eye_color")
        obj_userData.setValue(u_hairColor, forKey: "hair_color")
        obj_userData.setValue(u_score, forKey: "score")
        obj_userData.setValue(u_sex, forKey: "sex")
        obj_userData.setValue(u_enrollDt, forKey: "enroll_date")
        obj_userData.setValue(u_birthPlace, forKey: "birth_palce")
        obj_userData.setValue(u_citizenship, forKey: "citizenship")
        obj_userData.setValue(u_newImage, forKey: "newImage")
        obj_userData.setValue(u_oldImage, forKey: "oldImage")
        obj_userData.setValue(u_googleMap, forKey: "google_map")
        obj_userData.setValue(u_race, forKey: "race")
        obj_userData.setValue(u_dob, forKey: "dob")
        obj_userData.setValue(latt, forKey: "lattitude")
        obj_userData.setValue(long, forKey: "longitude")
        obj_userData.setValue(lattSrc, forKey: "lattitudeSearch")
        obj_userData.setValue(longSrc, forKey: "longitudeSearch")
        obj_userData.setValue(searchDate, forKey: "search_date")
        obj_userData.setValue(sign, forKey: "signature")
        do {
            try manage_context.save()
            
          // //print(self.fetchAllRecord())
        } catch {
           // //print(error)
        }
    }
    func fetchAllRecord() -> NSArray
    {
        var userData:NSArray = NSArray ()
        let fetchRequest = NSFetchRequest(entityName: "UserData")
        do {
            userData = try managedObjectContext().executeFetchRequest(fetchRequest)
          //  //print(userData)
            
        }
        catch let error {
            ////print(error)
        }
        return userData
    }
    
    
    func deleteUserData(object_id: NSString) -> Bool
    {
        let manage_context: NSManagedObjectContext = self.managedObjectContext()
        let fetchRequest = NSFetchRequest(entityName: "UserData")
        fetchRequest.predicate = NSPredicate(format: "(obj_Id = %@)", object_id)
        fetchRequest.returnsObjectsAsFaults = false
        do{
            let fetchResults = try self.managedObjectContext().executeFetchRequest(fetchRequest)
            if fetchResults.count>0
            {
                self.managedObjectContext().deleteObject(fetchResults[0] as! NSManagedObject)
            }
            else
            {
                // no data
            }
            do {
                try manage_context.save()
            } catch {
              //  //print(error)
            }

            return true
        }
        catch
        {
            return false
        }
    }
}
