//
//  signatureViewController.swift
//  CORVUS
//
//  Created by Riken Shah on 09/09/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit
import ACEDrawingView
import Alamofire

class signatureViewController: UIViewController, ACEDrawingViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var crossButon: UIButton!
    @IBOutlet weak var fourBox: UIButton!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var viewTopContainer: UIView!
    @IBOutlet weak var drawingView: ACEDrawingView!
    var picker:UIImagePickerController?=UIImagePickerController()
    var popup : KLCPopup?
    var imageSelected = UIImage()
    var signImage = UIImage()
    let obj_WebService = WebService()
    var img_strSg : String = String()
    var sign_data : String = String()
    var LongitudeDegreeSg = Int()
    var LatitudeDegreeSg = Int()
    var LongitudeMinuteSg = Int()
    var LatitudeMinuteSg = Int()
    var LongitudeSecondSg = Int()
    var LatitudeSecondSg = Int()
    var nameOfPersonSg : String = String()
    var sirnameOfPersonSg : String = String()
    var genderOfPersonSg : String = String()
    var heightOfPersonSg : String = String()
    var weightOfPersonSg : String = String()
    var dobOfPersonSg : String = String()
    var eyeOfPersonSg : String = String()
    var hairOfPersonSg : String = String()
    var countryOfPersonSg : String = String()
    var citizenshipOfPersonSg : String = String()
    var raceOfPersonSg : String = String()
    var appdelegate: AppDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationItem.hidesBackButton = true
        //let newBackButton = UIBarButtonItem(title: "< Back", style: UIBarButtonItemStyle.Bordered, target: self, action: "back:")
        //self.navigationItem.leftBarButtonItem = newBackButton;
        self.navigationController?.navigationBarHidden = true
        self.drawingView.delegate = self
        //self.drawingView.layer.borderWidth = 2
        //drawingView.layer.masksToBounds = false
        appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        
        viewTopContainer.layer.shadowColor = UIColor.blackColor().CGColor
        viewTopContainer.hidden = true
        lineView.hidden = true
        picker?.delegate=self
    }

    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
            let value = UIInterfaceOrientation.LandscapeLeft.rawValue
            UIDevice.currentDevice().setValue(value, forKey: "orientation")
    }
//    override func viewDidAppear(animated: Bool) {
//        super.viewDidAppear(true)
//        let value = UIInterfaceOrientation.LandscapeLeft.rawValue
//        UIDevice.currentDevice().setValue(value, forKey: "orientation")
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    @IBAction func cancelBtn(sender: AnyObject) {
        UIApplication.sharedApplication().delegate as! AppDelegate
        appdelegate!.shouldSupportAllOrientation = false
       self.navigationController?.popViewControllerAnimated(true)
        
    }
    @IBAction func showTopView(sender: AnyObject) {
        viewTopContainer.hidden = false
        lineView.hidden = false
        fourBox.hidden = true
    }
    @IBAction func crossButton(sender: AnyObject) {
        viewTopContainer.hidden = true
        lineView.hidden = true
        fourBox.hidden = false
    }

    @IBAction func clear(sender: AnyObject) {
       self.drawingView.clear()
    }
  
    func recheablityCheck()-> Bool
    {
        let net = NetworkReachabilityManager()
        net?.startListening()
        
        if  net?.isReachable ?? false
        {
            
            if ((net?.isReachableOnEthernetOrWiFi) != nil)
            {
                return true
            }
            else if(net?.isReachableOnWWAN)!
            {
                return true
            }
        }
        else
        {
            //// print("no connection")
            return false
        }
        return false
    }

    @IBAction func enrollWithSignature(sender: AnyObject)
    {
        if recheablityCheck() == true
        {
           enrollSignWithNoErrors()
        }
        else
        {
            let myAlert = UIAlertController(title: "Internet connection FAILED", message: "Your device is not connected to internet", preferredStyle: UIAlertControllerStyle.Alert);
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil);
            myAlert.addAction(okAction)
            self.presentViewController(myAlert, animated: true, completion: nil)
        }
    }
    @IBAction func homeButton(sender: AnyObject) {
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appdelegate.shouldSupportAllOrientation = false
        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("homeScreenViewController") as! homeScreenViewController
        self.navigationController!.pushViewController(destination, animated: true)

    }
    @IBAction func searchButton(sender: AnyObject) {
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appdelegate.shouldSupportAllOrientation = false
        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("searchViewController") as! searchViewController
        self.navigationController!.pushViewController(destination, animated: true)

    }
    @IBAction func navigateToEnrollScreen(sender: AnyObject) {
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appdelegate.shouldSupportAllOrientation = false
        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("EnrollViewController") as! EnrollViewController
        self.navigationController!.pushViewController(destination, animated: true)
    }
    
    func enrollSignWithNoErrors(){
        appdelegate!.showHUD(self.view)
        signImage = self.drawingView.image
        let dispatchQueue : dispatch_queue_t = dispatch_queue_create("dispatchQueue", nil)
        dispatch_async(dispatchQueue) {
            self.encodeToBase64String(self.signImage) { (result) in
                self.sign_data = result!
                self.enrollUserWithSignature()
        }
    }
    }
    func enrollUserWithSignature(){
        
        var LatitudeDeg : String = String()
        var LatitudeMin : String = String()
        var LatitudeSec : String = String()
        var LongitudeDeg : String = String()
        var LongitudeMin : String = String()
        var LongitudeSec : String = String()
        if(LatitudeDegreeSg == 0 && LatitudeMinuteSg == 0 && LatitudeSecondSg == 0 && LongitudeDegreeSg == 0 && LongitudeMinuteSg == 0 && LongitudeSecondSg == 0){
            LatitudeDeg = "?"
            LatitudeMin = "?"
            LatitudeSec = "?"
            LongitudeDeg = "?"
            LongitudeMin = "?"
            LongitudeSec = "?"
        }else{
            LatitudeDeg = String(LatitudeDegreeSg)
            LatitudeMin = String(LatitudeMinuteSg)
            LatitudeSec = String(LatitudeSecondSg)
            LongitudeDeg = String(LongitudeDegreeSg)
            LongitudeMin = String(LongitudeMinuteSg)
            LongitudeSec = String(LongitudeSecondSg)
        }

        obj_WebService.sendResponseWithSign(img_strSg,sign_str:sign_data,firstName:nameOfPersonSg,lastName:sirnameOfPersonSg,dob:dobOfPersonSg,pob:countryOfPersonSg,citizenship:citizenshipOfPersonSg,gender:genderOfPersonSg,race:raceOfPersonSg,height:heightOfPersonSg,weight:weightOfPersonSg,eye:eyeOfPersonSg,hair:hairOfPersonSg,LatitudeDegree:LatitudeDeg,LatitudeMinute:LatitudeMin,LatitudeSecond:LatitudeSec,LongitudeDegree:LongitudeDeg,LongitudeMinute:LongitudeMin,LongitudeSecond:LongitudeSec){ (block) in
            //print(block);
            self.appdelegate?.dismissView(self.view)
            if(block == nil){
                let Cust = CustomClass()
                var alert = UIAlertController()
                alert = Cust.displayMyAlertMessage("Not able to connect to Server")
                self.presentViewController(alert, animated: true, completion: nil)
                self.appdelegate?.dismissView(self.view)
            }
            else
            {

            let success = block!.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessEnrollResponse")?.objectForKey("ProcessEnrollResult")?.objectForKey("Code")?.objectForKey("text") as? String
            if(success == "enroll.success"){
                let alertController = UIAlertController(title: "Success", message: "Enroll request successfully processed", preferredStyle: UIAlertControllerStyle.Alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
                    self.appdelegate!.shouldSupportAllOrientation = false
                    let destination = self.storyboard!.instantiateViewControllerWithIdentifier("searchViewController") as! searchViewController
                    self.navigationController!.pushViewController(destination, animated: true)
                })
                alertController.addAction(okAction)
                self.presentViewController(alertController, animated: true, completion:{})
            }
            else{
                let alertController = UIAlertController(title: "Fail", message: "Try again with another image?", preferredStyle: UIAlertControllerStyle.Alert)
                
                let somethingAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in //print("Yes")
                    let alertview = NSBundle.mainBundle().loadNibNamed("ChooseImageView", owner: self, options: nil)[0]
                    self.popup = KLCPopup(contentView: alertview as! UIView, showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: false, dismissOnContentTouch: false)
                    let camera = (alertview.viewWithTag(1)! as! UIButton)
                    camera.addTarget(self, action: #selector(self.cameraClicked(_:)), forControlEvents: .TouchUpInside)
                    let gallary = (alertview.viewWithTag(2)! as! UIButton)
                    gallary.addTarget(self, action: #selector(self.gallaryClicked(_:)), forControlEvents: .TouchUpInside)
                    let cancelImgSelection = (alertview.viewWithTag(3)! as! UIButton)
                    cancelImgSelection.addTarget(self, action: #selector(self.cancelImgSelectionClicked(_:)), forControlEvents: .TouchUpInside)
                    self.popup!.show()
                    
                })
                
                let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in //print("No")
                    let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appdelegate.shouldSupportAllOrientation = false
                    self.navigationController?.popViewControllerAnimated(true)
                })
                
                alertController.addAction(somethingAction)
                alertController.addAction(cancelAction)
                self.presentViewController(alertController, animated: true, completion:{})
                
            }
        }

    }
}
    
    func cancelImgSelectionClicked(sender:AnyObject){
        popup?.dismiss(true)
    }
    // MARK: - gallary button
    func gallaryClicked(sender:AnyObject){
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appdelegate.shouldSupportAllOrientation = false
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        presentViewController(picker!, animated: true, completion: nil)
        popup?.dismiss(true)
    }
    // MARK: - camera button
    func cameraClicked(sender:AnyObject){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appdelegate.shouldSupportAllOrientation = false
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerControllerSourceType.Camera
            picker!.cameraCaptureMode = .Photo
            presentViewController(picker!, animated: true, completion: nil)
            popup?.dismiss(true)
        }
        else
        {
            popup?.dismiss(true)
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .Alert)
            let ok = UIAlertAction(title: "OK", style:.Default, handler: nil)
            alert.addAction(ok)
            presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appdelegate.shouldSupportAllOrientation = true
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        var chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        //imageSelected = chosenImage
        chosenImage = fixOrientation(chosenImage)
        let dataComp = UIImageJPEGRepresentation(chosenImage, 1)
        let imageSize: Int = dataComp!.length
        let img = Double(imageSize)/1024.0
        //print(img)
        img_strSg = dataComp!.base64EncodedStringWithOptions([])
        imageSelected = UIImage(data: dataComp!)!
        if(img > 500){
            let dataComp = UIImageJPEGRepresentation(chosenImage, 0.5)
            img_strSg = dataComp!.base64EncodedStringWithOptions([])            //let imageSize1: Int = dataComp1!.length
            //let img1 = Double(imageSize1)
            //print(img1 / 1024.0)
            imageSelected = UIImage(data: dataComp!)!
        }
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appdelegate.shouldSupportAllOrientation = true
        dismissViewControllerAnimated(true, completion:
            {
                //self.img_strSg = self.encodeToBase64String(self.imageSelected)
                self.enrollSignWithNoErrors()
            }
        )
        
    }

    func fixOrientation(img:UIImage) -> UIImage {
        if (img.imageOrientation == UIImageOrientation.Up) {
            return img;
        }
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale);
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.drawInRect(rect)
        let normalizedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return normalizedImage;
    }

    func encodeToBase64String(image: UIImage,block:CompletionBlock) -> Void
    {
        let imageData = UIImagePNGRepresentation(image)
        let imag_str:String = imageData!.base64EncodedStringWithOptions([])
        return block(result: imag_str)
    }

    func encodeToBase64String(image: UIImage) -> String {
        
        let imageData = UIImagePNGRepresentation(image)
        let imag_str:String = imageData!.base64EncodedStringWithOptions([])
        return imag_str
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
