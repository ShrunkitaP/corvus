//
//  UserData+CoreDataProperties.m
//  CORVUS
//
//  Created by Riken Shah on 13/09/16.
//  Copyright © 2016 HPL. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "UserData+CoreDataProperties.h"

@implementation UserData (CoreDataProperties)

@dynamic age;
@dynamic birth_palce;
@dynamic citizenship;
@dynamic firstName;
@dynamic height;
@dynamic lastName;
@dynamic newImage;
@dynamic oldImage;
@dynamic score;
@dynamic weight;
@dynamic obj_Id;

@end
