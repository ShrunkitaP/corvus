//
//  EnrollPhotoViewController.swift
//  CORVUS
//
//  Created by Riken Shah on 30/08/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
typealias CompletionBlock = (result: String?) -> Void

class EnrollPhotoViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var MyImageToBeDisp: UIImageView!
     var locationManager = CLLocationManager()
    var lattitude : Double? = nil
    var longitude : Double? = nil
    var LongitudeDegree = Int()
    var LatitudeDegree = Int()
    var LongitudeMinute = Int()
    var LatitudeMinute = Int()
    var LongitudeSecond = Int()
    var LatitudeSecond = Int()
    var img_data : String = String()
    var appdelegate: AppDelegate?


    let obj_WebService = WebService()
    var imageToBeEnroll = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        MyImageToBeDisp.image = imageToBeEnroll
        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        //locationManager.startMonitoringSignificantLocationChanges()
        appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreavar.
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let userLocation:CLLocation = locations[0]
        longitude = userLocation.coordinate.longitude;
        lattitude = userLocation.coordinate.latitude;
        //print(longitude)
        //print(lattitude)
        
        self.locationManager.stopUpdatingLocation()
        let longMin1 = (findMinLattitude(longitude!))
        let lattMin1 = (findMinLattitude(lattitude!))
         LongitudeDegree = Int(longMin1)
         LatitudeDegree = Int(lattMin1)
        //print((LongitudeDegree))
        //print((LatitudeDegree))
        //let longitude1 = (longitude! - Double(longMin1))*60
        let longitude2 = findMinLattitude((longMin1 - Double(LongitudeDegree))*60)
        let longMin2 = (longitude2)
        LongitudeMinute = Int(longMin2)
        //print(LongitudeMinute)
        // let lattitude1 = (lattitude! - Double(LattMin1))*60
        let lattitude2 = findMinLattitude((lattMin1 - Double(LatitudeDegree))*60)
        let lattMin2 = (lattitude2)
        LatitudeMinute = Int(lattMin2)
        //print(LatitudeMinute)
        
        //for longMinInt 3
        let longitude3 = findMinLattitude((longMin2 - Double(LongitudeMinute))*60)
        let longMin3 = (longitude3)
        LongitudeSecond = Int(longMin3)
        //print(LongitudeSecond)
        
        // for latMinInt 3
        let lattitude3 = findMinLattitude((lattMin2 - Double(LatitudeMinute))*60)
        let lattMin3 = (lattitude3)
        LatitudeSecond = Int(lattMin3)
        //print(LatitudeSecond)

        locationManager.stopUpdatingLocation()
        //self.locationManager.stopMonitoringSignificantLocationChanges()
    }
    
    func findMinLattitude(lattitude : Double) -> Double
    {
        if(lattitude > 0)
        {
            return lattitude
        }
        else
        {
            let neg = lattitude + 360
            return neg
        }
    }
    @IBAction func homeButtonTapped(sender: AnyObject) {
        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("homeScreenViewController") as! homeScreenViewController
        self.navigationController!.pushViewController(destination, animated: true)
    }
    
    @IBAction func homeBtnLongPress(sender: UILongPressGestureRecognizer) {
        if (sender.state == .Began)
        {
            let destination = self.storyboard!.instantiateViewControllerWithIdentifier("openFileViewController") as! openFileViewController
            self.navigationController!.pushViewController(destination, animated: true)
        }
    }
    
    @IBAction func searchButtonTapped(sender: AnyObject)
    {
        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("searchViewController") as! searchViewController
        self.navigationController!.pushViewController(destination, animated: true)
    }
    
    @IBAction func cancelImage(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func biographicButtonTapped(sender: AnyObject)
    {
        //self.appdelegate!.showHUD(self.view)
     //   img_data = encodeToBase64String(imageToBeEnroll)
//        dispatch_async(dispatch_get_main_queue()) {
//             self.appdelegate!.showHUD(self.view)
//        }
//          self.encodeToBase64String(imageToBeEnroll) { (result) in
//            self.img_data = result!
//            dispatch_async(dispatch_get_main_queue()) {
//                self.appdelegate?.dismissView(self.view)
//            }
            self.performSegueWithIdentifier("EnrollDetails", sender: nil)
//        }
    }
    
    func recheablityCheck()-> Bool
    {
        let net = NetworkReachabilityManager()
        net?.startListening()
        
        if  net?.isReachable ?? false
        {
            
            if ((net?.isReachableOnEthernetOrWiFi) != nil)
            {
                return true
            }
            else if(net?.isReachableOnWWAN)!
            {
                return true
            }
        }
        else
        {
            //// print("no connection")
            return false
        }
        return false
    }

    
    @IBAction func enrollWithImage(sender: AnyObject)
    {
        if recheablityCheck() == true
        {
         self.enrollToService()
        }
        else
        {
            let myAlert = UIAlertController(title: "Internet connection FAILED", message: "Your device is not connected to internet", preferredStyle: UIAlertControllerStyle.Alert);
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil);
            myAlert.addAction(okAction)
            self.presentViewController(myAlert, animated: true, completion: nil)
        }
    }
    func enrollToService()
    {
         self.appdelegate!.showHUD(self.view)
        self.enrollUser()
//        let dispatchQueue : dispatch_queue_t = dispatch_queue_create("dispatchQueue", nil)
//         dispatch_async(dispatchQueue) {
//            self.encodeToBase64String(self.imageToBeEnroll) { (result) in
//                self.img_data = result!
//                self.enrollUser()
//            }
//        }
        
        
     }
    
    func enrollUser()
    {
        var LatitudeDeg : String = String()
        var LatitudeMin : String = String()
        var LatitudeSec : String = String()
        var LongitudeDeg : String = String()
        var LongitudeMin : String = String()
        var LongitudeSec : String = String()
        if(LatitudeDegree == 0 && LatitudeMinute == 0 && LatitudeSecond == 0 && LongitudeDegree == 0 && LongitudeMinute == 0 && LongitudeSecond == 0){
            LatitudeDeg = "?"
            LatitudeMin = "?"
            LatitudeSec = "?"
            LongitudeDeg = "?"
            LongitudeMin = "?"
            LongitudeSec = "?"
        }else{
            LatitudeDeg = String(LatitudeDegree)
            LatitudeMin = String(LatitudeMinute)
            LatitudeSec = String(LatitudeSecond)
            LongitudeDeg = String(LongitudeDegree)
            LongitudeMin = String(LongitudeMinute)
            LongitudeSec = String(LongitudeSecond)
        }
        //print(img_data)
        self.obj_WebService.sendResponse(self.img_data,firstName:"?",lastName:"?",dob:"?",pob:"?",citizenship:"?",gender:"?",race:"?",height:"?",weight:"?",eye:"?",hair:"?",LatitudeDegree:LatitudeDeg,LatitudeMinute:LatitudeMin,LatitudeSecond:LatitudeSec,LongitudeDegree:LongitudeDeg,LongitudeMinute:LongitudeMin,LongitudeSecond:LongitudeSec){ (block) in
            //print(block);
            self.appdelegate?.dismissView(self.view)
            if(block == nil){
                let Cust = CustomClass()
                var alert = UIAlertController()
                alert = Cust.displayMyAlertMessage("Not able to connect to Server")
                self.presentViewController(alert, animated: true, completion: nil)
                self.appdelegate?.dismissView(self.view)
            }
            else
            {
            let success = block!.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessEnrollResponse")?.objectForKey("ProcessEnrollResult")?.objectForKey("Code")?.objectForKey("text") as? String
            if(success == "enroll.success"){
                let alertController = UIAlertController(title: "Success", message: "Enroll request successfully processed", preferredStyle: UIAlertControllerStyle.Alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
                    self.appdelegate!.shouldSupportAllOrientation = false
                    let destination = self.storyboard!.instantiateViewControllerWithIdentifier("searchViewController") as! searchViewController
                    self.navigationController!.pushViewController(destination, animated: true)
                })
                alertController.addAction(okAction)
                self.presentViewController(alertController, animated: true, completion:{})
            }
            else{
                let alertController = UIAlertController(title: "Fail", message: "Creating Enroll Template Failed", preferredStyle: UIAlertControllerStyle.Alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
                      self.navigationController?.popViewControllerAnimated(true)
                })
                alertController.addAction(okAction)
                self.presentViewController(alertController, animated: true, completion:{})
                
            }
            
            }
        }
    }
    
    func encodeToBase64String(image: UIImage,block:CompletionBlock) -> Void
    {
        
        let imageData = UIImagePNGRepresentation(image)
        let imag_str:String = imageData!.base64EncodedStringWithOptions([])
        ////print(imag_str)
        return block(result: imag_str)
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "EnrollDetails"
        {
            
            let Enroll_VC: EnrollDetailsViewController = (segue.destinationViewController as! EnrollDetailsViewController)
            Enroll_VC.LongitudeDegreeEn = LongitudeDegree
            Enroll_VC.LatitudeDegreeEn = LatitudeDegree
            Enroll_VC.LongitudeMinuteEn = LongitudeMinute
            Enroll_VC.LatitudeMinuteEn = LatitudeMinute
            Enroll_VC.LongitudeSecondEn = LongitudeSecond
            Enroll_VC.LatitudeSecondEn = LatitudeSecond
            Enroll_VC.img_strEn = img_data
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
