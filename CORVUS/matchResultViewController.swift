//
//  matchResultViewController.swift
//  CORVUS
//
//  Created by Riken Shah on 29/08/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit

class matchResultViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    var lattitudeSearch  = Double()
    var longitudeSearch = Double()
    var appdelegate: AppDelegate?
    var imageToBeSearchedAndMatch = UIImage()
    let obj_WebServiceMail = WebService()
    let obj_databaseHandler = DatabaseHandler()
    var user_data : NSArray = NSArray()
    var noOfData = Int()
    var respDictNew : NSDictionary = NSDictionary()
    let date = NSDate()
    let dateFormatter = NSDateFormatter()
    var convertedDate : String = String()
    var convertedTime : String = String()
    var Fname : String = String()
    var gender : String = String()
    var tempAge : String = String()
    var EyeColor : String = String()
    var HairColor : String = String()
    var agePerfect : String = String()
    var enroll_dt : String = String()
    var lname : String = String()
    var age : String = String()
    var dobAge : String = String()
    var height : String = String()
    var weight : String = String()
    var score : String = String()
    var birthPlace : String = String()
    var googleURL : String = String()
    var citizen : String = String()
    var newImage : String = String()
    var oldImage : String = String()
     var imgStr : String = String()
    var image = UIImage()
    var Fname_sv :String = String()
    var signature : String = String()
    var lname_sv :String = String()
    var age_sv : String = String()
    var birthPlace_sv :String = String()
    var citizen_sv :String = String()
    var gender_sv :String = String()
    var race_sv :String = String()
    var height_sv :String = String()
    var weight_sv : String = String()
    var HairColor_sv : String = String()
    var EyeColor_sv : String = String()
    var score_sv : String = String()
    var enroll_dt_sv : String = String()
    var latDeg : String = String()
    var latMin : String = String()
    var latSec : String = String()
    var longDeg : String = String()
    var longMin : String = String()
    var longSec : String = String()
    var lattitude = Double()
    var longitude = Double()

    @IBOutlet weak var myTableView: UITableView!
    
    var respDict : NSDictionary = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(lattitudeSearch)
        //print(longitudeSearch)
        saveAllData()
        appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        
        }
   
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveAllData()
    {
        print(noOfData)
        print(self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))
        for i in 0...noOfData-1{
            dateFormatter.dateFormat = "MM/dd/yyyy"
            convertedDate = dateFormatter.stringFromDate(date)
        if((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))!.isKindOfClass(NSDictionary))
        {
            //print("only one match")
            
           Fname_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("FirstName")?.objectForKey("text") as? String)!
            if(Fname_sv == "?")
            {
                Fname_sv = "-"
            }
           lname_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LastName")?.objectForKey("text") as? String)!
            if(lname_sv == "?")
            {
                lname_sv = "-"
            }

            let check1 = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("i:nil")
            
            if (check1 != nil)
            {
                age_sv = "-"
                dobAge = "-"
            }
            else
            {
                dobAge = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("text") as? String)!
                
               // age_sv = date!
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat =  "MM/dd/yyyy"
                let dateToSave =  dateFormatter.dateFromString(dobAge)
                age_sv  = String(calculateAge(dateToSave!))
                age_sv = "\(age_sv) Years"
                //print(age_sv)
                if(age_sv == "0 Years"){
                    age_sv = String(calculateAgeInMonths(dateToSave!))
                    age_sv = "\(age_sv) Months"
                    //print(age_sv)
                    if(age_sv == "0 Months") {
                        age_sv = String(calculateAgeInDays(dateToSave!))
                        age_sv = "\(age_sv) Days"
                        //print(age_sv)
                    }
                }
                
            }
            birthPlace_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("PlaceOfBirth")?.objectForKey("text") as? String)!
            if(birthPlace_sv == "?")
            {
                birthPlace_sv = "-"
            }
            citizen_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Citizenship")?.objectForKey("text") as? String)!
            if(citizen_sv == "?")
            {
                citizen_sv = "-"
            }
            gender_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Gender")?.objectForKey("text") as? String)!
            if(gender_sv == "?")
            {
                gender_sv = "-"
            }
           race_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Race")?.objectForKey("text") as? String)!
            if(race_sv == "?")
            {
                race_sv = "-"
            }
            let tempHt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text")
            if(tempHt != nil)
            {
                height_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text") as? String)!
                if(height_sv == "0")
                {
                    height_sv = "-"
                }
                else
                {
                    if(height_sv.characters.contains("0"))
                    {
                    height_sv = height_sv.stringByReplacingCharactersInRange(height_sv.startIndex.successor()..<height_sv.startIndex.successor().successor(), withString: "'")
                    //height_sv = height_sv+"\""
                    //print(height_sv)
                    }
                }

            }
            else
            {
                height_sv = "-"
            }
            let tempWt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text")
            if(tempWt != nil)
            {
                weight_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text") as? String)!
                if(weight_sv == "0"){
                    weight_sv = "-"
                }
            }
            else
            {
                 weight_sv = "-"
            }
           
            HairColor_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("HairColor")?.objectForKey("text") as? String)!
            if(HairColor_sv == "?")
            {
                HairColor_sv = "-"
            }
            EyeColor_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("EyeColor")?.objectForKey("text") as? String)!
            if(EyeColor_sv == "?")
            {
                EyeColor_sv = "-"
            }
            let onlyscr =  (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("Score")?.objectForKey("text") as? String)!
            let algo = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("AlgorithmID")?.objectForKey("text") as? String)!
            score_sv = "\(algo):\(onlyscr)"
            enroll_dt_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("EnrollDate")?.objectForKey("text") as? String)!
            let image_sv = decodeBase64ToImage(((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("FaceImage")?.objectForKey("text")) as? String)!)
            
            let new = saveImg(image_sv)
            let old = saveImg(imageToBeSearchedAndMatch)
            let checkSign = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")
            if(checkSign != nil){
                let sign_Img = decodeBase64ToImage((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")?.objectForKey("text") as? String)!)
                signature = saveImg(sign_Img)
            }
            else
            {
                signature = "No image found"
            }
            //save gps url in local db
            let checkForGps = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("i:nil")
            if(checkForGps != nil)
            {
                googleURL = "No GPS available"
                lattitude = 0.0
                longitude = 0.0
            }
            else
            {
            latDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("text") as? String)!
            latMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeMinute")?.objectForKey("text") as? String)!
            latSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeSecond")?.objectForKey("text") as? String)!
            longDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LongitudeDegree")?.objectForKey("text") as? String)!
            longMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LongitudeMinute")?.objectForKey("text") as? String)!
            longSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LongitudeSecond")?.objectForKey("text") as? String)!

                ////print(longitude)
                if(latDeg == "?"){
                    googleURL = "No GPS available"
                }
                else
                {
                    lattitude = Double(latDeg)!+(Double(latMin)!/60.0)+(Double(latSec)!/3600.0)
                    longitude = Double(longDeg)!+(Double(longMin)!/60.0)+(Double(longSec)!/3600.0)
                    if(lattitude > 180){
                        lattitude = lattitude - 360
                    }
                    if(longitude > 180){
                        longitude = longitude - 360
                    }

            googleURL = "http://maps.google.com/maps?&z=20&q=\(lattitude),\(longitude)&ll=\(lattitude),\(longitude)"
                }
            }
            self.obj_databaseHandler.saveUserData(Fname_sv, l_name: lname_sv, u_age: age_sv, u_height: height_sv, u_weight: weight_sv,u_EyeColor:EyeColor_sv,u_hairColor:HairColor_sv, u_score: score_sv, u_birthPlace: birthPlace_sv, u_citizenship: citizen_sv,u_sex: gender_sv,u_enrollDt:enroll_dt_sv,u_newImage: new,u_oldImage:old,u_googleMap:googleURL,u_race:race_sv,u_dob:dobAge,latt:lattitude,long:longitude,lattSrc:lattitudeSearch,longSrc:longitudeSearch,searchDate:convertedDate,sign:signature)
        }
        else
        {
            //print("multiple matches")
            Fname_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("FirstName")?.objectForKey("text") as? String)!
            if(Fname_sv == "?")
            {
                Fname_sv = "-"
            }
            lname_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("LastName")?.objectForKey("text") as? String)!
            if(lname_sv == "?")
            {
                lname_sv = "-"
            }
            let check1 = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("i:nil")
            if (check1 != nil)
            {
                age_sv = "-"
                dobAge = "-"
            }
            else
            {
                dobAge = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("text") as? String)!
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat =  "MM/dd/yyyy"
                let dateToSave =  dateFormatter.dateFromString(dobAge)
                age_sv  = String(calculateAge(dateToSave!))
                age_sv = "\(age_sv) Years"
                //print(age_sv)
                if(age_sv == "0 Years"){
                    age_sv = String(calculateAgeInMonths(dateToSave!))
                    age_sv = "\(age_sv) Months"
                    //print(age_sv)
                    if(age_sv == "0 Months") {
                        age_sv = String(calculateAgeInDays(dateToSave!))
                        age_sv = "\(age_sv) Days"
                        //print(age_sv)
                    }
                }

            }
            birthPlace_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("PlaceOfBirth")?.objectForKey("text") as? String)!
            if(birthPlace_sv == "?")
            {
                birthPlace_sv = "-"
            }
            citizen_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("Citizenship")?.objectForKey("text") as? String)!
            if(citizen_sv == "?")
            {
                citizen_sv = "-"
            }
            gender_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("Gender")?.objectForKey("text") as? String)!
            if(gender_sv == "?")
            {
                gender_sv = "-"
            }
            race_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("Race")?.objectForKey("text") as? String)!
            if(race_sv == "?")
            {
                race_sv = "-"
            }
            let tempHt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text")
            if(tempHt != nil)
            {
                height_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text") as? String)!
                if(height_sv == "0")
                {
                    height_sv = "-"
                }
                else
                {
                    if(height_sv.characters.contains("0"))
                    {
                         height_sv = height_sv.stringByReplacingCharactersInRange(height_sv.startIndex.successor()..<height_sv.startIndex.successor().successor(), withString: "'")
                    }
                   
                    //height_sv = height_sv+"\""
                    //print(height_sv)
                }
 
            }
            else
            {
                height_sv = "-"
            }
            let tempWt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text")
            if(tempWt != nil)
            {
                weight_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text") as? String)!
                if(weight_sv == "0")
                {
                    weight_sv = "-"
                }

            }
            else
            {
                weight_sv = "-"
            }
           
            HairColor_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("HairColor")?.objectForKey("text") as? String)!
            if(HairColor_sv == "?")
            {
                HairColor_sv = "-"
            }
            EyeColor_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("EyeColor")?.objectForKey("text") as? String)!
            if(EyeColor_sv == "?")
            {
                EyeColor_sv = "-"
            }
            let onlyscr = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("Score")?.objectForKey("text") as? String)!
            let algo = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("AlgorithmID")?.objectForKey("text") as? String)!
            score_sv = "\(algo):\(onlyscr)"

            enroll_dt_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("EnrollDate")?.objectForKey("text") as? String)!
            let image_sv = decodeBase64ToImage(((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("FaceImage")?.objectForKey("text")) as? String)!)
            
            let new = saveImg(image_sv)
            let old = saveImg(imageToBeSearchedAndMatch)
            let checkSign = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")
            if(checkSign != nil){
                let sign_Img = decodeBase64ToImage((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")?.objectForKey("text") as? String)!)
                signature = saveImg(sign_Img)
            }
            else
            {
                signature = "No image found"
            }
            
            let checkForGps = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("i:nil")
            if(checkForGps != nil)
            {
                googleURL = "No GPS available"
                lattitude = 0.0
                longitude = 0.0
            }

            else{
            latDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("text") as? String)!
            latMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("LatitudeMinute")?.objectForKey("text") as? String)!
            latSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("LatitudeSecond")?.objectForKey("text") as? String)!
            longDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("LongitudeDegree")?.objectForKey("text") as? String)!
            longMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("LongitudeMinute")?.objectForKey("text") as? String)!
            longSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(i).objectForKey("BiographicInformation")?.objectForKey("LongitudeSecond")?.objectForKey("text") as? String)!
             if(latDeg == "?")
             {
                googleURL = "No GPS available"
                lattitude = 0.0
                longitude = 0.0
            }
            else
                {
                    lattitude = Double(latDeg)!+(Double(latMin)!/60.0)+(Double(latSec)!/3600.0)
                    longitude = Double(longDeg)!+(Double(longMin)!/60.0)+(Double(longSec)!/3600.0)
                    if(lattitude > 180){
                        lattitude = lattitude - 360
                    }
                    if(longitude > 180){
                        longitude = longitude - 360
                    }

                    googleURL = "http://maps.google.com/maps?&z=20&q=\(lattitude),\(longitude)&ll=\(lattitude),\(longitude)"
                }
            }
            self.obj_databaseHandler.saveUserData(Fname_sv, l_name: lname_sv, u_age: age_sv, u_height: height_sv, u_weight: weight_sv,u_EyeColor:EyeColor_sv,u_hairColor:HairColor_sv, u_score: score_sv, u_birthPlace: birthPlace_sv, u_citizenship: citizen_sv,u_sex: gender_sv,u_enrollDt:enroll_dt_sv,u_newImage: new,u_oldImage:old,u_googleMap:googleURL,u_race:race_sv,u_dob:dobAge,latt:lattitude,long:longitude,lattSrc:lattitudeSearch,longSrc:longitudeSearch,searchDate:convertedDate,sign:signature)
            }
        }
    }
    
    @IBAction func longPressHomeBtn(sender: UILongPressGestureRecognizer) {
        if (sender.state == .Began){
            let destination = self.storyboard!.instantiateViewControllerWithIdentifier("openFileViewController") as! openFileViewController
            self.navigationController!.pushViewController(destination, animated: true)
        }
    }
    
    @IBAction func homeButtonTapped(sender: AnyObject) {
        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("homeScreenViewController") as! homeScreenViewController
        self.navigationController!.pushViewController(destination, animated: true)
    }
    @IBAction func EnrollButtonTapped(sender: AnyObject) {
        performSegueWithIdentifier("EnrollScreen", sender: nil)
    }

    // MARK: table view mathods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))!.isKindOfClass(NSDictionary))
        {
            return 1
        }
        else
        {
        return ((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))?.count)!
        }
        //(self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))?.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let deviceType = UIDevice.currentDevice().model
        
       //var cell = UITableViewCell()
           if (UIDevice().userInterfaceIdiom == .Phone)
           {
            let cell : UITableViewCell = UITableViewCell(style: .Default, reuseIdentifier: "dynamicCell")
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            dateFormatter.dateFormat = "MM/dd/yyyy"
            convertedDate = dateFormatter.stringFromDate(date)
            
            if((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))!.isKindOfClass(NSDictionary))
                    {
                      // print("only one match")
                    var searchLabel = UILabel()
                    var matchLabel = UILabel()
                    var searchImage = UIImageView()
                    var matchImage = UIImageView()
                        
                    searchLabel = UILabel(frame: CGRectMake(0, 8, self.view.frame.size.width/2, 20))
                        
                    matchLabel = UILabel(frame: CGRectMake(self.view.frame.size.width/2, 8, self.view.frame.size.width/2, 20))
                        //searchLabel.backgroundColor = UIColor.greenColor()
                        //matchLabel.backgroundColor = UIColor.greenColor()
    
                        
                    
                    searchLabel.text = "Search"
                    searchLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                    searchLabel.font = UIFont.boldSystemFontOfSize(17)
                    searchLabel.textAlignment = .Center
                    matchLabel.text = "Match"
                    matchLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                    matchLabel.font = UIFont.boldSystemFontOfSize(17)
                    matchLabel.textAlignment = .Center
                        
                    var searchImageY = searchLabel.frame.size.height + 16
                    searchImage = UIImageView(frame: CGRectMake(10, searchImageY, (self.view.frame.width/2) - 20 , 90))
                    matchImage = UIImageView(frame: CGRectMake((self.view.frame.width/2) + 10 , searchImageY, (self.view.frame.width/2) - 20, 90))
                        searchImage.image = imageToBeSearchedAndMatch
                    searchImage.contentMode = .ScaleAspectFit
                        let image = decodeBase64ToImage(((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("FaceImage")?.objectForKey("text")) as? String)!)
                        matchImage.image = image
                    matchImage.contentMode = .ScaleAspectFit
                    
                    var firstNameY = searchImageY + searchImage.frame.size.height + 10
                    var firstNameLabel = UILabel()
                    firstNameLabel = UILabel(frame: CGRectMake(20, firstNameY, self.view.frame.size.width - 20, 20))
                    firstNameLabel.text = "First Name"
                    firstNameLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                    firstNameLabel.font = UIFont.boldSystemFontOfSize(14)
                        
                    // first name value
                    var firstNameValY = firstNameY + 22
                    var firstNameVal = UILabel()
                    firstNameVal = UILabel(frame: CGRectMake(20, firstNameValY, self.view.frame.size.width - 20, 20))
                    firstNameVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                    firstNameVal.font = firstNameVal.font.fontWithSize(14)
                        
                    Fname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("FirstName")?.objectForKey("text") as? String)!
                
                        if(Fname == "?")
                            {
                                Fname = "-"
                            }
                    firstNameVal.text = Fname
                        
                    // last name label
                        var lastNameLabelY = firstNameValY + firstNameVal.frame.size.height + 5
                        var lastNameLabel = UILabel(frame: CGRectMake(20, lastNameLabelY, self.view.frame.size.width - 20, 20))
                        lastNameLabel.text = "Last Name"
                        lastNameLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        lastNameLabel.font = UIFont.boldSystemFontOfSize(14)
                        
                        // last name value
                        var lastNameValY = lastNameLabelY + 22
                        var lastNameVal = UILabel(frame: CGRectMake(20, lastNameValY, self.view.frame.size.width - 20, 20))
                        lastNameVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                        lastNameVal.font = lastNameVal.font.fontWithSize(14)
                        
                        lname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LastName")?.objectForKey("text") as? String)!
                            if(lname == "?")
                                {
                                    lname = "-"
                                }
                            lastNameVal.text = lname
                        
                        //dob row view
                        var dobY = lastNameValY + 22
                        var dobRowView = UIView(frame: CGRectMake(20, dobY, self.view.frame.size.width - 40, 50))
                        
                        //dob label
                        var dobLabel = UILabel(frame: CGRectMake(0,0,(dobRowView.frame.size.width)/4 - 2 ,20))
                        dobLabel.numberOfLines = 0
                        dobLabel.text = "Date of Birth"
                        dobLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        dobLabel.font = UIFont.boldSystemFontOfSize(14)
                        dobLabel.sizeToFit()
                        
                        //age label 
                        var ageLabel = UILabel(frame: CGRectMake((dobRowView.frame.size.width)/4, 0, (dobRowView.frame.size.width)/4 - 2, 20))
                        ageLabel.text = "Age"
                        ageLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        ageLabel.font = UIFont.boldSystemFontOfSize(14)
                        ageLabel.sizeToFit()
                        
                        //gender label
                        var genderLabel = UILabel(frame: CGRectMake(dobRowView.frame.size.width/2, 0, (dobRowView.frame.size.width)/4 - 2, 20))
                        genderLabel.text = "Gender"
                        genderLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        genderLabel.font = UIFont.boldSystemFontOfSize(14)
                        genderLabel.sizeToFit()
                        
                        //height label
                        var heightLabel = UILabel(frame: CGRectMake((dobRowView.frame.size.width/4)*3, 0, (dobRowView.frame.size.width)/4 - 2, 20))
                        heightLabel.text = "Height"
                        heightLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        heightLabel.font = UIFont.boldSystemFontOfSize(14)
                        heightLabel.sizeToFit()
                        
                        dobRowView.addSubview(dobLabel)
                        dobRowView.addSubview(ageLabel)
                        dobRowView.addSubview(genderLabel)
                        dobRowView.addSubview(heightLabel)
                        
                        dobRowView.frame = CGRectMake(20, dobY, self.view.frame.size.width - 40, dobLabel.frame.size.height)
                        
                        //dob row value
                        var dobValY = dobY + dobRowView.frame.size.height + 4
                        
                        var dobValRowView = UIView(frame: CGRectMake(20, dobValY, self.view.frame.size.width - 40, 50))
                        
                        //dob val
                        var dobVal = UILabel(frame: CGRectMake(0,0,dobRowView.frame.size.width/4 - 2,20))
                        dobVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                        dobVal.font = dobVal.font.fontWithSize(10)
                        
                                        let check1 = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("i:nil")
                                        if (check1 != nil)
                                        {
                                            age = "-"
                                            tempAge = "-"
                                        }
                                        else
                                        {
                                            let date = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("text") as? String
                                            age = date!
                                            let dateFormatter = NSDateFormatter()
                                            dateFormatter.dateFormat =  "MM/dd/yyyy"
                                            let dateToSave =  dateFormatter.dateFromString(date!)
                                            tempAge  = String(calculateAge(dateToSave!))
                                            tempAge = "\(tempAge) Years"
                                            //print(tempAge)
                                            if(tempAge == "0 Years"){
                                                tempAge = String(calculateAgeInMonths(dateToSave!))
                                                tempAge = "\(tempAge) Months"
                                                //print(tempAge)
                                                if(tempAge == "0 Months") {
                                                    tempAge = String(calculateAgeInDays(dateToSave!))
                                                    tempAge = "\(tempAge) Days"
                                                    //print(tempAge)
                                                }
                                            }
                                        }

                        dobVal.text = age
                        dobVal.sizeToFit()
                        
                        //age val
                        var ageVal = UILabel(frame: CGRectMake(dobRowView.frame.size.width/4, 0, dobRowView.frame.size.width/4 - 2, 20))
                        ageVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                        ageVal.font = ageVal.font.fontWithSize(13)
                        ageVal.text = tempAge
                        ageVal.sizeToFit()
                        
                        //gender value
                        var genderVal = UILabel(frame: CGRectMake(dobRowView.frame.size.width/2, 0, dobRowView.frame.size.width/4 - 2, 20))
                        genderVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                        genderVal.font = genderVal.font.fontWithSize(13)
                        genderVal.numberOfLines = 0
                                        gender = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Gender")?.objectForKey("text") as? String)!
                                        if(gender == "?")
                                        {
                                            gender = "-"
                                        }
                        genderVal.text = gender
                        genderVal.sizeToFit()
                        
                        //height value
                        var heightVal = UILabel(frame: CGRectMake((dobRowView.frame.size.width/4)*3, 0, dobRowView.frame.size.width/4 - 2, 20))
                        heightVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                        heightVal.font = heightVal.font.fontWithSize(13)
                        
                                        let tempHt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text")
                                        if(tempHt != nil)
                                        {
                                            height = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text") as? String)!
                                            if(height == "0")
                                            {
                                                height = "-"
                                            }
                                            else
                                            {
                                                if(height.characters.contains("0"))
                                                {
                                                height = height.stringByReplacingCharactersInRange(height.startIndex.successor()..<height.startIndex.successor().successor(), withString: "'")
                                                height = height+"\""
                                                }
                                            }
                                            
                                        }
                                        else
                                        {
                                            height = "-"
                                        }
                        heightVal.text = height
                        heightVal.sizeToFit()
                        
                        dobValRowView.addSubview(dobVal)
                        dobValRowView.addSubview(ageVal)
                        dobValRowView.addSubview(genderVal)
                        dobValRowView.addSubview(heightVal)
                        
                        dobValRowView.frame = CGRectMake(20, dobValY, self.view.frame.size.width - 40, genderVal.frame.size.height)
                        
                        var  dateSeenRowY = dobValY + dobValRowView.frame.size.height + 4
                        var dateSeenRowView = UIView(frame: CGRectMake(20, dateSeenRowY, self.view.frame.size.width - 40, 50))
                        
                        //date see label
                        var dateSeenLabel = UILabel(frame: CGRectMake(0,0,(dateSeenRowView.frame.size.width)/4 - 2 ,20))
                        dateSeenLabel.numberOfLines = 0
                        dateSeenLabel.text = "Date 1st Seen"
                        dateSeenLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        dateSeenLabel.font = UIFont.boldSystemFontOfSize(14)
                        dateSeenLabel.sizeToFit()

                        //eye color label
                        var eyeColorLabel = UILabel(frame: CGRectMake((dateSeenRowView.frame.size.width)/4, 0, (dateSeenRowView.frame.size.width)/4 - 2, 20))
                        eyeColorLabel.text = "Eye Color"
                        eyeColorLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        eyeColorLabel.font = UIFont.boldSystemFontOfSize(14)
                        eyeColorLabel.sizeToFit()
                        
                        //hair color label
                        var hairColorLabel = UILabel(frame: CGRectMake(dateSeenRowView.frame.size.width/2, 0, (dateSeenRowView.frame.size.width)/4 - 2, 20))
                        hairColorLabel.text = "Hair Color"
                        hairColorLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        hairColorLabel.font = UIFont.boldSystemFontOfSize(14)
                        hairColorLabel.sizeToFit()
                        
                        //weight label
                        var weightLabel = UILabel(frame: CGRectMake((dateSeenRowView.frame.size.width/4)*3, 0, (dateSeenRowView.frame.size.width)/4 - 2, 20))
                        weightLabel.text = "Weight"
                        weightLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        weightLabel.font = UIFont.boldSystemFontOfSize(14)
                        weightLabel.sizeToFit()
                        
                        dateSeenRowView.addSubview(dateSeenLabel)
                        dateSeenRowView.addSubview(eyeColorLabel)
                        dateSeenRowView.addSubview(hairColorLabel)
                        dateSeenRowView.addSubview(weightLabel)
                        
                        dateSeenRowView.frame = CGRectMake(20, dateSeenRowY, self.view.frame.size.width - 40, dateSeenLabel.frame.size.height)
                        
                        var dateSeenValY = dateSeenRowY + dateSeenRowView.frame.size.height + 4
                        var dateSeenValView = UIView(frame: CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, 50))
                        
                        //date 1st seen value
                        var dateSeenVal = UILabel(frame: CGRectMake(0,0,(dateSeenValView.frame.size.width)/4 - 2,20))
                        dateSeenVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                        dateSeenVal.font = dateSeenVal.font.fontWithSize(10)
                        enroll_dt = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("EnrollDate")?.objectForKey("text") as? String)!
                        dateSeenVal.text = enroll_dt
                        dateSeenVal.sizeToFit()
                        
                        //eye color value
                        var eyeColorValue = UILabel(frame: CGRectMake(dateSeenValView.frame.size.width/4,0,(dateSeenRowView.frame.size.width)/4 - 2,20))
                        eyeColorValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                        eyeColorValue.font = eyeColorValue.font.fontWithSize(13)
                        eyeColorValue.numberOfLines = 0
                        EyeColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("EyeColor")?.objectForKey("text") as? String)!
                            if(EyeColor == "?")
                              {
                                  EyeColor = "-"
                              }
                        eyeColorValue.text = EyeColor
                        eyeColorValue.sizeToFit()
                        
                        //hair color value
                        var hairColorValue = UILabel(frame: CGRectMake(dateSeenValView.frame.size.width/2,0,(dateSeenValView.frame.size.width)/4 - 2,20))
                        hairColorValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                        hairColorValue.font = hairColorValue.font.fontWithSize(13)
                        hairColorValue.numberOfLines = 0
                        HairColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("HairColor")?.objectForKey("text") as? String)!
                            if(HairColor == "?")
                              {
                                HairColor = "-"
                               }
                        hairColorValue.text = HairColor
                        hairColorValue.sizeToFit()

                        //weight value
                        var weightVal = UILabel(frame: CGRectMake((dateSeenValView.frame.size.width/4)*3, 0, (dateSeenValView.frame.size.width)/4 - 2, 20))
                        weightVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                        weightVal.font = weightVal.font.fontWithSize(13)
                        let tempWt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text")
                         if(tempWt != nil)
                           {
                             weight = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text") as? String)!
                                 if(weight == "0")
                                 {
                                    weight = "-"
                                  }
                            }
                            else
                            {
                                weight = "-"
                             }
                        weightVal.text = weight
                        
                        
                        dateSeenValView.addSubview(dateSeenVal)
                        dateSeenValView.addSubview(eyeColorValue)
                        dateSeenValView.addSubview(hairColorValue)
                        dateSeenValView.addSubview(weightVal)
                        
                        if(eyeColorValue.frame.size.height > hairColorValue.frame.size.height)
                        {
                            dateSeenValView.frame = CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, eyeColorValue.frame.size.height)
                        }
                        else if(eyeColorValue.frame.size.height < hairColorValue.frame.size.height)
                        {
                            dateSeenValView.frame = CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, hairColorValue.frame.size.height)
                        }
                        else
                        {
                        dateSeenValView.frame = CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, hairColorValue.frame.size.height)
                        }
                        
                        //countryLabelView
                        
                        var countryLabelViewY = dateSeenValY + dateSeenValView.frame.size.height + 4
                        var countryLabelRowView = UIView(frame: CGRectMake(20, countryLabelViewY, self.view.frame.size.width - 40,50))
                        
                        //country label
                        var countryLabel = UILabel(frame: CGRectMake(0, 0, (countryLabelRowView.frame.size.width)/4 - 2,20))
                        countryLabel.text = "Country of Birth"
                        countryLabel.numberOfLines = 0
                        countryLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        countryLabel.font = UIFont.boldSystemFontOfSize(14)
                        countryLabel.sizeToFit()

                        //citizenship label
                        var citizenshipLabel = UILabel(frame: CGRectMake(countryLabelRowView.frame.size.width/4, 0, (countryLabelRowView.frame.size.width/4)-2,20))
                        citizenshipLabel.text = "Citizenship"
                        citizenshipLabel.numberOfLines = 0
                        citizenshipLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        citizenshipLabel.font = UIFont.boldSystemFontOfSize(14)
                        citizenshipLabel.sizeToFit()
                        
                        //score Label
                        var scoreLabel = UILabel(frame: CGRectMake(countryLabelRowView.frame.size.width/2, 0, (countryLabelRowView.frame.size.width/4)-2,20))
                        scoreLabel.text = "Score"
                        scoreLabel.numberOfLines = 0
                        scoreLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        scoreLabel.font = UIFont.boldSystemFontOfSize(14)
                        scoreLabel.sizeToFit()

                        
                        //EnrollGps Label
                        var enrollGpsLabel = UILabel(frame: CGRectMake((countryLabelRowView.frame.size.width/4)*3, 0, (countryLabelRowView.frame.size.width/4)-2,30))
                        enrollGpsLabel.text = "Enroll GPS Location"
                        enrollGpsLabel.numberOfLines = 0
                        enrollGpsLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        enrollGpsLabel.font = UIFont.boldSystemFontOfSize(14)
                        enrollGpsLabel.sizeToFit()
                        
                        countryLabelRowView.addSubview(countryLabel)
                        countryLabelRowView.addSubview(citizenshipLabel)
                        countryLabelRowView.addSubview(scoreLabel)
                        countryLabelRowView.addSubview(enrollGpsLabel)

                        countryLabelRowView.frame = CGRectMake(20, countryLabelViewY, self.view.frame.size.width - 40,enrollGpsLabel.frame.size.height)
                        
                        
                        //country value label
                        var countryValY = countryLabelViewY + enrollGpsLabel.frame.size.height + 4
                        var countryVal = UILabel(frame: CGRectMake(20, countryValY, (countryLabelRowView.frame.size.width)/4 - 2,20))
                        countryVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                        countryVal.font = countryVal.font.fontWithSize(13)
                        countryVal.numberOfLines = 0
                        birthPlace = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("PlaceOfBirth")?.objectForKey("text") as? String)!
                        if(birthPlace == "?")
                        {
                            birthPlace = "-"
                         }
                        countryVal.text = birthPlace
                        countryVal.sizeToFit()
                        
                        //citizenship value
                        var citizenshipVal = UILabel(frame: CGRectMake(20 + (countryLabelRowView.frame.size.width)/4,countryValY,(countryLabelRowView.frame.size.width)/4,20))
                        citizenshipVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                        citizenshipVal.font = citizenshipVal.font.fontWithSize(13)
                        citizenshipVal.numberOfLines = 0
                        
                        citizen = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Citizenship")?.objectForKey("text") as? String)!
                        if(citizen == "?")
                         {
                          citizen = "-"
                          }
                        citizenshipVal.text = citizen
                        citizenshipVal.sizeToFit()
                        
                        //score val
                        var scoreVal = UILabel(frame: CGRectMake(20 + countryLabelRowView.frame.size.width/2, countryValY, (countryLabelRowView.frame.size.width)/4,20))
                        
                        scoreVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                        scoreVal.font = scoreVal.font.fontWithSize(13)
                        scoreVal.numberOfLines = 0
                        
                        let nlyScr = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("Score")?.objectForKey("text") as? String)!
                         let algo = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("AlgorithmID")?.objectForKey("text") as? String)!
                          score = "\(algo):\(nlyScr)"
                        scoreVal.text = score
                        scoreVal.sizeToFit()
                        
                        //enroll GPS button and title label
                        
                        var enrollValLbl = UILabel(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3,countryValY,(countryLabelRowView.frame.size.width)/4,35))
                        enrollValLbl.numberOfLines = 0
                        enrollValLbl.font = enrollValLbl.font.fontWithSize(11)
                        
                        var enrollGpsBtn = UIButton(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3,countryValY,(countryLabelRowView.frame.size.width)/4,35))
                        enrollGpsBtn.titleLabel?.font = UIFont.systemFontOfSize(11)
                        enrollGpsBtn.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
                        enrollGpsBtn.titleLabel?.numberOfLines = 0
//                        enrollGpsBtn.backgroundColor = UIColor.redColor()
                        
                        let checkForGps = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("i:nil")
                        if(checkForGps != nil || latDeg == "?")
                         {
                          //googleURL = "No GPS available"
//                            enrollGpsBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
//                            enrollGpsBtn.setTitle("No GPS available", forState: .Normal)
                            enrollValLbl.textColor = UIColor.darkGrayColor()
                            enrollValLbl.text = "No GPS available"
                         }
                        else
                        {
//                        enrollGpsBtn.setTitleColor(UIColor.blueColor(), forState: .Normal)
//                        enrollGpsBtn.setTitle("\(lattitude) \(longitude)", forState: .Normal)
                            enrollValLbl.textColor = UIColor.blueColor()
                            enrollValLbl.text = "\(lattitude) \(longitude)"
                         
                         enrollGpsBtn.accessibilityIdentifier = String(indexPath.row)
                         enrollGpsBtn.addTarget(self, action: Selector("gpsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                        }
                        enrollValLbl.sizeToFit()
                        enrollGpsBtn.layoutIfNeeded()
                        enrollGpsBtn.frame = CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3,countryValY,(countryLabelRowView.frame.size.width)/4, enrollValLbl.frame.size.height)
                        
                        
                        //search GPS label row
                        var searchGpsLocY = CGFloat()
                        if((countryVal.frame.size.height >= citizenshipVal.frame.size.height) && (countryVal.frame.size.height >= scoreVal.frame.size.height) && (countryVal.frame.size.height >= enrollValLbl.frame.size.height))
                        {
                            searchGpsLocY = countryValY + countryVal.frame.size.height + 4
                        }
                        else if((citizenshipVal.frame.size.height >= countryVal.frame.size.height) && (citizenshipVal.frame.size.height >= scoreVal.frame.size.height) && (citizenshipVal.frame.size.height >= enrollValLbl.frame.size.height))
                        {
                            searchGpsLocY = countryValY + citizenshipVal.frame.size.height + 4
                        }
                        else if((scoreVal.frame.size.height >= countryVal.frame.size.height) && (scoreVal.frame.size.height >= citizenshipVal.frame.size.height) && (scoreVal.frame.size.height >= enrollValLbl.frame.size.height))
                        {
                             searchGpsLocY = countryValY + scoreVal.frame.size.height + 4
                        }
                        else
                        {
                            searchGpsLocY = countryValY + enrollValLbl.frame.size.height + 4
                        }
                        
                        //date searched Label
                        var dateSearchedLabel = UILabel(frame: CGRectMake(20 + countryLabelRowView.frame.size.width/2, searchGpsLocY, (countryLabelRowView.frame.size.width)/4,20))
                        dateSearchedLabel.text = "Date seen"
                        dateSearchedLabel.numberOfLines = 0
                        dateSearchedLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        dateSearchedLabel.font = UIFont.boldSystemFontOfSize(14)
                        dateSearchedLabel.sizeToFit()

                        //search GPS location
                        var searchGpsLocLabel = UILabel(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocY, (countryLabelRowView.frame.size.width)/4,20))
                        searchGpsLocLabel.text = "Search GPS Location"
                        searchGpsLocLabel.numberOfLines = 0
                        searchGpsLocLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                        searchGpsLocLabel.font = UIFont.boldSystemFontOfSize(14)
                        searchGpsLocLabel.sizeToFit()
                        
                        //search GPS row value
                        var searchGpsLocValY = searchGpsLocY + searchGpsLocLabel.frame.size.height + 4
                        
                        //date searched
                        var dateSearchedVal = UILabel(frame: CGRectMake(20 + countryLabelRowView.frame.size.width/2, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4, 20))
                        dateSearchedVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                        dateSearchedVal.font = dateSearchedVal.font.fontWithSize(10)
                        dateSearchedVal.text = convertedDate
                        
                        //search GPS Location and title label
                        var searchValLbl = UILabel(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4,40))
                        searchValLbl.numberOfLines = 0
                         searchValLbl.font = searchValLbl.font.fontWithSize(11)
                        
                        var searchGpsLocation = UIButton(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4,40))
                        searchGpsLocation.titleLabel?.font = UIFont.systemFontOfSize(11)
                        searchGpsLocation.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
                        searchGpsLocation.titleLabel?.numberOfLines = 0
                        //searchGpsLocation.backgroundColor = UIColor.yellowColor()
                        
                        if(lattitudeSearch == 0 && longitudeSearch == 0)
                        {
//                        searchGpsLocation.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
//                        searchGpsLocation.setTitle("No GPS available", forState: .Normal)
                            searchValLbl.textColor = UIColor.darkGrayColor()
                            searchValLbl.text = "No GPS available"
                        }
                        else
                        {
//                         searchGpsLocation.setTitleColor(UIColor.blueColor(), forState: .Normal)
//                         searchGpsLocation.setTitle("\(lattitudeSearch) \((longitudeSearch))", forState: .Normal)
                            searchValLbl.textColor = UIColor.blueColor()
                            searchValLbl.text = "\(lattitudeSearch) \((longitudeSearch))"
                         searchGpsLocation.addTarget(self, action: Selector("gpsSearchButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                        }
                        searchValLbl.sizeToFit()
                        searchGpsLocation.layoutIfNeeded()
                        searchGpsLocation.frame = CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4,searchValLbl.frame.size.height)
                        
                        //sign y value
                        var signImageY = searchGpsLocValY + searchValLbl.frame.size.height + 4
                        
                        // sign image 
                        var signImage = UIImageView(frame: CGRectMake(20, signImageY,(self.view.frame.width/2)-30, 80))
                        
                            let checkSign = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")
                             if(checkSign != nil){
                                            // sign is available
                              let sign_img = decodeBase64ToImage((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")?.objectForKey("text") as? String)!)
                             signImage.image = sign_img
                              }
                              else
                              {
                              // no sign available
                              signImage.image = UIImage(named:"noImage")
                              }
                        //signImage.backgroundColor = UIColor.blueColor()
                        signImage.contentMode = .ScaleAspectFit
                        signImage.frame = CGRectMake(20, signImageY,(self.view.frame.width/2)-30, ((self.view.frame.width/2)-30)/1.6)
                        
                        // email button
                        var emailButton = UIButton(frame: CGRectMake((self.view.frame.width/2)+(self.view.frame.width/8) - 25, signImageY + ((signImage.frame.size.height/2)-25), 50, 50))
                        emailButton.setBackgroundImage(UIImage(named: "bigAlert"), forState: .Normal)
                        emailButton.accessibilityIdentifier = String(indexPath.row)
                        emailButton.addTarget(self, action: Selector("mailButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)

                        
                        //close Button
                        var closeButton = UIButton(frame: CGRectMake((self.view.frame.size.width/4)*3 + (self.view.frame.width/8) - 25, signImageY + ((signImage.frame.size.height/2)-25), 50, 50))
                        
                        closeButton.setBackgroundImage(UIImage(named: "bigCross"), forState: .Normal)
                        closeButton.accessibilityIdentifier = String(indexPath.row)
                        closeButton.addTarget(self, action: Selector("cancelButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                       
                        
                        cell.contentView.addSubview(searchLabel)
                        cell.contentView.addSubview(matchLabel)
                        cell.contentView.addSubview(searchImage)
                        cell.contentView.addSubview(matchImage)
                        cell.contentView.addSubview(firstNameLabel)
                        cell.contentView.addSubview(firstNameVal)
                        cell.contentView.addSubview(lastNameLabel)
                        cell.contentView.addSubview(lastNameVal)
                        cell.contentView.addSubview(dobRowView)
                        cell.contentView.addSubview(dobValRowView)
                        cell.contentView.addSubview(dateSeenRowView)
                        cell.contentView.addSubview(dateSeenValView)
                        cell.contentView.addSubview(countryLabelRowView)
                        cell.contentView.addSubview(countryVal)
                        cell.contentView.addSubview(citizenshipVal)
                        cell.contentView.addSubview(scoreVal)
                        cell.contentView.addSubview(enrollValLbl)
                        cell.contentView.addSubview(enrollGpsBtn)
                        cell.contentView.addSubview(dateSearchedLabel)
                        cell.contentView.addSubview(searchGpsLocLabel)
                        cell.contentView.addSubview(dateSearchedVal)
                        cell.contentView.addSubview(searchValLbl)
                        cell.contentView.addSubview(searchGpsLocation)
                        cell.contentView.addSubview(signImage)
                        cell.contentView.addSubview(emailButton)
                        cell.contentView.addSubview(closeButton)
                        
           }
            else
            {
                //print("multiple matches") 
                
                var searchLabel = UILabel()
                var matchLabel = UILabel()
                var searchImage = UIImageView()
                var matchImage = UIImageView()
                
                searchLabel = UILabel(frame: CGRectMake(0, 8, self.view.frame.size.width/2, 20))
                matchLabel = UILabel(frame: CGRectMake(self.view.frame.size.width/2, 8, self.view.frame.size.width/2, 20))
                
                
                searchLabel.text = "Search"
                searchLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                searchLabel.font = UIFont.boldSystemFontOfSize(17)
                searchLabel.textAlignment = .Center
                matchLabel.text = "Match"
                matchLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                matchLabel.font = UIFont.boldSystemFontOfSize(17)
                matchLabel.textAlignment = .Center
                
                var searchImageY = searchLabel.frame.size.height + 16
                searchImage = UIImageView(frame: CGRectMake(10, searchImageY, (self.view.frame.width/2) - 20 , 90))
                matchImage = UIImageView(frame: CGRectMake((self.view.frame.width/2) + 10 , searchImageY, (self.view.frame.width/2) - 20, 90))
                searchImage.image = imageToBeSearchedAndMatch
                searchImage.contentMode = .ScaleAspectFit
                let image = decodeBase64ToImage(((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("FaceImage")?.objectForKey("text")) as? String)!)
                matchImage.image = image
                matchImage.contentMode = .ScaleAspectFit
                
                var firstNameY = searchImageY + searchImage.frame.size.height + 10
                var firstNameLabel = UILabel()
                firstNameLabel = UILabel(frame: CGRectMake(20, firstNameY, self.view.frame.size.width - 20, 20))
                firstNameLabel.text = "First Name"
                firstNameLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                firstNameLabel.font = UIFont.boldSystemFontOfSize(14)
                
                // first name value
                var firstNameValY = firstNameY + 22
                var firstNameVal = UILabel()
                firstNameVal = UILabel(frame: CGRectMake(20, firstNameValY, self.view.frame.size.width - 20, 20))
                firstNameVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                firstNameVal.font = firstNameVal.font.fontWithSize(14)
                
                Fname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("FirstName")?.objectForKey("text") as? String)!
                
                if(Fname == "?")
                {
                    Fname = "-"
                }
                firstNameVal.text = Fname
                
                // last name label
                var lastNameLabelY = firstNameValY + firstNameVal.frame.size.height + 5
                var lastNameLabel = UILabel(frame: CGRectMake(20, lastNameLabelY, self.view.frame.size.width - 20, 20))
                lastNameLabel.text = "Last Name"
                lastNameLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                lastNameLabel.font = UIFont.boldSystemFontOfSize(14)
                
                // last name value
                var lastNameValY = lastNameLabelY + 22
                var lastNameVal = UILabel(frame: CGRectMake(20, lastNameValY, self.view.frame.size.width - 20, 20))
                lastNameVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                lastNameVal.font = lastNameVal.font.fontWithSize(14)
                
                lname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LastName")?.objectForKey("text") as? String)!
                if(lname == "?")
                {
                    lname = "-"
                }
                lastNameVal.text = lname
                
                //dob row view
                var dobY = lastNameValY + 22
                var dobRowView = UIView(frame: CGRectMake(20, dobY, self.view.frame.size.width - 40, 50))
                
                //dob label
                var dobLabel = UILabel(frame: CGRectMake(0,0,(dobRowView.frame.size.width)/4 - 2 ,20))
                dobLabel.numberOfLines = 0
                dobLabel.text = "Date of Birth"
                dobLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                dobLabel.font = UIFont.boldSystemFontOfSize(14)
                dobLabel.sizeToFit()
                
                //age label
                var ageLabel = UILabel(frame: CGRectMake((dobRowView.frame.size.width)/4, 0, (dobRowView.frame.size.width)/4 - 2, 20))
                ageLabel.text = "Age"
                ageLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                ageLabel.font = UIFont.boldSystemFontOfSize(14)
                ageLabel.sizeToFit()
                
                //gender label
                var genderLabel = UILabel(frame: CGRectMake(dobRowView.frame.size.width/2, 0, (dobRowView.frame.size.width)/4 - 2, 20))
                genderLabel.text = "Gender"
                genderLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                genderLabel.font = UIFont.boldSystemFontOfSize(14)
                genderLabel.sizeToFit()
                
                //height label
                var heightLabel = UILabel(frame: CGRectMake((dobRowView.frame.size.width/4)*3, 0, (dobRowView.frame.size.width)/4 - 2, 20))
                heightLabel.text = "Height"
                heightLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                heightLabel.font = UIFont.boldSystemFontOfSize(14)
                heightLabel.sizeToFit()
                
                dobRowView.addSubview(dobLabel)
                dobRowView.addSubview(ageLabel)
                dobRowView.addSubview(genderLabel)
                dobRowView.addSubview(heightLabel)
                
                dobRowView.frame = CGRectMake(20, dobY, self.view.frame.size.width - 40, dobLabel.frame.size.height)
                
                //dob row value
                var dobValY = dobY + dobRowView.frame.size.height + 4
                
                var dobValRowView = UIView(frame: CGRectMake(20, dobValY, self.view.frame.size.width - 40, 50))
                
                //dob val
                var dobVal = UILabel(frame: CGRectMake(0,0,dobRowView.frame.size.width/4 - 2,20))
                dobVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                dobVal.font = dobVal.font.fontWithSize(10)
                
                let check1 = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("i:nil")
                if (check1 != nil)
                {
                    age = "-"
                    tempAge = "-"
                }
                else
                {
                    let date = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("text") as? String
                    age = date!
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat =  "MM/dd/yyyy"
                    let dateToSave =  dateFormatter.dateFromString(date!)
                    tempAge  = String(calculateAge(dateToSave!))
                    tempAge = "\(tempAge) Years"
                    //print(tempAge)
                    if(tempAge == "0 Years"){
                        tempAge = String(calculateAgeInMonths(dateToSave!))
                        tempAge = "\(tempAge) Months"
                        //print(tempAge)
                        if(tempAge == "0 Months") {
                            tempAge = String(calculateAgeInDays(dateToSave!))
                            tempAge = "\(tempAge) Days"
                            //print(tempAge)
                        }
                    }
                }
                dobVal.text = age
                dobVal.sizeToFit()
                
                //age val
                var ageVal = UILabel(frame: CGRectMake(dobRowView.frame.size.width/4, 0, dobRowView.frame.size.width/4 - 2, 20))
                ageVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                ageVal.font = ageVal.font.fontWithSize(13)
                ageVal.text = tempAge
                ageVal.sizeToFit()
                
                //gender value
                var genderVal = UILabel(frame: CGRectMake(dobRowView.frame.size.width/2, 0, dobRowView.frame.size.width/4 - 2, 20))
                genderVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                genderVal.font = genderVal.font.fontWithSize(13)
                genderVal.numberOfLines = 0
                gender = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Gender")?.objectForKey("text") as? String)!
                if(gender == "?")
                {
                    gender = "-"
                }
                genderVal.text = gender
                genderVal.sizeToFit()
                
                //height value
                var heightVal = UILabel(frame: CGRectMake((dobRowView.frame.size.width/4)*3, 0, dobRowView.frame.size.width/4 - 2, 20))
                heightVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                heightVal.font = heightVal.font.fontWithSize(13)
                
                let tempHt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text")
                if(tempHt != nil)
                {
                    height = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text") as? String)!
                    if(height == "0")
                    {
                        height = "-"
                    }
                    else
                    {
                        if(height.characters.contains("0"))
                        {
                            height = height.stringByReplacingCharactersInRange(height.startIndex.successor()..<height.startIndex.successor().successor(), withString: "'")
                            height = height+"\""
                        }
                    }
                    
                }
                else
                {
                    height = "-"
                }
                heightVal.text = height
                heightVal.sizeToFit()
                
                dobValRowView.addSubview(dobVal)
                dobValRowView.addSubview(ageVal)
                dobValRowView.addSubview(genderVal)
                dobValRowView.addSubview(heightVal)
                
                dobValRowView.frame = CGRectMake(20, dobValY, self.view.frame.size.width - 40, genderVal.frame.size.height)
                
                var  dateSeenRowY = dobValY + dobValRowView.frame.size.height + 4
                var dateSeenRowView = UIView(frame: CGRectMake(20, dateSeenRowY, self.view.frame.size.width - 40, 50))
                
                //date see label
                var dateSeenLabel = UILabel(frame: CGRectMake(0,0,(dateSeenRowView.frame.size.width)/4 - 2 ,20))
                dateSeenLabel.numberOfLines = 0
                dateSeenLabel.text = "Date 1st Seen"
                dateSeenLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                dateSeenLabel.font = UIFont.boldSystemFontOfSize(14)
                dateSeenLabel.sizeToFit()
                
                //eye color label
                var eyeColorLabel = UILabel(frame: CGRectMake((dateSeenRowView.frame.size.width)/4, 0, (dateSeenRowView.frame.size.width)/4 - 2, 20))
                eyeColorLabel.text = "Eye Color"
                eyeColorLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                eyeColorLabel.font = UIFont.boldSystemFontOfSize(14)
                eyeColorLabel.sizeToFit()
                
                //hair color label
                var hairColorLabel = UILabel(frame: CGRectMake(dateSeenRowView.frame.size.width/2, 0, (dateSeenRowView.frame.size.width)/4 - 2, 20))
                hairColorLabel.text = "Hair Color"
                hairColorLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                hairColorLabel.font = UIFont.boldSystemFontOfSize(14)
                hairColorLabel.sizeToFit()
                
                //weight label
                var weightLabel = UILabel(frame: CGRectMake((dateSeenRowView.frame.size.width/4)*3, 0, (dateSeenRowView.frame.size.width)/4 - 2, 20))
                weightLabel.text = "Weight"
                weightLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                weightLabel.font = UIFont.boldSystemFontOfSize(14)
                weightLabel.sizeToFit()
                
                dateSeenRowView.addSubview(dateSeenLabel)
                dateSeenRowView.addSubview(eyeColorLabel)
                dateSeenRowView.addSubview(hairColorLabel)
                dateSeenRowView.addSubview(weightLabel)
                
                dateSeenRowView.frame = CGRectMake(20, dateSeenRowY, self.view.frame.size.width - 40, dateSeenLabel.frame.size.height)
                
                var dateSeenValY = dateSeenRowY + dateSeenRowView.frame.size.height + 4
                var dateSeenValView = UIView(frame: CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, 50))
                
                //date 1st seen value
                var dateSeenVal = UILabel(frame: CGRectMake(0,0,(dateSeenValView.frame.size.width)/4 - 2,20))
                dateSeenVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                dateSeenVal.font = dateSeenVal.font.fontWithSize(10)
                enroll_dt = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("EnrollDate")?.objectForKey("text") as? String)!
                dateSeenVal.text = enroll_dt
                dateSeenVal.sizeToFit()
                
                //eye color value
                var eyeColorValue = UILabel(frame: CGRectMake(dateSeenValView.frame.size.width/4,0,(dateSeenRowView.frame.size.width)/4 - 2,20))
                eyeColorValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                eyeColorValue.font = eyeColorValue.font.fontWithSize(13)
                eyeColorValue.numberOfLines = 0
                EyeColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("EyeColor")?.objectForKey("text") as? String)!
                if(EyeColor == "?")
                {
                    EyeColor = "-"
                }
                eyeColorValue.text = EyeColor
                eyeColorValue.sizeToFit()
                
                //hair color value
                var hairColorValue = UILabel(frame: CGRectMake(dateSeenValView.frame.size.width/2,0,(dateSeenValView.frame.size.width)/4 - 2,20))
                hairColorValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                hairColorValue.font = hairColorValue.font.fontWithSize(13)
                hairColorValue.numberOfLines = 0
                HairColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("HairColor")?.objectForKey("text") as? String)!
                if(HairColor == "?")
                {
                    HairColor = "-"
                }
                hairColorValue.text = HairColor
                hairColorValue.sizeToFit()
                
                //weight value
                var weightVal = UILabel(frame: CGRectMake((dateSeenValView.frame.size.width/4)*3, 0, (dateSeenValView.frame.size.width)/4 - 2, 20))
                weightVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                weightVal.font = weightVal.font.fontWithSize(13)
                let tempWt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text")
                if(tempWt != nil)
                {
                     weight = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text") as? String)!
                    if(weight == "0")
                    {
                        weight = "-"
                    }
                }
                else
                {
                    weight = "-"
                }
                weightVal.text = weight
                
                
                dateSeenValView.addSubview(dateSeenVal)
                dateSeenValView.addSubview(eyeColorValue)
                dateSeenValView.addSubview(hairColorValue)
                dateSeenValView.addSubview(weightVal)
                
                if(eyeColorValue.frame.size.height > hairColorValue.frame.size.height)
                {
                    dateSeenValView.frame = CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, eyeColorValue.frame.size.height)
                }
                else if(eyeColorValue.frame.size.height < hairColorValue.frame.size.height)
                {
                    dateSeenValView.frame = CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, hairColorValue.frame.size.height)
                }
                else
                {
                    dateSeenValView.frame = CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, hairColorValue.frame.size.height)
                }
                
                //countryLabelView
                
                var countryLabelViewY = dateSeenValY + dateSeenValView.frame.size.height + 4
                var countryLabelRowView = UIView(frame: CGRectMake(20, countryLabelViewY, self.view.frame.size.width - 40,50))
                
                //country label
                var countryLabel = UILabel(frame: CGRectMake(0, 0, (countryLabelRowView.frame.size.width)/4 - 2,20))
                countryLabel.text = "Country of Birth"
                countryLabel.numberOfLines = 0
                countryLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                countryLabel.font = UIFont.boldSystemFontOfSize(14)
                countryLabel.sizeToFit()
                
                //citizenship label
                var citizenshipLabel = UILabel(frame: CGRectMake(countryLabelRowView.frame.size.width/4, 0, (countryLabelRowView.frame.size.width/4)-2,20))
                citizenshipLabel.text = "Citizenship"
                citizenshipLabel.numberOfLines = 0
                citizenshipLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                citizenshipLabel.font = UIFont.boldSystemFontOfSize(14)
                citizenshipLabel.sizeToFit()
                
                //score Label
                var scoreLabel = UILabel(frame: CGRectMake(countryLabelRowView.frame.size.width/2, 0, (countryLabelRowView.frame.size.width/4)-2,20))
                scoreLabel.text = "Score"
                scoreLabel.numberOfLines = 0
                scoreLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                scoreLabel.font = UIFont.boldSystemFontOfSize(14)
                scoreLabel.sizeToFit()
                
                
                //EnrollGps Label
                var enrollGpsLabel = UILabel(frame: CGRectMake((countryLabelRowView.frame.size.width/4)*3, 0, (countryLabelRowView.frame.size.width/4)-2,30))
                enrollGpsLabel.text = "Enroll GPS Location"
                enrollGpsLabel.numberOfLines = 0
                enrollGpsLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                enrollGpsLabel.font = UIFont.boldSystemFontOfSize(14)
                enrollGpsLabel.sizeToFit()
                
                countryLabelRowView.addSubview(countryLabel)
                countryLabelRowView.addSubview(citizenshipLabel)
                countryLabelRowView.addSubview(scoreLabel)
                countryLabelRowView.addSubview(enrollGpsLabel)
                
                countryLabelRowView.frame = CGRectMake(20, countryLabelViewY, self.view.frame.size.width - 40,enrollGpsLabel.frame.size.height)
                
                
                //country value label
                var countryValY = countryLabelViewY + enrollGpsLabel.frame.size.height + 4
                var countryVal = UILabel(frame: CGRectMake(20, countryValY, (countryLabelRowView.frame.size.width)/4 - 2,20))
                countryVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                countryVal.font = countryVal.font.fontWithSize(13)
                countryVal.numberOfLines = 0
                birthPlace = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("PlaceOfBirth")?.objectForKey("text") as? String)!
                if(birthPlace == "?")
                {
                    birthPlace = "-"
                }
                countryVal.text = birthPlace
                countryVal.sizeToFit()
                
                //citizenship value
                var citizenshipVal = UILabel(frame: CGRectMake(20 + (countryLabelRowView.frame.size.width)/4,countryValY,(countryLabelRowView.frame.size.width)/4,20))
                citizenshipVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                citizenshipVal.font = citizenshipVal.font.fontWithSize(13)
                citizenshipVal.numberOfLines = 0
                
                citizen = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Citizenship")?.objectForKey("text") as? String)!
                if(citizen == "?")
                {
                    citizen = "-"
                }
                citizenshipVal.text = citizen
                citizenshipVal.sizeToFit()
                
                //score val
                var scoreVal = UILabel(frame: CGRectMake(20 + countryLabelRowView.frame.size.width/2, countryValY, (countryLabelRowView.frame.size.width)/4,20))
                
                scoreVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                scoreVal.font = scoreVal.font.fontWithSize(13)
                scoreVal.numberOfLines = 0
                
                let nlyScr = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("Score")?.objectForKey("text") as? String)!
                let algo = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("AlgorithmID")?.objectForKey("text") as? String)!
                score = "\(algo):\(nlyScr)"
                scoreVal.text = score
                scoreVal.sizeToFit()
                
                //enroll GPS button and title label
                var enrollValLbl = UILabel(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3,countryValY,(countryLabelRowView.frame.size.width)/4,35))
                enrollValLbl.numberOfLines = 0
                enrollValLbl.font = enrollValLbl.font.fontWithSize(11)

                var enrollGpsBtn = UIButton(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3,countryValY,(countryLabelRowView.frame.size.width)/4,35))
                enrollGpsBtn.titleLabel?.font = UIFont.systemFontOfSize(11)
                enrollGpsBtn.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
                enrollGpsBtn.titleLabel?.numberOfLines = 0
                let checkForGps = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("i:nil")
                if(checkForGps != nil)
                {
                    //googleURL = "No GPS available"
//                    enrollGpsBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
//                    enrollGpsBtn.setTitle("No GPS available", forState: .Normal)
                    enrollValLbl.textColor = UIColor.darkGrayColor()
                    enrollValLbl.text = "No GPS available"

                }
                else
                {
                    latDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("text") as? String)!

                    latMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeMinute")?.objectForKey("text") as? String)!

                    latSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeSecond")?.objectForKey("text") as? String)!
                    
                    longDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LongitudeDegree")?.objectForKey("text") as? String)!
                    
                    longMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LongitudeMinute")?.objectForKey("text") as? String)!
                    
                    longSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LongitudeSecond")?.objectForKey("text") as? String)!
                    
                        if(latDeg == "?")
                         {
//                            enrollGpsBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
//                            enrollGpsBtn.setTitle("No GPS available", forState: .Normal)
                            enrollValLbl.textColor = UIColor.darkGrayColor()
                            enrollValLbl.text = "No GPS available"
                            enrollGpsBtn.tag = 420
                            }
                          else
                          {
                            var lattitude1 = Double(latDeg)!+(Double(latMin)!/60.0)+(Double(latSec)!/3600.0)
                            var longitude1 = Double(longDeg)!+(Double(longMin)!/60.0)+(Double(longSec)!/3600.0)
                            if(lattitude1 > 180)
                            {
                               lattitude1 = lattitude1 - 360
                             }
                            if(longitude1 > 180)
                            {
                               longitude1 = longitude1 - 360
                             }
                    
                            enrollGpsBtn.tag = 520
//                            enrollGpsBtn.setTitleColor(UIColor.blueColor(), forState: .Normal)
//                            enrollGpsBtn.setTitle("\(lattitude1) \(longitude1)", forState: .Normal)
                            enrollValLbl.textColor = UIColor.blueColor()
                            enrollValLbl.text = "\(lattitude1) \(longitude1)"
                            enrollGpsBtn.accessibilityIdentifier = String(indexPath.row)
                            enrollGpsBtn.addTarget(self, action: Selector("gpsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                            }
                }
                //enrollGpsBtn.sizeToFit()
                enrollValLbl.sizeToFit()
                enrollGpsBtn.layoutIfNeeded()
                enrollGpsBtn.frame = CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3,countryValY,(countryLabelRowView.frame.size.width)/4, enrollValLbl.frame.size.height)
                
                //search GPS label row
                var searchGpsLocY = CGFloat()
                if((countryVal.frame.size.height >= citizenshipVal.frame.size.height) && (countryVal.frame.size.height >= scoreVal.frame.size.height) && (countryVal.frame.size.height >= enrollValLbl.frame.size.height))
                {
                    searchGpsLocY = countryValY + countryVal.frame.size.height + 4
                }
                else if((citizenshipVal.frame.size.height >= countryVal.frame.size.height) && (citizenshipVal.frame.size.height >= scoreVal.frame.size.height) && (citizenshipVal.frame.size.height >= enrollValLbl.frame.size.height))
                {
                    searchGpsLocY = countryValY + citizenshipVal.frame.size.height + 4
                }
                else if((scoreVal.frame.size.height >= countryVal.frame.size.height) && (scoreVal.frame.size.height >= citizenshipVal.frame.size.height) && (scoreVal.frame.size.height >= enrollValLbl.frame.size.height))
                {
                    searchGpsLocY = countryValY + scoreVal.frame.size.height + 4
                }
                else
                {
                    searchGpsLocY = countryValY + enrollValLbl.frame.size.height + 4
                }
                
                //date searched Label
                var dateSearchedLabel = UILabel(frame: CGRectMake(20 + countryLabelRowView.frame.size.width/2, searchGpsLocY, (countryLabelRowView.frame.size.width)/4,20))
                dateSearchedLabel.text = "Date seen"
                dateSearchedLabel.numberOfLines = 0
                dateSearchedLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                dateSearchedLabel.font = UIFont.boldSystemFontOfSize(14)
                dateSearchedLabel.sizeToFit()
                
                //search GPS location
                var searchGpsLocLabel = UILabel(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocY, (countryLabelRowView.frame.size.width)/4,20))
                searchGpsLocLabel.text = "Search GPS Location"
                searchGpsLocLabel.numberOfLines = 0
                searchGpsLocLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                searchGpsLocLabel.font = UIFont.boldSystemFontOfSize(14)
                searchGpsLocLabel.sizeToFit()
                
                //search GPS row value
                var searchGpsLocValY = searchGpsLocY + searchGpsLocLabel.frame.size.height + 4
                
                //date searched
                var dateSearchedVal = UILabel(frame: CGRectMake(20 + countryLabelRowView.frame.size.width/2, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4, 20))
                dateSearchedVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                dateSearchedVal.font = dateSearchedVal.font.fontWithSize(10)
                dateSearchedVal.text = convertedDate
                
                //search GPS Location and title label
                var searchValLbl = UILabel(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4,40))
                searchValLbl.numberOfLines = 0
                searchValLbl.font = searchValLbl.font.fontWithSize(11)
                
                var searchGpsLocation = UIButton(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4,40))
                searchGpsLocation.titleLabel?.font = UIFont.systemFontOfSize(11)
                searchGpsLocation.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
                searchGpsLocation.titleLabel?.numberOfLines = 0
                //searchGpsLocation.backgroundColor = UIColor.yellowColor()
                
                if(lattitudeSearch == 0 && longitudeSearch == 0)
                {
//                    searchGpsLocation.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
//                    searchGpsLocation.setTitle("No GPS available", forState: .Normal)
                    searchValLbl.textColor = UIColor.darkGrayColor()
                    searchValLbl.text = "No GPS available"
                }
                else
                {
//                    searchGpsLocation.setTitleColor(UIColor.blueColor(), forState: .Normal)
//                    searchGpsLocation.setTitle("\(lattitudeSearch) \((longitudeSearch))", forState: .Normal)
                    searchValLbl.textColor = UIColor.blueColor()
                    searchValLbl.text = "\(lattitudeSearch) \((longitudeSearch))"
                    searchGpsLocation.addTarget(self, action: Selector("gpsSearchButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                }
                searchValLbl.sizeToFit()
                searchGpsLocation.layoutIfNeeded()
                searchGpsLocation.frame = CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4,searchValLbl.frame.size.height)
                
                //sign y value
                var signImageY = searchGpsLocValY + searchValLbl.frame.size.height + 4
                
                // sign image
                var signImage = UIImageView(frame: CGRectMake(20, signImageY,(self.view.frame.width/2)-30, 80))
                
                let checkSign = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")
                if(checkSign != nil){
                    // sign is available
                    let sign_img = decodeBase64ToImage((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")?.objectForKey("text") as? String)!)
                    signImage.image = sign_img
                }
                else
                {
                    // no sign available
                    signImage.image = UIImage(named:"noImage")
                }
                //signImage.backgroundColor = UIColor.blueColor()
                signImage.contentMode = .ScaleAspectFit
                signImage.frame = CGRectMake(20, signImageY,(self.view.frame.width/2)-30, ((self.view.frame.width/2)-30)/1.6)
                
                // email button
                var emailButton = UIButton(frame: CGRectMake((self.view.frame.width/2)+(self.view.frame.width/8) - 25, signImageY + ((signImage.frame.size.height/2)-25), 50, 50))
                emailButton.setBackgroundImage(UIImage(named: "bigAlert"), forState: .Normal)
                emailButton.accessibilityIdentifier = String(indexPath.row)
                emailButton.addTarget(self, action: Selector("mailButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                
                
                //close Button
                var closeButton = UIButton(frame: CGRectMake((self.view.frame.size.width/4)*3 + (self.view.frame.width/8) - 25, signImageY + ((signImage.frame.size.height/2)-25), 50, 50))
                
                closeButton.setBackgroundImage(UIImage(named: "bigCross"), forState: .Normal)
                closeButton.accessibilityIdentifier = String(indexPath.row)
                closeButton.addTarget(self, action: Selector("cancelButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                
                var divider = UIView(frame: CGRectMake(0,signImageY+signImage.frame.size.height + 8,self.view.frame.size.width,2))
                divider.backgroundColor = UIColor.blackColor()
                
                cell.contentView.addSubview(searchLabel)
                cell.contentView.addSubview(matchLabel)
                cell.contentView.addSubview(searchImage)
                cell.contentView.addSubview(matchImage)
                cell.contentView.addSubview(firstNameLabel)
                cell.contentView.addSubview(firstNameVal)
                cell.contentView.addSubview(lastNameLabel)
                cell.contentView.addSubview(lastNameVal)
                cell.contentView.addSubview(dobRowView)
                cell.contentView.addSubview(dobValRowView)
                cell.contentView.addSubview(dateSeenRowView)
                cell.contentView.addSubview(dateSeenValView)
                cell.contentView.addSubview(countryLabelRowView)
                cell.contentView.addSubview(countryVal)
                cell.contentView.addSubview(citizenshipVal)
                cell.contentView.addSubview(scoreVal)
                cell.contentView.addSubview(enrollValLbl)
                cell.contentView.addSubview(enrollGpsBtn)
                cell.contentView.addSubview(dateSearchedLabel)
                cell.contentView.addSubview(searchGpsLocLabel)
                cell.contentView.addSubview(dateSearchedVal)
                cell.contentView.addSubview(searchValLbl)
                cell.contentView.addSubview(searchGpsLocation)
                cell.contentView.addSubview(signImage)
                cell.contentView.addSubview(emailButton)
                cell.contentView.addSubview(closeButton)
                cell.contentView.addSubview(divider)

            }
            
            
            
//    {
//            print("iPhone 4 or 4S")
//            let cell = tableView.dequeueReusableCellWithIdentifier("matchIphone4", forIndexPath: indexPath) as! MatchTableViewCell
//            cell.selectionStyle = UITableViewCellSelectionStyle.None
//            dateFormatter.dateFormat = "MM/dd/yyyy"
//            convertedDate = dateFormatter.stringFromDate(date)
//            if((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))!.isKindOfClass(NSDictionary))
//            {
//                //print("only one match")
//                Fname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("FirstName")?.objectForKey("text") as? String)!
//                if(Fname == "?")
//                {
//                    Fname = "-"
//                }
//
//                cell.firstNameLabel.text = Fname
//                lname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LastName")?.objectForKey("text") as? String)!
//                if(lname == "?")
//                {
//                    lname = "-"
//                }
//                cell.lastNameLabel.text = lname
//                let check1 = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("i:nil")
//                if (check1 != nil)
//                {
//                    age = "-"
//                    tempAge = "-"
//                }
//                else
//                {
//                    let date = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("text") as? String
//                    age = date!
//                    let dateFormatter = NSDateFormatter()
//                    dateFormatter.dateFormat =  "MM/dd/yyyy"
//                    let dateToSave =  dateFormatter.dateFromString(date!)
//                    tempAge  = String(calculateAge(dateToSave!))
//                    tempAge = "\(tempAge) Years"
//                    //print(tempAge)
//                    if(tempAge == "0 Years"){
//                        tempAge = String(calculateAgeInMonths(dateToSave!))
//                        tempAge = "\(tempAge) Months"
//                        //print(tempAge)
//                        if(tempAge == "0 Months") {
//                            tempAge = String(calculateAgeInDays(dateToSave!))
//                            tempAge = "\(tempAge) Days"
//                            //print(tempAge)
//                        }
//                    }
//                }
//                cell.ageLabel.text = age
//                cell.perfectAge.text = tempAge
//                birthPlace = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("PlaceOfBirth")?.objectForKey("text") as? String)!
//                if(birthPlace == "?")
//                {
//                    birthPlace = "-"
//                }
//                cell.countryLabel.text = birthPlace
//                if (deviceType == "iPhone") {
//                    let oldBounds:CGRect = cell.countryLabel.bounds
//                    cell.countryLabel.sizeToFit()
//                    cell.countryLabel.frame.size.width = oldBounds.size.width
//                }
//                citizen = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Citizenship")?.objectForKey("text") as? String)!
//                if(citizen == "?")
//                {
//                    citizen = "-"
//                }
//                cell.citizenLabel.text = citizen
//                if (deviceType == "iPhone") {
//                    let oldBounds:CGRect = cell.citizenLabel.bounds
//                    cell.citizenLabel.sizeToFit()
//                    cell.citizenLabel.frame.size.width = oldBounds.size.width
//                    
//                }
//                
//                gender = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Gender")?.objectForKey("text") as? String)!
//                if(gender == "?")
//                {
//                    gender = "-"
//                }
//                cell.sexLabel.text = gender
//                if (deviceType == "iPhone") {
//                    let oldBounds:CGRect = cell.sexLabel.bounds
//                    cell.sexLabel.sizeToFit()
//                    cell.sexLabel.frame.size.width = oldBounds.size.width
//                }
//                let tempHt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text")
//                if(tempHt != nil)
//                {
//                    height = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text") as? String)!
//                    if(height == "0")
//                    {
//                        height = "-"
//                    }
//                    else
//                    {
//                        if(height.characters.contains("0"))
//                        {
//                        height = height.stringByReplacingCharactersInRange(height.startIndex.successor()..<height.startIndex.successor().successor(), withString: "'")
//                        height = height+"\""
//                        }
//                    }
//                    
//                }
//                else
//                {
//                    height = "-"
//                }
//                cell.heightLabel.text = height
//                
//                let tempWt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text")
//                if(tempWt != nil)
//                {
//                    weight = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text") as? String)!
//                    if(weight == "0"){
//                        weight = "-"
//                    }
//                }
//                else
//                {
//                    weight = "-"
//                }
//                
//                
//                cell.weightLabel.text = weight
//                HairColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("HairColor")?.objectForKey("text") as? String)!
//                if(HairColor == "?")
//                {
//                    HairColor = "-"
//                }
//                cell.hairLabel.text = HairColor
//                if (deviceType == "iPhone") {
//                    let oldBounds:CGRect = cell.hairLabel.bounds
//                    cell.hairLabel.sizeToFit()
//                    cell.hairLabel.frame.size.width = oldBounds.size.width
//                }
//                EyeColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("EyeColor")?.objectForKey("text") as? String)!
//                if(EyeColor == "?")
//                {
//                    EyeColor = "-"
//                }
//                cell.eyeLabel.text = EyeColor
//                if (deviceType == "iPhone") {
//                    let oldBounds:CGRect = cell.eyeLabel.bounds
//                    cell.eyeLabel.sizeToFit()
//                    cell.eyeLabel.frame.size.width = oldBounds.size.width
//                }
//                let nlyScr = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("Score")?.objectForKey("text") as? String)!
//                let algo = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("AlgorithmID")?.objectForKey("text") as? String)!
//                score = "\(algo):\(nlyScr)"
//                cell.scoreLabel.text = score
//                cell.dateSearchedLbl.text = convertedDate
//                enroll_dt = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("EnrollDate")?.objectForKey("text") as? String)!
//                cell.dtSeenLabel.text = enroll_dt
//                if(lattitudeSearch == 0 && longitudeSearch == 0){
//                    cell.gpsSearched.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
//                    cell.gpsSearched.setTitle("No GPS available", forState: .Normal)
//                }
//                else
//                {
//                    cell.gpsSearched.setTitleColor(UIColor.blueColor(), forState: .Normal)
//                    cell.gpsSearched.setTitle("\(lattitudeSearch) \((longitudeSearch))", forState: .Normal)
//                    cell.gpsSearched.addTarget(self, action: Selector("gpsSearchButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
//                }
//                if (deviceType == "iPhone") {
//                    cell.gpsSearched.sizeToFit()
//                }
//                
//                let checkSign = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")
//                if(checkSign != nil){
//                    // sign is available
//                    let sign_img = decodeBase64ToImage((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")?.objectForKey("text") as? String)!)
//                    cell.signImage.image = sign_img
//                }
//                else{
//                    // no sign available
//                    cell.signImage.image = UIImage(named:"noImage")
//                }
//                
//                cell.searchImage.image = imageToBeSearchedAndMatch
//                let image = decodeBase64ToImage(((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("FaceImage")?.objectForKey("text")) as? String)!)
//                
//                cell.matchImage.image = image
//                
//                let checkForGps = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("i:nil")
//                if(checkForGps != nil || latDeg == "?")
//                {
//                    //googleURL = "No GPS available"
//                    cell.gpsBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
//                    cell.gpsBtn.setTitle("No GPS available", forState: .Normal)
//                }
//                else
//                {
//                    cell.gpsBtn.setTitleColor(UIColor.blueColor(), forState: .Normal)
//                    cell.gpsBtn.setTitle("\(lattitude) \(longitude)", forState: .Normal)
//                    if (deviceType == "iPhone") {
//                        cell.gpsBtn.sizeToFit()
//                    }
//                    cell.gpsBtn.accessibilityIdentifier = String(indexPath.row)
//                    cell.gpsBtn.addTarget(self, action: Selector("gpsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
//                }
//            }
//            else
//            {
//                //print("multiple matches")
//                
//                Fname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("FirstName")?.objectForKey("text") as? String)!
//                if(Fname == "?")
//                {
//                    Fname = "-"
//                }
//                cell.firstNameLabel.text = Fname
//                lname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LastName")?.objectForKey("text") as? String)!
//                if(lname == "?")
//                {
//                    lname = "-"
//                }
//                cell.lastNameLabel.text = lname
//                let check1 = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("i:nil")
//                if (check1 != nil)
//                {
//                    age = "-"
//                    tempAge = "-"
//                }
//                else
//                {
//                    let date = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("text") as? String
//                    
//                    age = date!
//                    let dateFormatter = NSDateFormatter()
//                    dateFormatter.dateFormat =  "MM/dd/yyyy"
//                    let dateToSave =  dateFormatter.dateFromString(date!)
//                    tempAge  = String(calculateAge(dateToSave!))
//                    tempAge = "\(tempAge) Years"
//                    //print(tempAge)
//                    if(tempAge == "0 Years"){
//                        tempAge = String(calculateAgeInMonths(dateToSave!))
//                        tempAge = "\(tempAge) Months"
//                        //print(tempAge)
//                        if(tempAge == "0 Months") {
//                            tempAge = String(calculateAgeInDays(dateToSave!))
//                            tempAge = "\(tempAge) Days"
//                            //print(tempAge)
//                        }
//                    }
//                }
//                cell.ageLabel.text = age
//                cell.perfectAge.text = tempAge
//                birthPlace = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("PlaceOfBirth")?.objectForKey("text") as? String)!
//                if(birthPlace == "?")
//                {
//                    birthPlace = "-"
//                }
//                cell.countryLabel.text = birthPlace
//                if (deviceType == "iPhone") {
//                    let oldBounds:CGRect = cell.countryLabel.bounds
//                    cell.countryLabel.sizeToFit()
//                    cell.countryLabel.frame.size.width = oldBounds.size.width
//                }
//                citizen = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Citizenship")?.objectForKey("text") as? String)!
//                if(citizen == "?")
//                {
//                    citizen = "-"
//                }
//                cell.citizenLabel.text = citizen
//                if (deviceType == "iPhone") {
//                    
//                    let oldBounds:CGRect = cell.citizenLabel.bounds
//                    cell.citizenLabel.sizeToFit()
//                    //var newBounds:CGRect = cell.citizenLabel.bounds
//                    // newBounds.size.width = oldBounds.size.width
//                    cell.citizenLabel.frame.size.width = oldBounds.size.width
//                    
//                }
//                gender = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Gender")?.objectForKey("text") as? String)!
//                if(gender == "?")
//                {
//                    gender = "-"
//                }
//                cell.sexLabel.text = gender
//                if (deviceType == "iPhone") {
//                    let oldBounds:CGRect = cell.sexLabel.bounds
//                    cell.sexLabel.sizeToFit()
//                    //var newBounds:CGRect = cell.sexLabel.bounds
//                    // newBounds.size.width = oldBounds.size.width
//                    cell.sexLabel.frame.size.width = oldBounds.size.width
//                }
//                
//                let tempHt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text")
//                if(tempHt != nil)
//                {
//                    height = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text") as? String)!
//                    if(height == "0")
//                    {
//                        height = "-"
//                    }
//                    else
//                    {
//                        if(height.characters.contains("0"))
//                        {
//
//                        height = height.stringByReplacingCharactersInRange(height.startIndex.successor()..<height.startIndex.successor().successor(), withString: "'")
//                        height = height+"\""
//                        }
//                    }
//                    
//                }
//                else
//                {
//                    height = "-"
//                }
//                cell.heightLabel.text = height
//                
//                let tempWt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text")
//                if(tempWt != nil)
//                {
//                    weight = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text") as? String)!
//                    if(weight == "0"){
//                        weight = "-"
//                    }
//                }
//                else
//                {
//                    weight = "-"
//                }
//                cell.weightLabel.text = weight
//                
//                HairColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("HairColor")?.objectForKey("text") as? String)!
//                if(HairColor == "?")
//                {
//                    HairColor = "-"
//                }
//                cell.hairLabel.text = HairColor
//                if (deviceType == "iPhone") {
//                    let oldBounds:CGRect = cell.hairLabel.bounds
//                    cell.hairLabel.sizeToFit()
//                    cell.hairLabel.frame.size.width = oldBounds.size.width
//                    
//                }
//                
//                EyeColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("EyeColor")?.objectForKey("text") as? String)!
//                if(EyeColor == "?")
//                {
//                    EyeColor = "-"
//                }
//                cell.eyeLabel.text = EyeColor
//                if (deviceType == "iPhone") {
//                    let oldBounds:CGRect = cell.eyeLabel.bounds
//                    cell.eyeLabel.sizeToFit()
//                    cell.eyeLabel.frame.size.width = oldBounds.size.width
//                }
//                let nlyScr = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("Score")?.objectForKey("text") as? String)!
//                let algo = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("AlgorithmID")?.objectForKey("text") as? String)!
//                score = "\(algo):\(nlyScr)"
//                cell.scoreLabel.text = score
//                cell.dateSearchedLbl.text = convertedDate
//                enroll_dt = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("EnrollDate")?.objectForKey("text") as? String)!
//                cell.dtSeenLabel.text = enroll_dt
//                if(lattitudeSearch == 0 && longitudeSearch == 0){
//                    cell.gpsSearched.setTitle("No GPS available", forState: .Normal)
//                }
//                else
//                {
//                    cell.gpsSearched.setTitleColor(UIColor.blueColor(), forState: .Normal)
//                    cell.gpsSearched.setTitle("\(lattitudeSearch) \(longitudeSearch)", forState: .Normal)
//                    cell.gpsSearched.addTarget(self, action: Selector("gpsSearchButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
//                }
//                //        if (deviceType == "iPhone") {
//                //            cell.gpsSearched.sizeToFit()
//                //        }
//                let checkSign = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")
//                if(checkSign != nil){
//                    // sign is available
//                    let sign_img = decodeBase64ToImage((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")?.objectForKey("text") as? String)!)
//                    cell.signImage.image = sign_img
//                }
//                else{
//                    // no sign available
//                    cell.signImage.image = UIImage(named:"noImage")
//                }
//                
//                cell.searchImage.image = imageToBeSearchedAndMatch
//                let image = decodeBase64ToImage(((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("FaceImage")?.objectForKey("text")) as? String)!)
//                //        let dataComp = UIImageJPEGRepresentation(image, 1)
//                //        let imageSize: Int = dataComp!.length
//                //        let img = Double(imageSize)/1024.0
//                //        //print(img)
//                
//                cell.matchImage.image = image
//                //let new = saveImg(image)
//                //let old = saveImg(imageToBeSearchedAndMatch)
//                
//                let checkForGps = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("i:nil")
//                if(checkForGps != nil)
//                {
//                    cell.gpsBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
//                    cell.gpsBtn.setTitle("No GPS available", forState: .Normal)
//                }
//                else{
//                    latDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("text") as? String)!
//                    latMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeMinute")?.objectForKey("text") as? String)!
//                    latSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeSecond")?.objectForKey("text") as? String)!
//                    longDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LongitudeDegree")?.objectForKey("text") as? String)!
//                    longMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LongitudeMinute")?.objectForKey("text") as? String)!
//                    longSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LongitudeSecond")?.objectForKey("text") as? String)!
//                    if(latDeg == "?")
//                    {
//                        //cell.gpsBtn = nil
//                        cell.gpsBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
//                        cell.gpsBtn.setTitle("No GPS available", forState: .Normal)
//                        cell.gpsBtn.tag = 420
//                    }
//                    else
//                    {
//                        var lattitude1 = Double(latDeg)!+(Double(latMin)!/60.0)+(Double(latSec)!/3600.0)
//                        var longitude1 = Double(longDeg)!+(Double(longMin)!/60.0)+(Double(longSec)!/3600.0)
//                        if(lattitude1 > 180){
//                            lattitude1 = lattitude1 - 360
//                        }
//                        if(longitude1 > 180){
//                            longitude1 = longitude1 - 360
//                        }
//                        
//                        cell.gpsBtn.tag = 520
//                        cell.gpsBtn.setTitleColor(UIColor.blueColor(), forState: .Normal)
//                        cell.gpsBtn.setTitle("\(lattitude1) \(longitude1)", forState: .Normal)
//                        //                if (deviceType == "iPhone") {
//                        //                    cell.gpsBtn.sizeToFit()
//                        //                }
//                        cell.gpsBtn.accessibilityIdentifier = String(indexPath.row)
//                        cell.gpsBtn.addTarget(self, action: Selector("gpsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
//                    }
//                }
//            }
//            
//            cell.cancelBtn.accessibilityIdentifier = String(indexPath.row)
//            cell.cancelBtn.addTarget(self, action: Selector("cancelButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
//            cell.mailButton.accessibilityIdentifier = String(indexPath.row)
//            cell.mailButton.addTarget(self, action: Selector("mailButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
//          
            cell.contentView.backgroundColor = UIColor.init(red: 250/255.0, green: 250/255.0, blue: 250/255.0, alpha: 1)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("matchResult", forIndexPath: indexPath) as! MatchTableViewCell
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                dateFormatter.dateFormat = "MM/dd/yyyy"
                convertedDate = dateFormatter.stringFromDate(date)
                if((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))!.isKindOfClass(NSDictionary))
                {
                    //print("only one match")
                    Fname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("FirstName")?.objectForKey("text") as? String)!
                    if(Fname == "?")
                    {
                        Fname = "-"
                    }
                    
                    cell.firstNameLabel.text = Fname
                    lname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LastName")?.objectForKey("text") as? String)!
                    if(lname == "?")
                    {
                        lname = "-"
                    }
                    cell.lastNameLabel.text = lname
                    let check1 = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("i:nil")
                    if (check1 != nil)
                    {
                        age = "-"
                        tempAge = "-"
                    }
                    else
                    {
                        let date = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("text") as? String
                        age = date!
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat =  "MM/dd/yyyy"
                        let dateToSave =  dateFormatter.dateFromString(date!)
                        tempAge  = String(calculateAge(dateToSave!))
                        tempAge = "\(tempAge) Years"
                        //print(tempAge)
                        if(tempAge == "0 Years"){
                            tempAge = String(calculateAgeInMonths(dateToSave!))
                            tempAge = "\(tempAge) Months"
                            //print(tempAge)
                            if(tempAge == "0 Months") {
                                tempAge = String(calculateAgeInDays(dateToSave!))
                                tempAge = "\(tempAge) Days"
                                //print(tempAge)
                            }
                        }
                    }
                    cell.ageLabel.text = age
                    cell.perfectAge.text = tempAge
                    birthPlace = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("PlaceOfBirth")?.objectForKey("text") as? String)!
                    if(birthPlace == "?")
                    {
                        birthPlace = "-"
                    }
                    cell.countryLabel.text = birthPlace
                    if (deviceType == "iPhone") {
                        let oldBounds:CGRect = cell.countryLabel.bounds
                        cell.countryLabel.sizeToFit()
                        cell.countryLabel.frame.size.width = oldBounds.size.width
                    }
                    citizen = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Citizenship")?.objectForKey("text") as? String)!
                    if(citizen == "?")
                    {
                        citizen = "-"
                    }
                    cell.citizenLabel.text = citizen
                    if (deviceType == "iPhone") {
                        let oldBounds:CGRect = cell.citizenLabel.bounds
                        cell.citizenLabel.sizeToFit()
                        cell.citizenLabel.frame.size.width = oldBounds.size.width
                        
                    }
                    
                    gender = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Gender")?.objectForKey("text") as? String)!
                    if(gender == "?")
                    {
                        gender = "-"
                    }
                    cell.sexLabel.text = gender
                    if (deviceType == "iPhone") {
                        let oldBounds:CGRect = cell.sexLabel.bounds
                        cell.sexLabel.sizeToFit()
                        cell.sexLabel.frame.size.width = oldBounds.size.width
                    }
                    let tempHt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text")
                    if(tempHt != nil)
                    {
                        height = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text") as? String)!
                        if(height == "0")
                        {
                            height = "-"
                        }
                        else
                        {
                            if(height.characters.contains("0"))
                            {

                            height = height.stringByReplacingCharactersInRange(height.startIndex.successor()..<height.startIndex.successor().successor(), withString: "'")
                            height = height+"\""
                            }
                        }
                        
                    }
                    else
                    {
                        height = "-"
                    }
                    cell.heightLabel.text = height
                    
                    let tempWt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text")
                    if(tempWt != nil)
                    {
                        weight = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text") as? String)!
                        if(weight == "0"){
                            weight = "-"
                        }
                    }
                    else
                    {
                        weight = "-"
                    }
                    
                    
                    cell.weightLabel.text = weight
                    HairColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("HairColor")?.objectForKey("text") as? String)!
                    if(HairColor == "?")
                    {
                        HairColor = "-"
                    }
                    cell.hairLabel.text = HairColor
                    if (deviceType == "iPhone") {
                        let oldBounds:CGRect = cell.hairLabel.bounds
                        cell.hairLabel.sizeToFit()
                        cell.hairLabel.frame.size.width = oldBounds.size.width
                    }
                    EyeColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("EyeColor")?.objectForKey("text") as? String)!
                    if(EyeColor == "?")
                    {
                        EyeColor = "-"
                    }
                    cell.eyeLabel.text = EyeColor
                    if (deviceType == "iPhone") {
                        let oldBounds:CGRect = cell.eyeLabel.bounds
                        cell.eyeLabel.sizeToFit()
                        cell.eyeLabel.frame.size.width = oldBounds.size.width
                    }
                    let nlyScr = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("Score")?.objectForKey("text") as? String)!
                    let algo = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("AlgorithmID")?.objectForKey("text") as? String)!
                    score = "\(algo):\(nlyScr)"
                    cell.scoreLabel.text = score
                    cell.dateSearchedLbl.text = convertedDate
                    enroll_dt = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("EnrollDate")?.objectForKey("text") as? String)!
                    cell.dtSeenLabel.text = enroll_dt
                    if(lattitudeSearch == 0 && longitudeSearch == 0){
                        cell.gpsSearched.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
                        cell.gpsSearched.setTitle("No GPS available", forState: .Normal)
                    }
                    else
                    {
                        cell.gpsSearched.setTitleColor(UIColor.blueColor(), forState: .Normal)
                        cell.gpsSearched.setTitle("\(lattitudeSearch) \((longitudeSearch))", forState: .Normal)
                        cell.gpsSearched.addTarget(self, action: Selector("gpsSearchButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                    }
                    if (deviceType == "iPhone") {
                        cell.gpsSearched.sizeToFit()
                    }
                    
                    let checkSign = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")
                    if(checkSign != nil){
                        // sign is available
                        let sign_img = decodeBase64ToImage((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")?.objectForKey("text") as? String)!)
                        cell.signImage.image = sign_img
                    }
                    else{
                        // no sign available
                        cell.signImage.image = UIImage(named:"noImage")
                    }
                    
                    cell.searchImage.image = imageToBeSearchedAndMatch
                    let image = decodeBase64ToImage(((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("FaceImage")?.objectForKey("text")) as? String)!)
                    
                    cell.matchImage.image = image
                    
                    let checkForGps = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("i:nil")
                    if(checkForGps != nil || latDeg == "?")
                    {
                        //googleURL = "No GPS available"
                        cell.gpsBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
                        cell.gpsBtn.setTitle("No GPS available", forState: .Normal)
                    }
                    else
                    {
                        cell.gpsBtn.setTitleColor(UIColor.blueColor(), forState: .Normal)
                        cell.gpsBtn.setTitle("\(lattitude) \(longitude)", forState: .Normal)
                        if (deviceType == "iPhone") {
                            cell.gpsBtn.sizeToFit()
                        }
                        cell.gpsBtn.accessibilityIdentifier = String(indexPath.row)
                        cell.gpsBtn.addTarget(self, action: Selector("gpsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                    }
                }
                else
                {
                    //print("multiple matches")
                    
                    Fname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("FirstName")?.objectForKey("text") as? String)!
                    if(Fname == "?")
                    {
                        Fname = "-"
                    }
                    cell.firstNameLabel.text = Fname
                    lname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LastName")?.objectForKey("text") as? String)!
                    if(lname == "?")
                    {
                        lname = "-"
                    }
                    cell.lastNameLabel.text = lname
                    let check1 = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("i:nil")
                    if (check1 != nil)
                    {
                        age = "-"
                        tempAge = "-"
                    }
                    else
                    {
                        let date = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("text") as? String
                        
                        age = date!
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat =  "MM/dd/yyyy"
                        let dateToSave =  dateFormatter.dateFromString(date!)
                        tempAge  = String(calculateAge(dateToSave!))
                        tempAge = "\(tempAge) Years"
                        //print(tempAge)
                        if(tempAge == "0 Years"){
                            tempAge = String(calculateAgeInMonths(dateToSave!))
                            tempAge = "\(tempAge) Months"
                            //print(tempAge)
                            if(tempAge == "0 Months") {
                                tempAge = String(calculateAgeInDays(dateToSave!))
                                tempAge = "\(tempAge) Days"
                                //print(tempAge)
                            }
                        }
                    }
                    cell.ageLabel.text = age
                    cell.perfectAge.text = tempAge
                    birthPlace = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("PlaceOfBirth")?.objectForKey("text") as? String)!
                    if(birthPlace == "?")
                    {
                        birthPlace = "-"
                    }
                    cell.countryLabel.text = birthPlace
                    if (deviceType == "iPhone") {
                        let oldBounds:CGRect = cell.countryLabel.bounds
                        cell.countryLabel.sizeToFit()
                        cell.countryLabel.frame.size.width = oldBounds.size.width
                    }
                    citizen = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Citizenship")?.objectForKey("text") as? String)!
                    if(citizen == "?")
                    {
                        citizen = "-"
                    }
                    cell.citizenLabel.text = citizen
                    if (deviceType == "iPhone") {
                        
                        let oldBounds:CGRect = cell.citizenLabel.bounds
                        cell.citizenLabel.sizeToFit()
                        //var newBounds:CGRect = cell.citizenLabel.bounds
                        // newBounds.size.width = oldBounds.size.width
                        cell.citizenLabel.frame.size.width = oldBounds.size.width
                        
                    }
                    gender = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Gender")?.objectForKey("text") as? String)!
                    if(gender == "?")
                    {
                        gender = "-"
                    }
                    cell.sexLabel.text = gender
                    if (deviceType == "iPhone") {
                        let oldBounds:CGRect = cell.sexLabel.bounds
                        cell.sexLabel.sizeToFit()
                        //var newBounds:CGRect = cell.sexLabel.bounds
                        // newBounds.size.width = oldBounds.size.width
                        cell.sexLabel.frame.size.width = oldBounds.size.width
                    }
                    
                    let tempHt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text")
                    if(tempHt != nil)
                    {
                        height = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text") as? String)!
                        if(height == "0")
                        {
                            height = "-"
                        }
                        else
                        {
                            if(height.characters.contains("0"))
                            {
                            height = height.stringByReplacingCharactersInRange(height.startIndex.successor()..<height.startIndex.successor().successor(), withString: "'")
                            height = height+"\""
                            }
                        }
                        
                    }
                    else
                    {
                        height = "-"
                    }
                    cell.heightLabel.text = height
                    
                    let tempWt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text")
                    if(tempWt != nil)
                    {
                        weight = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text") as? String)!
                        if(weight == "0"){
                            weight = "-"
                        }
                    }
                    else
                    {
                        weight = "-"
                    }
                    cell.weightLabel.text = weight
                    
                    HairColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("HairColor")?.objectForKey("text") as? String)!
                    if(HairColor == "?")
                    {
                        HairColor = "-"
                    }
                    cell.hairLabel.text = HairColor
                    if (deviceType == "iPhone") {
                        let oldBounds:CGRect = cell.hairLabel.bounds
                        cell.hairLabel.sizeToFit()
                        cell.hairLabel.frame.size.width = oldBounds.size.width
                        
                    }
                    
                    EyeColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("EyeColor")?.objectForKey("text") as? String)!
                    if(EyeColor == "?")
                    {
                        EyeColor = "-"
                    }
                    cell.eyeLabel.text = EyeColor
                    if (deviceType == "iPhone") {
                        let oldBounds:CGRect = cell.eyeLabel.bounds
                        cell.eyeLabel.sizeToFit()
                        cell.eyeLabel.frame.size.width = oldBounds.size.width
                    }
                    let nlyScr = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("Score")?.objectForKey("text") as? String)!
                    let algo = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("AlgorithmID")?.objectForKey("text") as? String)!
                    score = "\(algo):\(nlyScr)"
                    cell.scoreLabel.text = score
                    cell.dateSearchedLbl.text = convertedDate
                    enroll_dt = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("EnrollDate")?.objectForKey("text") as? String)!
                    cell.dtSeenLabel.text = enroll_dt
                    if(lattitudeSearch == 0 && longitudeSearch == 0){
                        cell.gpsSearched.setTitle("No GPS available", forState: .Normal)
                    }
                    else
                    {
                        cell.gpsSearched.setTitleColor(UIColor.blueColor(), forState: .Normal)
                        cell.gpsSearched.setTitle("\(lattitudeSearch) \(longitudeSearch)", forState: .Normal)
                        cell.gpsSearched.addTarget(self, action: Selector("gpsSearchButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                    }
                    //        if (deviceType == "iPhone") {
                    //            cell.gpsSearched.sizeToFit()
                    //        }
                    let checkSign = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")
                    if(checkSign != nil){
                        // sign is available
                        let sign_img = decodeBase64ToImage((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")?.objectForKey("text") as? String)!)
                        cell.signImage.image = sign_img
                    }
                    else{
                        // no sign available
                        cell.signImage.image = UIImage(named:"noImage")
                    }
                    
                    cell.searchImage.image = imageToBeSearchedAndMatch
                    let image = decodeBase64ToImage(((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("FaceImage")?.objectForKey("text")) as? String)!)
                    //        let dataComp = UIImageJPEGRepresentation(image, 1)
                    //        let imageSize: Int = dataComp!.length
                    //        let img = Double(imageSize)/1024.0
                    //        //print(img)
                    
                    cell.matchImage.image = image
                    //let new = saveImg(image)
                    //let old = saveImg(imageToBeSearchedAndMatch)
                    
                    let checkForGps = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("i:nil")
                    if(checkForGps != nil)
                    {
                        cell.gpsBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
                        cell.gpsBtn.setTitle("No GPS available", forState: .Normal)
                    }
                    else{
                        latDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("text") as? String)!
                        latMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeMinute")?.objectForKey("text") as? String)!
                        latSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeSecond")?.objectForKey("text") as? String)!
                        longDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LongitudeDegree")?.objectForKey("text") as? String)!
                        longMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LongitudeMinute")?.objectForKey("text") as? String)!
                        longSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LongitudeSecond")?.objectForKey("text") as? String)!
                        if(latDeg == "?")
                        {
                            //cell.gpsBtn = nil
                            cell.gpsBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
                            cell.gpsBtn.setTitle("No GPS available", forState: .Normal)
                            cell.gpsBtn.tag = 420
                        }
                        else
                        {
                            var lattitude1 = Double(latDeg)!+(Double(latMin)!/60.0)+(Double(latSec)!/3600.0)
                            var longitude1 = Double(longDeg)!+(Double(longMin)!/60.0)+(Double(longSec)!/3600.0)
                            if(lattitude1 > 180){
                                lattitude1 = lattitude1 - 360
                            }
                            if(longitude1 > 180){
                                longitude1 = longitude1 - 360
                            }
                            
                            cell.gpsBtn.tag = 520
                            cell.gpsBtn.setTitleColor(UIColor.blueColor(), forState: .Normal)
                            cell.gpsBtn.setTitle("\(lattitude1) \(longitude1)", forState: .Normal)
                            //                if (deviceType == "iPhone") {
                            //                    cell.gpsBtn.sizeToFit()
                            //                }
                            cell.gpsBtn.accessibilityIdentifier = String(indexPath.row)
                            cell.gpsBtn.addTarget(self, action: Selector("gpsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                        }
                    }
                }
                
                cell.cancelBtn.accessibilityIdentifier = String(indexPath.row)
                cell.cancelBtn.addTarget(self, action: Selector("cancelButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                cell.mailButton.accessibilityIdentifier = String(indexPath.row)
                cell.mailButton.addTarget(self, action: Selector("mailButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                
                return cell
            }
    }
    
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let deviceType = UIDevice.currentDevice().model
        if (deviceType == "iPhone")
        {
            //print("iphone")
            let cell : UITableViewCell = UITableViewCell(style: .Default, reuseIdentifier: "dynamicCell")
            dateFormatter.dateFormat = "MM/dd/yyyy"
            convertedDate = dateFormatter.stringFromDate(date)
            
            if((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))!.isKindOfClass(NSDictionary))
            {
                var searchLabel = UILabel()
                var matchLabel = UILabel()
                var searchImage = UIImageView()
                var matchImage = UIImageView()
                
                searchLabel = UILabel(frame: CGRectMake(0, 8, self.view.frame.size.width/2, 20))
                
                matchLabel = UILabel(frame: CGRectMake(self.view.frame.size.width/2, 8, self.view.frame.size.width/2, 20))
                
                searchLabel.text = "Search"
                searchLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                searchLabel.font = UIFont.boldSystemFontOfSize(17)
                searchLabel.textAlignment = .Center
                matchLabel.text = "Match"
                matchLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                matchLabel.font = UIFont.boldSystemFontOfSize(17)
                matchLabel.textAlignment = .Center
                
                var searchImageY = searchLabel.frame.size.height + 16
                searchImage = UIImageView(frame: CGRectMake(10, searchImageY, (self.view.frame.width/2) - 20 , 90))
                matchImage = UIImageView(frame: CGRectMake((self.view.frame.width/2) + 10 , searchImageY, (self.view.frame.width/2) - 20, 90))
                searchImage.image = imageToBeSearchedAndMatch
                searchImage.contentMode = .ScaleAspectFit
                let image = decodeBase64ToImage(((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("FaceImage")?.objectForKey("text")) as? String)!)
                matchImage.image = image
                matchImage.contentMode = .ScaleAspectFit
                
                var firstNameY = searchImageY + searchImage.frame.size.height + 10
                var firstNameLabel = UILabel()
                firstNameLabel = UILabel(frame: CGRectMake(20, firstNameY, self.view.frame.size.width - 20, 20))
                firstNameLabel.text = "First Name"
                firstNameLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                firstNameLabel.font = UIFont.boldSystemFontOfSize(14)
                
                // first name value
                var firstNameValY = firstNameY + 22
                var firstNameVal = UILabel()
                firstNameVal = UILabel(frame: CGRectMake(20, firstNameValY, self.view.frame.size.width - 20, 20))
                firstNameVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                firstNameVal.font = firstNameVal.font.fontWithSize(14)
                
                Fname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("FirstName")?.objectForKey("text") as? String)!
                
                if(Fname == "?")
                {
                    Fname = "-"
                }
                firstNameVal.text = Fname
                
                // last name label
                var lastNameLabelY = firstNameValY + firstNameVal.frame.size.height + 5
                var lastNameLabel = UILabel(frame: CGRectMake(20, lastNameLabelY, self.view.frame.size.width - 20, 20))
                lastNameLabel.text = "Last Name"
                lastNameLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                lastNameLabel.font = UIFont.boldSystemFontOfSize(14)
                
                // last name value
                var lastNameValY = lastNameLabelY + 22
                var lastNameVal = UILabel(frame: CGRectMake(20, lastNameValY, self.view.frame.size.width - 20, 20))
                lastNameVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                lastNameVal.font = lastNameVal.font.fontWithSize(14)
                
                lname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LastName")?.objectForKey("text") as? String)!
                if(lname == "?")
                {
                    lname = "-"
                }
                lastNameVal.text = lname
                
                //dob row view
                var dobY = lastNameValY + 22
                var dobRowView = UIView(frame: CGRectMake(20, dobY, self.view.frame.size.width - 40, 50))
                
                //dob label
                var dobLabel = UILabel(frame: CGRectMake(0,0,(dobRowView.frame.size.width)/4 - 2 ,20))
                dobLabel.numberOfLines = 0
                dobLabel.text = "Date of Birth"
                dobLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                dobLabel.font = UIFont.boldSystemFontOfSize(14)
                dobLabel.sizeToFit()
                
                //age label
                var ageLabel = UILabel(frame: CGRectMake((dobRowView.frame.size.width)/4, 0, (dobRowView.frame.size.width)/4 - 2, 20))
                ageLabel.text = "Age"
                ageLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                ageLabel.font = UIFont.boldSystemFontOfSize(14)
                ageLabel.sizeToFit()
                
                //gender label
                var genderLabel = UILabel(frame: CGRectMake(dobRowView.frame.size.width/2, 0, (dobRowView.frame.size.width)/4 - 2, 20))
                genderLabel.text = "Gender"
                genderLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                genderLabel.font = UIFont.boldSystemFontOfSize(14)
                genderLabel.sizeToFit()
                
                //height label
                var heightLabel = UILabel(frame: CGRectMake((dobRowView.frame.size.width/4)*3, 0, (dobRowView.frame.size.width)/4 - 2, 20))
                heightLabel.text = "Height"
                heightLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                heightLabel.font = UIFont.boldSystemFontOfSize(14)
                heightLabel.sizeToFit()
                
                dobRowView.addSubview(dobLabel)
                dobRowView.addSubview(ageLabel)
                dobRowView.addSubview(genderLabel)
                dobRowView.addSubview(heightLabel)
                
                dobRowView.frame = CGRectMake(20, dobY, self.view.frame.size.width - 40, dobLabel.frame.size.height)
                
                //dob row value
                var dobValY = dobY + dobRowView.frame.size.height + 4
                
                var dobValRowView = UIView(frame: CGRectMake(20, dobValY, self.view.frame.size.width - 40, 50))
                
                //dob val
                var dobVal = UILabel(frame: CGRectMake(0,0,dobRowView.frame.size.width/4 - 2,20))
                dobVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                dobVal.font = dobVal.font.fontWithSize(10)
                
                let check1 = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("i:nil")
                if (check1 != nil)
                {
                    age = "-"
                    tempAge = "-"
                }
                else
                {
                    let date = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("text") as? String
                    age = date!
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat =  "MM/dd/yyyy"
                    let dateToSave =  dateFormatter.dateFromString(date!)
                    tempAge  = String(calculateAge(dateToSave!))
                    tempAge = "\(tempAge) Years"
                    //print(tempAge)
                    if(tempAge == "0 Years"){
                        tempAge = String(calculateAgeInMonths(dateToSave!))
                        tempAge = "\(tempAge) Months"
                        //print(tempAge)
                        if(tempAge == "0 Months") {
                            tempAge = String(calculateAgeInDays(dateToSave!))
                            tempAge = "\(tempAge) Days"
                            //print(tempAge)
                        }
                    }
                }
                
                dobVal.text = age
                dobVal.sizeToFit()
                
                //age val
                var ageVal = UILabel(frame: CGRectMake(dobRowView.frame.size.width/4, 0, dobRowView.frame.size.width/4 - 2, 20))
                ageVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                ageVal.font = ageVal.font.fontWithSize(13)
                ageVal.text = tempAge
                ageVal.sizeToFit()
                
                //gender value
                var genderVal = UILabel(frame: CGRectMake(dobRowView.frame.size.width/2, 0, dobRowView.frame.size.width/4 - 2, 20))
                genderVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                genderVal.font = genderVal.font.fontWithSize(13)
                genderVal.numberOfLines = 0
                gender = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Gender")?.objectForKey("text") as? String)!
                if(gender == "?")
                {
                    gender = "-"
                }
                genderVal.text = gender
                genderVal.sizeToFit()
                
                //height value
                var heightVal = UILabel(frame: CGRectMake((dobRowView.frame.size.width/4)*3, 0, dobRowView.frame.size.width/4 - 2, 20))
                heightVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                heightVal.font = heightVal.font.fontWithSize(13)
                
                let tempHt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text")
                if(tempHt != nil)
                {
                    height = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text") as? String)!
                    if(height == "0")
                    {
                        height = "-"
                    }
                    else
                    {
                        if(height.characters.contains("0"))
                        {
                            height = height.stringByReplacingCharactersInRange(height.startIndex.successor()..<height.startIndex.successor().successor(), withString: "'")
                            height = height+"\""
                        }
                    }
                    
                }
                else
                {
                    height = "-"
                }
                heightVal.text = height
                heightVal.sizeToFit()
                
                dobValRowView.addSubview(dobVal)
                dobValRowView.addSubview(ageVal)
                dobValRowView.addSubview(genderVal)
                dobValRowView.addSubview(heightVal)
                
                dobValRowView.frame = CGRectMake(20, dobValY, self.view.frame.size.width - 40, genderVal.frame.size.height)
                
                var  dateSeenRowY = dobValY + dobValRowView.frame.size.height + 4
                var dateSeenRowView = UIView(frame: CGRectMake(20, dateSeenRowY, self.view.frame.size.width - 40, 50))
                
                //date see label
                var dateSeenLabel = UILabel(frame: CGRectMake(0,0,(dateSeenRowView.frame.size.width)/4 - 2 ,20))
                dateSeenLabel.numberOfLines = 0
                dateSeenLabel.text = "Date 1st Seen"
                dateSeenLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                dateSeenLabel.font = UIFont.boldSystemFontOfSize(14)
                dateSeenLabel.sizeToFit()
                
                //eye color label
                var eyeColorLabel = UILabel(frame: CGRectMake((dateSeenRowView.frame.size.width)/4, 0, (dateSeenRowView.frame.size.width)/4 - 2, 20))
                eyeColorLabel.text = "Eye Color"
                eyeColorLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                eyeColorLabel.font = UIFont.boldSystemFontOfSize(14)
                eyeColorLabel.sizeToFit()
                
                //hair color label
                var hairColorLabel = UILabel(frame: CGRectMake(dateSeenRowView.frame.size.width/2, 0, (dateSeenRowView.frame.size.width)/4 - 2, 20))
                hairColorLabel.text = "Hair Color"
                hairColorLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                hairColorLabel.font = UIFont.boldSystemFontOfSize(14)
                hairColorLabel.sizeToFit()
                
                //weight label
                var weightLabel = UILabel(frame: CGRectMake((dateSeenRowView.frame.size.width/4)*3, 0, (dateSeenRowView.frame.size.width)/4 - 2, 20))
                weightLabel.text = "Weight"
                weightLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                weightLabel.font = UIFont.boldSystemFontOfSize(14)
                weightLabel.sizeToFit()
                
                dateSeenRowView.addSubview(dateSeenLabel)
                dateSeenRowView.addSubview(eyeColorLabel)
                dateSeenRowView.addSubview(hairColorLabel)
                dateSeenRowView.addSubview(weightLabel)
                
                dateSeenRowView.frame = CGRectMake(20, dateSeenRowY, self.view.frame.size.width - 40, dateSeenLabel.frame.size.height)
                
                var dateSeenValY = dateSeenRowY + dateSeenRowView.frame.size.height + 4
                var dateSeenValView = UIView(frame: CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, 50))
                
                //date 1st seen value
                var dateSeenVal = UILabel(frame: CGRectMake(0,0,(dateSeenValView.frame.size.width)/4 - 2,20))
                dateSeenVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                dateSeenVal.font = dateSeenVal.font.fontWithSize(10)
                enroll_dt = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("EnrollDate")?.objectForKey("text") as? String)!
                dateSeenVal.text = enroll_dt
                dateSeenVal.sizeToFit()
                
                //eye color value
                var eyeColorValue = UILabel(frame: CGRectMake(dateSeenValView.frame.size.width/4,0,(dateSeenRowView.frame.size.width)/4 - 2,20))
                eyeColorValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                eyeColorValue.font = eyeColorValue.font.fontWithSize(13)
                eyeColorValue.numberOfLines = 0
                EyeColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("EyeColor")?.objectForKey("text") as? String)!
                if(EyeColor == "?")
                {
                    EyeColor = "-"
                }
                eyeColorValue.text = EyeColor
                eyeColorValue.sizeToFit()
                
                //hair color value
                var hairColorValue = UILabel(frame: CGRectMake(dateSeenValView.frame.size.width/2,0,(dateSeenValView.frame.size.width)/4 - 2,20))
                hairColorValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                hairColorValue.font = hairColorValue.font.fontWithSize(13)
                hairColorValue.numberOfLines = 0
                HairColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("HairColor")?.objectForKey("text") as? String)!
                if(HairColor == "?")
                {
                    HairColor = "-"
                }
                hairColorValue.text = HairColor
                hairColorValue.sizeToFit()
                
                //weight value
                var weightVal = UILabel(frame: CGRectMake((dateSeenValView.frame.size.width/4)*3, 0, (dateSeenValView.frame.size.width)/4 - 2, 20))
                weightVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                weightVal.font = weightVal.font.fontWithSize(13)
                let tempWt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text")
                if(tempWt != nil)
                {
                    weight = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text") as? String)!
                    if(weight == "0")
                    {
                        weight = "-"
                    }
                }
                else
                {
                    weight = "-"
                }
                weightVal.text = weight
                
                
                dateSeenValView.addSubview(dateSeenVal)
                dateSeenValView.addSubview(eyeColorValue)
                dateSeenValView.addSubview(hairColorValue)
                dateSeenValView.addSubview(weightVal)
                
                if(eyeColorValue.frame.size.height > hairColorValue.frame.size.height)
                {
                    dateSeenValView.frame = CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, eyeColorValue.frame.size.height)
                }
                else if(eyeColorValue.frame.size.height < hairColorValue.frame.size.height)
                {
                    dateSeenValView.frame = CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, hairColorValue.frame.size.height)
                }
                else
                {
                    dateSeenValView.frame = CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, hairColorValue.frame.size.height)
                }
                
                //countryLabelView
                
                var countryLabelViewY = dateSeenValY + dateSeenValView.frame.size.height + 4
                var countryLabelRowView = UIView(frame: CGRectMake(20, countryLabelViewY, self.view.frame.size.width - 40,50))
                
                //country label
                var countryLabel = UILabel(frame: CGRectMake(0, 0, (countryLabelRowView.frame.size.width)/4 - 2,20))
                countryLabel.text = "Country of Birth"
                countryLabel.numberOfLines = 0
                countryLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                countryLabel.font = UIFont.boldSystemFontOfSize(14)
                countryLabel.sizeToFit()
                
                //citizenship label
                var citizenshipLabel = UILabel(frame: CGRectMake(countryLabelRowView.frame.size.width/4, 0, (countryLabelRowView.frame.size.width/4)-2,20))
                citizenshipLabel.text = "Citizenship"
                citizenshipLabel.numberOfLines = 0
                citizenshipLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                citizenshipLabel.font = UIFont.boldSystemFontOfSize(14)
                citizenshipLabel.sizeToFit()
                
                //score Label
                var scoreLabel = UILabel(frame: CGRectMake(countryLabelRowView.frame.size.width/2, 0, (countryLabelRowView.frame.size.width/4)-2,20))
                scoreLabel.text = "Score"
                scoreLabel.numberOfLines = 0
                scoreLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                scoreLabel.font = UIFont.boldSystemFontOfSize(14)
                scoreLabel.sizeToFit()
                
                
                //EnrollGps Label
                var enrollGpsLabel = UILabel(frame: CGRectMake((countryLabelRowView.frame.size.width/4)*3, 0, (countryLabelRowView.frame.size.width/4)-2,30))
                enrollGpsLabel.text = "Enroll GPS Location"
                enrollGpsLabel.numberOfLines = 0
                enrollGpsLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                enrollGpsLabel.font = UIFont.boldSystemFontOfSize(14)
                enrollGpsLabel.sizeToFit()
                
                countryLabelRowView.addSubview(countryLabel)
                countryLabelRowView.addSubview(citizenshipLabel)
                countryLabelRowView.addSubview(scoreLabel)
                countryLabelRowView.addSubview(enrollGpsLabel)
                
                countryLabelRowView.frame = CGRectMake(20, countryLabelViewY, self.view.frame.size.width - 40,enrollGpsLabel.frame.size.height)
                
                
                //country value label
                var countryValY = countryLabelViewY + enrollGpsLabel.frame.size.height + 4
                var countryVal = UILabel(frame: CGRectMake(20, countryValY, (countryLabelRowView.frame.size.width)/4 - 2,20))
                countryVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                countryVal.font = countryVal.font.fontWithSize(13)
                countryVal.numberOfLines = 0
                birthPlace = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("PlaceOfBirth")?.objectForKey("text") as? String)!
                if(birthPlace == "?")
                {
                    birthPlace = "-"
                }
                countryVal.text = birthPlace
                countryVal.sizeToFit()
                
                //citizenship value
                var citizenshipVal = UILabel(frame: CGRectMake(20 + (countryLabelRowView.frame.size.width)/4,countryValY,(countryLabelRowView.frame.size.width)/4,20))
                citizenshipVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                citizenshipVal.font = citizenshipVal.font.fontWithSize(13)
                citizenshipVal.numberOfLines = 0
                
                citizen = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Citizenship")?.objectForKey("text") as? String)!
                if(citizen == "?")
                {
                    citizen = "-"
                }
                citizenshipVal.text = citizen
                citizenshipVal.sizeToFit()
                
                //score val
                var scoreVal = UILabel(frame: CGRectMake(20 + countryLabelRowView.frame.size.width/2, countryValY, (countryLabelRowView.frame.size.width)/4,20))
                
                scoreVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                scoreVal.font = scoreVal.font.fontWithSize(13)
                scoreVal.numberOfLines = 0
                
                let nlyScr = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("Score")?.objectForKey("text") as? String)!
                let algo = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("AlgorithmID")?.objectForKey("text") as? String)!
                score = "\(algo):\(nlyScr)"
                scoreVal.text = score
                scoreVal.sizeToFit()
                
                //enroll GPS button
                var enrollValLbl = UILabel(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3,countryValY,(countryLabelRowView.frame.size.width)/4,35))
                enrollValLbl.numberOfLines = 0
                enrollValLbl.font = enrollValLbl.font.fontWithSize(11)

                var enrollGpsBtn = UIButton(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3,countryValY,(countryLabelRowView.frame.size.width)/4,35))
                enrollGpsBtn.titleLabel?.font = UIFont.systemFontOfSize(11)
                enrollGpsBtn.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
                enrollGpsBtn.titleLabel?.numberOfLines = 0
                let checkForGps = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("i:nil")
                if(checkForGps != nil || latDeg == "?")
                {
                    //googleURL = "No GPS available"
//                    enrollGpsBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
//                    enrollGpsBtn.setTitle("No GPS available", forState: .Normal)
                    enrollValLbl.textColor = UIColor.darkGrayColor()
                    enrollValLbl.text = "No GPS available"
                }
                else
                {
                    enrollValLbl.textColor = UIColor.blueColor()
                    enrollValLbl.text = "\(lattitude) \(longitude)"
//                    enrollGpsBtn.setTitleColor(UIColor.blueColor(), forState: .Normal)
//                    enrollGpsBtn.setTitle("\(lattitude) \(longitude)", forState: .Normal)
                    
//                    enrollGpsBtn.accessibilityIdentifier = String(indexPath.row)
//                    enrollGpsBtn.addTarget(self, action: Selector("gpsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                }
                //enrollGpsBtn.sizeToFit()
                enrollValLbl.sizeToFit()
                enrollGpsBtn.layoutIfNeeded()
                enrollGpsBtn.frame = CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3,countryValY,(countryLabelRowView.frame.size.width)/4, enrollValLbl.frame.size.height)
                
                //search GPS label row
                var searchGpsLocY = CGFloat()
                if((countryVal.frame.size.height >= citizenshipVal.frame.size.height) && (countryVal.frame.size.height >= scoreVal.frame.size.height) && (countryVal.frame.size.height >= enrollValLbl.frame.size.height))
                {
                    searchGpsLocY = countryValY + countryVal.frame.size.height + 4
                }
                else if((citizenshipVal.frame.size.height >= countryVal.frame.size.height) && (citizenshipVal.frame.size.height >= scoreVal.frame.size.height) && (citizenshipVal.frame.size.height >= enrollValLbl.frame.size.height))
                {
                    searchGpsLocY = countryValY + citizenshipVal.frame.size.height + 4
                }
                else if((scoreVal.frame.size.height >= countryVal.frame.size.height) && (scoreVal.frame.size.height >= citizenshipVal.frame.size.height) && (scoreVal.frame.size.height >= enrollValLbl.frame.size.height))
                {
                    searchGpsLocY = countryValY + scoreVal.frame.size.height + 4
                }
                else
                {
                    searchGpsLocY = countryValY + enrollValLbl.frame.size.height + 4
                }
                
                //date searched Label
                var dateSearchedLabel = UILabel(frame: CGRectMake(20 + countryLabelRowView.frame.size.width/2, searchGpsLocY, (countryLabelRowView.frame.size.width)/4,20))
                dateSearchedLabel.text = "Date seen"
                dateSearchedLabel.numberOfLines = 0
                dateSearchedLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                dateSearchedLabel.font = UIFont.boldSystemFontOfSize(14)
                dateSearchedLabel.sizeToFit()
                
                //search GPS location
                var searchGpsLocLabel = UILabel(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocY, (countryLabelRowView.frame.size.width)/4,20))
                searchGpsLocLabel.text = "Search GPS Location"
                searchGpsLocLabel.numberOfLines = 0
                searchGpsLocLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                searchGpsLocLabel.font = UIFont.boldSystemFontOfSize(14)
                searchGpsLocLabel.sizeToFit()
                
                //search GPS row value
                var searchGpsLocValY = searchGpsLocY + searchGpsLocLabel.frame.size.height + 4
                
                //date searched
                var dateSearchedVal = UILabel(frame: CGRectMake(20 + countryLabelRowView.frame.size.width/2, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4, 20))
                dateSearchedVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                dateSearchedVal.font = dateSearchedVal.font.fontWithSize(10)
                dateSearchedVal.text = convertedDate
                
                //search GPS Location and title label
                var searchValLbl = UILabel(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4,40))
                searchValLbl.numberOfLines = 0
                searchValLbl.font = searchValLbl.font.fontWithSize(11)
                
                var searchGpsLocation = UIButton(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4,40))
                searchGpsLocation.titleLabel?.font = UIFont.systemFontOfSize(11)
                searchGpsLocation.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
                searchGpsLocation.titleLabel?.numberOfLines = 0
                //searchGpsLocation.backgroundColor = UIColor.yellowColor()
                
                if(lattitudeSearch == 0 && longitudeSearch == 0)
                {
                    searchValLbl.textColor = UIColor.darkGrayColor()
                    searchValLbl.text = "No GPS available"
                }
                else
                {
                    searchValLbl.textColor = UIColor.blueColor()
                    searchValLbl.text = "\(lattitudeSearch) \((longitudeSearch))"
//                    searchGpsLocation.addTarget(self, action: Selector("gpsSearchButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                }
                searchValLbl.sizeToFit()
                searchGpsLocation.layoutIfNeeded()
                searchGpsLocation.frame = CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4,searchValLbl.frame.size.height)
                
                //sign y value
                var signImageY = searchGpsLocValY + searchValLbl.frame.size.height + 4
                
                // sign image
                var signImage = UIImageView(frame: CGRectMake(20, signImageY,(self.view.frame.width/2)-30, 80))
                
                let checkSign = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")
                if(checkSign != nil){
                    // sign is available
                    let sign_img = decodeBase64ToImage((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")?.objectForKey("text") as? String)!)
                    signImage.image = sign_img
                }
                else
                {
                    // no sign available
                    signImage.image = UIImage(named:"noImage")
                }
                //signImage.backgroundColor = UIColor.blueColor()
                signImage.contentMode = .ScaleAspectFit
                signImage.frame = CGRectMake(20, signImageY,(self.view.frame.width/2)-30, ((self.view.frame.width/2)-30)/1.6)
 
                return signImageY+signImage.frame.size.height + 10
            }
            else
            {
                var searchLabel = UILabel()
                var matchLabel = UILabel()
                var searchImage = UIImageView()
                var matchImage = UIImageView()
                
                searchLabel = UILabel(frame: CGRectMake(0, 8, self.view.frame.size.width/2, 20))
                
                matchLabel = UILabel(frame: CGRectMake(self.view.frame.size.width/2, 8, self.view.frame.size.width/2, 20))
                
                searchLabel.text = "Search"
                searchLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                searchLabel.font = UIFont.boldSystemFontOfSize(17)
                searchLabel.textAlignment = .Center
                matchLabel.text = "Match"
                matchLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                matchLabel.font = UIFont.boldSystemFontOfSize(17)
                matchLabel.textAlignment = .Center
                
                var searchImageY = searchLabel.frame.size.height + 16
                searchImage = UIImageView(frame: CGRectMake(10, searchImageY, (self.view.frame.width/2) - 20 , 90))
                matchImage = UIImageView(frame: CGRectMake((self.view.frame.width/2) + 10 , searchImageY, (self.view.frame.width/2) - 20, 90))
                searchImage.image = imageToBeSearchedAndMatch
                searchImage.contentMode = .ScaleAspectFit
                let image = decodeBase64ToImage(((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("FaceImage")?.objectForKey("text")) as? String)!)
                matchImage.image = image
                matchImage.contentMode = .ScaleAspectFit
                
                var firstNameY = searchImageY + searchImage.frame.size.height + 10
                var firstNameLabel = UILabel()
                firstNameLabel = UILabel(frame: CGRectMake(20, firstNameY, self.view.frame.size.width - 20, 20))
                firstNameLabel.text = "First Name"
                firstNameLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                firstNameLabel.font = UIFont.boldSystemFontOfSize(14)
                
                // first name value
                var firstNameValY = firstNameY + 22
                var firstNameVal = UILabel()
                firstNameVal = UILabel(frame: CGRectMake(20, firstNameValY, self.view.frame.size.width - 20, 20))
                firstNameVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                firstNameVal.font = firstNameVal.font.fontWithSize(14)
                
                Fname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("FirstName")?.objectForKey("text") as? String)!
                
                if(Fname == "?")
                {
                    Fname = "-"
                }
                firstNameVal.text = Fname
                
                // last name label
                var lastNameLabelY = firstNameValY + firstNameVal.frame.size.height + 5
                var lastNameLabel = UILabel(frame: CGRectMake(20, lastNameLabelY, self.view.frame.size.width - 20, 20))
                lastNameLabel.text = "Last Name"
                lastNameLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                lastNameLabel.font = UIFont.boldSystemFontOfSize(14)
                
                // last name value
                var lastNameValY = lastNameLabelY + 22
                var lastNameVal = UILabel(frame: CGRectMake(20, lastNameValY, self.view.frame.size.width - 20, 20))
                lastNameVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                lastNameVal.font = lastNameVal.font.fontWithSize(14)
                
                lname = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LastName")?.objectForKey("text") as? String)!
                if(lname == "?")
                {
                    lname = "-"
                }
                lastNameVal.text = lname
                
                //dob row view
                var dobY = lastNameValY + 22
                var dobRowView = UIView(frame: CGRectMake(20, dobY, self.view.frame.size.width - 40, 50))
                
                //dob label
                var dobLabel = UILabel(frame: CGRectMake(0,0,(dobRowView.frame.size.width)/4 - 2 ,20))
                dobLabel.numberOfLines = 0
                dobLabel.text = "Date of Birth"
                dobLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                dobLabel.font = UIFont.boldSystemFontOfSize(14)
                dobLabel.sizeToFit()
                
                //age label
                var ageLabel = UILabel(frame: CGRectMake((dobRowView.frame.size.width)/4, 0, (dobRowView.frame.size.width)/4 - 2, 20))
                ageLabel.text = "Age"
                ageLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                ageLabel.font = UIFont.boldSystemFontOfSize(14)
                ageLabel.sizeToFit()
                
                //gender label
                var genderLabel = UILabel(frame: CGRectMake(dobRowView.frame.size.width/2, 0, (dobRowView.frame.size.width)/4 - 2, 20))
                genderLabel.text = "Gender"
                genderLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                genderLabel.font = UIFont.boldSystemFontOfSize(14)
                genderLabel.sizeToFit()
                
                //height label
                var heightLabel = UILabel(frame: CGRectMake((dobRowView.frame.size.width/4)*3, 0, (dobRowView.frame.size.width)/4 - 2, 20))
                heightLabel.text = "Height"
                heightLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                heightLabel.font = UIFont.boldSystemFontOfSize(14)
                heightLabel.sizeToFit()
                
                dobRowView.addSubview(dobLabel)
                dobRowView.addSubview(ageLabel)
                dobRowView.addSubview(genderLabel)
                dobRowView.addSubview(heightLabel)
                
                dobRowView.frame = CGRectMake(20, dobY, self.view.frame.size.width - 40, dobLabel.frame.size.height)
                
                //dob row value
                var dobValY = dobY + dobRowView.frame.size.height + 4
                
                var dobValRowView = UIView(frame: CGRectMake(20, dobValY, self.view.frame.size.width - 40, 50))
                
                //dob val
                var dobVal = UILabel(frame: CGRectMake(0,0,dobRowView.frame.size.width/4 - 2,20))
                dobVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                dobVal.font = dobVal.font.fontWithSize(10)
                
                let check1 = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("i:nil")
                if (check1 != nil)
                {
                    age = "-"
                    tempAge = "-"
                }
                else
                {
                    let date = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("text") as? String
                    age = date!
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat =  "MM/dd/yyyy"
                    let dateToSave =  dateFormatter.dateFromString(date!)
                    tempAge  = String(calculateAge(dateToSave!))
                    tempAge = "\(tempAge) Years"
                    //print(tempAge)
                    if(tempAge == "0 Years"){
                        tempAge = String(calculateAgeInMonths(dateToSave!))
                        tempAge = "\(tempAge) Months"
                        //print(tempAge)
                        if(tempAge == "0 Months") {
                            tempAge = String(calculateAgeInDays(dateToSave!))
                            tempAge = "\(tempAge) Days"
                            //print(tempAge)
                        }
                    }
                }
                dobVal.text = age
                dobVal.sizeToFit()
                
                //age val
                var ageVal = UILabel(frame: CGRectMake(dobRowView.frame.size.width/4, 0, dobRowView.frame.size.width/4 - 2, 20))
                ageVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                ageVal.font = ageVal.font.fontWithSize(13)
                ageVal.text = tempAge
                ageVal.sizeToFit()
                
                //gender value
                var genderVal = UILabel(frame: CGRectMake(dobRowView.frame.size.width/2, 0, dobRowView.frame.size.width/4 - 2, 20))
                genderVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                genderVal.font = genderVal.font.fontWithSize(13)
                genderVal.numberOfLines = 0
                gender = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Gender")?.objectForKey("text") as? String)!
                if(gender == "?")
                {
                    gender = "-"
                }
                genderVal.text = gender
                genderVal.sizeToFit()
                
                //height value
                var heightVal = UILabel(frame: CGRectMake((dobRowView.frame.size.width/4)*3, 0, dobRowView.frame.size.width/4 - 2, 20))
                heightVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                heightVal.font = heightVal.font.fontWithSize(13)
                
                let tempHt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text")
                if(tempHt != nil)
                {
                    height = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text") as? String)!
                    if(height == "0")
                    {
                        height = "-"
                    }
                    else
                    {
                        if(height.characters.contains("0"))
                        {
                            height = height.stringByReplacingCharactersInRange(height.startIndex.successor()..<height.startIndex.successor().successor(), withString: "'")
                            height = height+"\""
                        }
                    }
                    
                }
                else
                {
                    height = "-"
                }
                heightVal.text = height
                heightVal.sizeToFit()
                
                dobValRowView.addSubview(dobVal)
                dobValRowView.addSubview(ageVal)
                dobValRowView.addSubview(genderVal)
                dobValRowView.addSubview(heightVal)
                
                dobValRowView.frame = CGRectMake(20, dobValY, self.view.frame.size.width - 40, genderVal.frame.size.height)
                
                var  dateSeenRowY = dobValY + dobValRowView.frame.size.height + 4
                var dateSeenRowView = UIView(frame: CGRectMake(20, dateSeenRowY, self.view.frame.size.width - 40, 50))
                
                //date see label
                var dateSeenLabel = UILabel(frame: CGRectMake(0,0,(dateSeenRowView.frame.size.width)/4 - 2 ,20))
                dateSeenLabel.numberOfLines = 0
                dateSeenLabel.text = "Date 1st Seen"
                dateSeenLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                dateSeenLabel.font = UIFont.boldSystemFontOfSize(14)
                dateSeenLabel.sizeToFit()
                
                //eye color label
                var eyeColorLabel = UILabel(frame: CGRectMake((dateSeenRowView.frame.size.width)/4, 0, (dateSeenRowView.frame.size.width)/4 - 2, 20))
                eyeColorLabel.text = "Eye Color"
                eyeColorLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                eyeColorLabel.font = UIFont.boldSystemFontOfSize(14)
                eyeColorLabel.sizeToFit()
                
                //hair color label
                var hairColorLabel = UILabel(frame: CGRectMake(dateSeenRowView.frame.size.width/2, 0, (dateSeenRowView.frame.size.width)/4 - 2, 20))
                hairColorLabel.text = "Hair Color"
                hairColorLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                hairColorLabel.font = UIFont.boldSystemFontOfSize(14)
                hairColorLabel.sizeToFit()
                
                //weight label
                var weightLabel = UILabel(frame: CGRectMake((dateSeenRowView.frame.size.width/4)*3, 0, (dateSeenRowView.frame.size.width)/4 - 2, 20))
                weightLabel.text = "Weight"
                weightLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                weightLabel.font = UIFont.boldSystemFontOfSize(14)
                weightLabel.sizeToFit()
                
                dateSeenRowView.addSubview(dateSeenLabel)
                dateSeenRowView.addSubview(eyeColorLabel)
                dateSeenRowView.addSubview(hairColorLabel)
                dateSeenRowView.addSubview(weightLabel)
                
                dateSeenRowView.frame = CGRectMake(20, dateSeenRowY, self.view.frame.size.width - 40, dateSeenLabel.frame.size.height)
                
                var dateSeenValY = dateSeenRowY + dateSeenRowView.frame.size.height + 4
                var dateSeenValView = UIView(frame: CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, 50))
                
                //date 1st seen value
                var dateSeenVal = UILabel(frame: CGRectMake(0,0,(dateSeenValView.frame.size.width)/4 - 2,20))
                dateSeenVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                dateSeenVal.font = dateSeenVal.font.fontWithSize(10)
                enroll_dt = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("EnrollDate")?.objectForKey("text") as? String)!
                dateSeenVal.text = enroll_dt
                dateSeenVal.sizeToFit()
                
                //eye color value
                var eyeColorValue = UILabel(frame: CGRectMake(dateSeenValView.frame.size.width/4,0,(dateSeenRowView.frame.size.width)/4 - 2,20))
                eyeColorValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                eyeColorValue.font = eyeColorValue.font.fontWithSize(13)
                eyeColorValue.numberOfLines = 0
                EyeColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("EyeColor")?.objectForKey("text") as? String)!
                if(EyeColor == "?")
                {
                    EyeColor = "-"
                }
                eyeColorValue.text = EyeColor
                eyeColorValue.sizeToFit()
                
                //hair color value
                var hairColorValue = UILabel(frame: CGRectMake(dateSeenValView.frame.size.width/2,0,(dateSeenValView.frame.size.width)/4 - 2,20))
                hairColorValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                hairColorValue.font = hairColorValue.font.fontWithSize(13)
                hairColorValue.numberOfLines = 0
                HairColor = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("HairColor")?.objectForKey("text") as? String)!
                if(HairColor == "?")
                {
                    HairColor = "-"
                }
                hairColorValue.text = HairColor
                hairColorValue.sizeToFit()
                
                //weight value
                var weightVal = UILabel(frame: CGRectMake((dateSeenValView.frame.size.width/4)*3, 0, (dateSeenValView.frame.size.width)/4 - 2, 20))
                weightVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                weightVal.font = weightVal.font.fontWithSize(13)
                let tempWt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text")
                if(tempWt != nil)
                {
                    weight = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text") as? String)!
                    if(weight == "0")
                    {
                        weight = "-"
                    }
                }
                else
                {
                    weight = "-"
                }
                weightVal.text = weight
                
                
                dateSeenValView.addSubview(dateSeenVal)
                dateSeenValView.addSubview(eyeColorValue)
                dateSeenValView.addSubview(hairColorValue)
                dateSeenValView.addSubview(weightVal)
                
                if(eyeColorValue.frame.size.height > hairColorValue.frame.size.height)
                {
                    dateSeenValView.frame = CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, eyeColorValue.frame.size.height)
                }
                else if(eyeColorValue.frame.size.height < hairColorValue.frame.size.height)
                {
                    dateSeenValView.frame = CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, hairColorValue.frame.size.height)
                }
                else
                {
                    dateSeenValView.frame = CGRectMake(20, dateSeenValY, self.view.frame.size.width - 40, hairColorValue.frame.size.height)
                }
                
                //countryLabelView
                
                var countryLabelViewY = dateSeenValY + dateSeenValView.frame.size.height + 4
                var countryLabelRowView = UIView(frame: CGRectMake(20, countryLabelViewY, self.view.frame.size.width - 40,50))
                
                //country label
                var countryLabel = UILabel(frame: CGRectMake(0, 0, (countryLabelRowView.frame.size.width)/4 - 2,20))
                countryLabel.text = "Country of Birth"
                countryLabel.numberOfLines = 0
                countryLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                countryLabel.font = UIFont.boldSystemFontOfSize(14)
                countryLabel.sizeToFit()
                
                //citizenship label
                var citizenshipLabel = UILabel(frame: CGRectMake(countryLabelRowView.frame.size.width/4, 0, (countryLabelRowView.frame.size.width/4)-2,20))
                citizenshipLabel.text = "Citizenship"
                citizenshipLabel.numberOfLines = 0
                citizenshipLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                citizenshipLabel.font = UIFont.boldSystemFontOfSize(14)
                citizenshipLabel.sizeToFit()
                
                //score Label
                var scoreLabel = UILabel(frame: CGRectMake(countryLabelRowView.frame.size.width/2, 0, (countryLabelRowView.frame.size.width/4)-2,20))
                scoreLabel.text = "Score"
                scoreLabel.numberOfLines = 0
                scoreLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                scoreLabel.font = UIFont.boldSystemFontOfSize(14)
                scoreLabel.sizeToFit()
                
                
                //EnrollGps Label
                var enrollGpsLabel = UILabel(frame: CGRectMake((countryLabelRowView.frame.size.width/4)*3, 0, (countryLabelRowView.frame.size.width/4)-2,30))
                enrollGpsLabel.text = "Enroll GPS Location"
                enrollGpsLabel.numberOfLines = 0
                enrollGpsLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                enrollGpsLabel.font = UIFont.boldSystemFontOfSize(14)
                enrollGpsLabel.sizeToFit()
                
                countryLabelRowView.addSubview(countryLabel)
                countryLabelRowView.addSubview(citizenshipLabel)
                countryLabelRowView.addSubview(scoreLabel)
                countryLabelRowView.addSubview(enrollGpsLabel)
                
                countryLabelRowView.frame = CGRectMake(20, countryLabelViewY, self.view.frame.size.width - 40,enrollGpsLabel.frame.size.height)
                
                
                //country value label
                var countryValY = countryLabelViewY + enrollGpsLabel.frame.size.height + 4
                var countryVal = UILabel(frame: CGRectMake(20, countryValY, (countryLabelRowView.frame.size.width)/4 - 2,20))
                countryVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                countryVal.font = countryVal.font.fontWithSize(13)
                countryVal.numberOfLines = 0
                birthPlace = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("PlaceOfBirth")?.objectForKey("text") as? String)!
                if(birthPlace == "?")
                {
                    birthPlace = "-"
                }
                countryVal.text = birthPlace
                countryVal.sizeToFit()
                
                //citizenship value
                var citizenshipVal = UILabel(frame: CGRectMake(20 + (countryLabelRowView.frame.size.width)/4,countryValY,(countryLabelRowView.frame.size.width)/4,20))
                citizenshipVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                citizenshipVal.font = citizenshipVal.font.fontWithSize(13)
                citizenshipVal.numberOfLines = 0
                
                citizen = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("Citizenship")?.objectForKey("text") as? String)!
                if(citizen == "?")
                {
                    citizen = "-"
                }
                citizenshipVal.text = citizen
                citizenshipVal.sizeToFit()
                
                //score val
                var scoreVal = UILabel(frame: CGRectMake(20 + countryLabelRowView.frame.size.width/2, countryValY, (countryLabelRowView.frame.size.width)/4,20))
                
                scoreVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                scoreVal.font = scoreVal.font.fontWithSize(13)
                scoreVal.numberOfLines = 0
                
                let nlyScr = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("Score")?.objectForKey("text") as? String)!
                let algo = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("MatchScores")?.objectForKey("MatchScore")?.objectForKey("AlgorithmID")?.objectForKey("text") as? String)!
                score = "\(algo):\(nlyScr)"
                scoreVal.text = score
                scoreVal.sizeToFit()
                
                //enroll GPS button
                var enrollValLbl = UILabel(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3,countryValY,(countryLabelRowView.frame.size.width)/4,35))
                enrollValLbl.numberOfLines = 0
                enrollValLbl.font = enrollValLbl.font.fontWithSize(11)

                var enrollGpsBtn = UIButton(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3,countryValY,(countryLabelRowView.frame.size.width)/4,35))
                enrollGpsBtn.titleLabel?.font = UIFont.systemFontOfSize(11)
                enrollGpsBtn.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
                enrollGpsBtn.titleLabel?.numberOfLines = 0
                let checkForGps = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("i:nil")
                if(checkForGps != nil)
                {
                    //googleURL = "No GPS available"
//                    enrollGpsBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
//                    enrollGpsBtn.setTitle("No GPS available", forState: .Normal)
                    enrollValLbl.textColor = UIColor.darkGrayColor()
                    enrollValLbl.text = "No GPS available"

                }
                else
                {
                    latDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("text") as? String)!
                    
                    latMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeMinute")?.objectForKey("text") as? String)!
                    
                    latSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LatitudeSecond")?.objectForKey("text") as? String)!
                    
                    longDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LongitudeDegree")?.objectForKey("text") as? String)!
                    
                    longMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LongitudeMinute")?.objectForKey("text") as? String)!
                    
                    longSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiographicInformation")?.objectForKey("LongitudeSecond")?.objectForKey("text") as? String)!
                    
                    if(latDeg == "?")
                    {
//                        enrollGpsBtn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
//                        enrollGpsBtn.setTitle("No GPS available", forState: .Normal)
                        enrollValLbl.textColor = UIColor.darkGrayColor()
                        enrollValLbl.text = "No GPS available"

                        enrollGpsBtn.tag = 420
                    }
                    else
                    {
                        var lattitude1 = Double(latDeg)!+(Double(latMin)!/60.0)+(Double(latSec)!/3600.0)
                        var longitude1 = Double(longDeg)!+(Double(longMin)!/60.0)+(Double(longSec)!/3600.0)
                        if(lattitude1 > 180)
                        {
                            lattitude1 = lattitude1 - 360
                        }
                        if(longitude1 > 180)
                        {
                            longitude1 = longitude1 - 360
                        }
                        
                        enrollGpsBtn.tag = 520
//                        enrollGpsBtn.setTitleColor(UIColor.blueColor(), forState: .Normal)
//                        enrollGpsBtn.setTitle("\(lattitude1) \(longitude1)", forState: .Normal)
                        enrollValLbl.textColor = UIColor.blueColor()
                        enrollValLbl.text = "\(lattitude1) \(longitude1)"
                        enrollGpsBtn.accessibilityIdentifier = String(indexPath.row)
                        enrollGpsBtn.addTarget(self, action: Selector("gpsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                    }
                }
                //enrollGpsBtn.sizeToFit()
                enrollValLbl.sizeToFit()
                enrollGpsBtn.layoutIfNeeded()
                enrollGpsBtn.frame = CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3,countryValY,(countryLabelRowView.frame.size.width)/4, enrollValLbl.frame.size.height)
                
                //search GPS label row
                var searchGpsLocY = CGFloat()
                if((countryVal.frame.size.height >= citizenshipVal.frame.size.height) && (countryVal.frame.size.height >= scoreVal.frame.size.height) && (countryVal.frame.size.height >= enrollValLbl.frame.size.height))
                {
                    searchGpsLocY = countryValY + countryVal.frame.size.height + 4
                }
                else if((citizenshipVal.frame.size.height >= countryVal.frame.size.height) && (citizenshipVal.frame.size.height >= scoreVal.frame.size.height) && (citizenshipVal.frame.size.height >= enrollValLbl.frame.size.height))
                {
                    searchGpsLocY = countryValY + citizenshipVal.frame.size.height + 4
                }
                else if((scoreVal.frame.size.height >= countryVal.frame.size.height) && (scoreVal.frame.size.height >= citizenshipVal.frame.size.height) && (scoreVal.frame.size.height >= enrollValLbl.frame.size.height))
                {
                    searchGpsLocY = countryValY + scoreVal.frame.size.height + 4
                }
                else
                {
                    searchGpsLocY = countryValY + enrollValLbl.frame.size.height + 4
                }
                
                //date searched Label
                var dateSearchedLabel = UILabel(frame: CGRectMake(20 + countryLabelRowView.frame.size.width/2, searchGpsLocY, (countryLabelRowView.frame.size.width)/4,20))
                dateSearchedLabel.text = "Date seen"
                dateSearchedLabel.numberOfLines = 0
                dateSearchedLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                dateSearchedLabel.font = UIFont.boldSystemFontOfSize(14)
                dateSearchedLabel.sizeToFit()
                
                //search GPS location
                var searchGpsLocLabel = UILabel(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocY, (countryLabelRowView.frame.size.width)/4,20))
                searchGpsLocLabel.text = "Search GPS Location"
                searchGpsLocLabel.numberOfLines = 0
                searchGpsLocLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                searchGpsLocLabel.font = UIFont.boldSystemFontOfSize(14)
                searchGpsLocLabel.sizeToFit()
                
                //search GPS row value
                var searchGpsLocValY = searchGpsLocY + searchGpsLocLabel.frame.size.height + 4
                
                //date searched
                var dateSearchedVal = UILabel(frame: CGRectMake(20 + countryLabelRowView.frame.size.width/2, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4, 20))
                dateSearchedVal.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                dateSearchedVal.font = dateSearchedVal.font.fontWithSize(10)
                dateSearchedVal.text = convertedDate
                
                //search GPS Location and title label
                var searchValLbl = UILabel(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4,40))
                searchValLbl.numberOfLines = 0
                searchValLbl.font = searchValLbl.font.fontWithSize(11)

                var searchGpsLocation = UIButton(frame: CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4,40))
                searchGpsLocation.titleLabel?.font = UIFont.systemFontOfSize(11)
                searchGpsLocation.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
                searchGpsLocation.titleLabel?.numberOfLines = 0
                //searchGpsLocation.backgroundColor = UIColor.yellowColor()
                
                if(lattitudeSearch == 0 && longitudeSearch == 0)
                {
                    searchValLbl.textColor = UIColor.darkGrayColor()
                    searchValLbl.text = "No GPS available"
                }
                else
                {
                    searchValLbl.textColor = UIColor.blueColor()
                    searchValLbl.text = "\(lattitudeSearch) \((longitudeSearch))"

                    searchGpsLocation.addTarget(self, action: Selector("gpsSearchButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                }
                searchValLbl.sizeToFit()
                searchGpsLocation.layoutIfNeeded()
                searchGpsLocation.frame = CGRectMake(20+(countryLabelRowView.frame.size.width/4)*3, searchGpsLocValY, (countryLabelRowView.frame.size.width)/4,searchValLbl.frame.size.height)
                
                //sign y value
                var signImageY = searchGpsLocValY + searchValLbl.frame.size.height + 4
                
                // sign image
                var signImage = UIImageView(frame: CGRectMake(20, signImageY,(self.view.frame.width/2)-30, 80))
                
                let checkSign = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")
                if(checkSign != nil){
                    // sign is available
                    let sign_img = decodeBase64ToImage((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(indexPath.row).objectForKey("BiomImages")?.objectForKey("BiomImage")?.objectForKey("Data")?.objectForKey("text") as? String)!)
                    signImage.image = sign_img
                }
                else
                {
                    // no sign available
                    signImage.image = UIImage(named:"noImage")
                }
                //signImage.backgroundColor = UIColor.blueColor()
                signImage.contentMode = .ScaleAspectFit
                signImage.frame = CGRectMake(20, signImageY,(self.view.frame.width/2)-30, ((self.view.frame.width/2)-30)/1.6)
            return signImageY+signImage.frame.size.height+10
            }
        }
        else
        {
            //print("ipad")
            return 844.0
        }

    }
    func gpsSearchButtonClicked(sender : UIButton)
    {
        let googleURL = "http://maps.google.com/maps?&z=20&q=\(lattitudeSearch),\(longitudeSearch)&ll=\(lattitudeSearch),\(longitudeSearch)"
        //print(googleURL)
        let openLink = NSURL(string : googleURL)
        UIApplication.sharedApplication().openURL(openLink!)
    }
    // MARK: mail Button
    func mailButtonClicked(sender : UIButton)
    {
        dateFormatter.dateFormat = "MMM dd, yyyy"
        convertedDate = dateFormatter.stringFromDate(date)
        
        dateFormatter.dateFormat = "HH:mm:ss"
        convertedTime = dateFormatter.stringFromDate(date)
        let mailRow = Int(sender.accessibilityIdentifier!)
        print(mailRow)
        appdelegate!.showHUD(self.view)
        imgStr = encodeToBase64String(imageToBeSearchedAndMatch)
        if((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))!.isKindOfClass(NSDictionary))
        {
        Fname_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("FirstName")?.objectForKey("text") as? String)!
        lname_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LastName")?.objectForKey("text") as? String)!
            let check1 = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("i:nil")
            
            if (check1 != nil)
            {
                age_sv = "-"
                //dobAge = "-"
            }
            else
            {
                dobAge = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("text") as? String)!
                
                // age_sv = date!
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat =  "MM/dd/yyyy"
                let dateToSave =  dateFormatter.dateFromString(dobAge)
                age_sv  = String(calculateAge(dateToSave!))
            }
            let tempHt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text")
            if(tempHt != nil)
            {

            height_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text") as? String)!
                if(height_sv == "0")
                {
                    height_sv = "-"
                }
                else
                {
                    if(height_sv.characters.contains("0"))
                    {
                        height_sv = height_sv.stringByReplacingCharactersInRange(height_sv.startIndex.successor()..<height_sv.startIndex.successor().successor(), withString: "'")
                        height_sv = "\(height_sv)\""
                    }
                }
            }
            else
            {
                height_sv = "-"
            }
            let tempWt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text")
            if(tempWt != nil)
            {
            weight_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text") as? String)!
                if(weight_sv == "0")
                {
                    weight_sv = "-"
                }
            }
            else
            {
                 weight_sv = "-"
            }
            HairColor_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("HairColor")?.objectForKey("text") as? String)!
            EyeColor_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("EyeColor")?.objectForKey("text") as? String)!
            race_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("Race")?.objectForKey("text") as? String)!
//            latDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("text") as? String)!
//            latMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeMinute")?.objectForKey("text") as? String)!
//            latSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeSecond")?.objectForKey("text") as? String)!
//            longDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LongitudeDegree")?.objectForKey("text") as? String)!
//            longMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LongitudeMinute")?.objectForKey("text") as? String)!
//            longSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LongitudeSecond")?.objectForKey("text") as? String)!
//            
//            let lattitude = Double(latDeg)!+(Double(latMin)!/60.0)+(Double(latSec)!/3600.0)
//            let longitude = Double(longDeg)!+(Double(longMin)!/60.0)+(Double(longSec)!/3600.0)
           googleURL = "http://maps.google.com/maps?&z=20&q=\(lattitudeSearch),\(longitudeSearch)&ll=\(lattitudeSearch),\(longitudeSearch)"

        }
        else
        {
            Fname_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("FirstName")?.objectForKey("text") as? String)!
            lname_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("LastName")?.objectForKey("text") as? String)!
            let check1 = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("i:nil")
            if (check1 != nil)
            {
                age_sv = "-"
            }
            else
            {
                dobAge = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("DOB")?.objectForKey("text") as? String)!
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat =  "MM/dd/yyyy"
                let dateToSave =  dateFormatter.dateFromString(dobAge)
                age_sv  = String(calculateAge(dateToSave!))
            }
            let tempHt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text")
            if(tempHt != nil)
            {
            height_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("Height")?.objectForKey("text") as? String)!
                print(height_sv)
                if(height_sv == "0")
                {
                    height_sv = "-"
                }
                else
                {
                    if(height_sv.characters.contains("0"))
                    {
                        height_sv = height_sv.stringByReplacingCharactersInRange(height_sv.startIndex.successor()..<height_sv.startIndex.successor().successor(), withString: "'")
                        height_sv = "\(height_sv)\""
                    }
                }
            }
            else
            {
                 height_sv = "-"
            }
            let tempWt = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text")
            if(tempWt != nil)
            {
            weight_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("Weight")?.objectForKey("text") as? String)!
                print(weight_sv)
                if(weight_sv == "0")
                {
                    weight_sv = "-"
                }
            }
            else
            {
                weight_sv = "-"
            }
            HairColor_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("HairColor")?.objectForKey("text") as? String)!
            EyeColor_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("EyeColor")?.objectForKey("text") as? String)!
            race_sv = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("Race")?.objectForKey("text") as? String)!
//            latDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("text") as? String)!
//            latMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("LatitudeMinute")?.objectForKey("text") as? String)!
//            latSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("LatitudeSecond")?.objectForKey("text") as? String)!
//            longDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("LongitudeDegree")?.objectForKey("text") as? String)!
//            longMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("LongitudeMinute")?.objectForKey("text") as? String)!
//            longSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(mailRow!).objectForKey("BiographicInformation")?.objectForKey("LongitudeSecond")?.objectForKey("text") as? String)!
//            let lattitude = Double(latDeg)!+(Double(latMin)!/60.0)+(Double(latSec)!/3600.0)
//            let longitude = Double(longDeg)!+(Double(longMin)!/60.0)+(Double(longSec)!/3600.0)
            googleURL = "http://maps.google.com/maps?&z=20&q=\(lattitudeSearch),\(longitudeSearch)&ll=\(lattitudeSearch),\(longitudeSearch)"
        }
        let dispatchQueue : dispatch_queue_t = dispatch_queue_create("dispatchQueue", nil)
        dispatch_async(dispatchQueue) {
            self.sendEmailServiceFromSearch(mailRow!)
        }
    }
    func sendEmailServiceFromSearch(cancelRow:Int)
    {
        print(height_sv)
        print(weight_sv)
        let alertController = UIAlertController(title: "Send Alert", message: "Do you want to send alert?", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
            self.appdelegate?.dismissView(self.view)
        })
        let sendAction = UIAlertAction(title: "SEND", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in

        self.obj_WebServiceMail.sendMail(self.imgStr, date: self.convertedDate, time: self.convertedTime, location: self.googleURL, surname: self.lname_sv, firstName: self.Fname_sv, age: self.age_sv, height: self.height_sv, weight: self.weight_sv, ethnicity: self.race_sv, hairColor: self.HairColor_sv, eyeColor: self.EyeColor_sv){ (block) in
            print(block);
            
            dispatch_async(dispatch_get_main_queue(),
                           {
                             self.appdelegate?.dismissView(self.view)
                        })
            if(block == nil){
                let Cust = CustomClass()
                var alert = UIAlertController()
                alert = Cust.displayMyAlertMessage("Check Your Network Connection")
                self.presentViewController(alert, animated: true, completion: nil)
                self.appdelegate?.dismissView(self.view)
            }
            else
            {

        if(block!.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("SendAlertResponse")?.objectForKey("SendAlertResult")?.objectForKey("Code")?.objectForKey("text") as? String == "sendalert.success")
        {
            let alertController = UIAlertController(title: "Success", message: "SendAlert request successfully processed", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
                //self.appdelegate?.dismissView(self.view)
                self.user_data = self.obj_databaseHandler.fetchAllRecord()
                let usr_dt = self.user_data.objectAtIndex(cancelRow)
                let obj_id = (usr_dt.valueForKey("obj_Id")) as! String
                self.obj_databaseHandler.deleteUserData(obj_id)
                if((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))!.isKindOfClass(NSDictionary))
                {
                    //self.navigationController?.popViewControllerAnimated(true)
                    let destination = self.storyboard!.instantiateViewControllerWithIdentifier("searchViewController") as! searchViewController
                    self.navigationController!.pushViewController(destination, animated: true)
                }
                else
                {
                    (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))?.removeObjectAtIndex(cancelRow)
                    self.myTableView.reloadData()
                }
            })
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion:{})
        }
        else
        {
            let Cust = CustomClass()
            var alert = UIAlertController( )
            alert = Cust.displayMyAlertMessage("SendAlert request failed")
            self.presentViewController(alert, animated: true, completion: nil)
        }
       }
    }
            
        })
        alertController.addAction(cancelAction)
        alertController.addAction(sendAction)
        dispatch_async(dispatch_get_main_queue(),
                       {
                        self.presentViewController(alertController, animated: true, completion:{})
        })

        

}
    // MARK: encode Image
    func encodeToBase64String(image: UIImage) -> String {
        let imageData = UIImagePNGRepresentation(image)
        let imag_str:String = imageData!.base64EncodedStringWithOptions([])
        ////print(imag_str)
        return imag_str
    }

    // MARK: cancel button
    func cancelButtonClicked(sender : UIButton)
    {
        let alertController = UIAlertController(title: "Delete", message: "Are you sure you want to delete this matched profile?", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
        })
        let deleteAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in

        self.user_data = self.obj_databaseHandler.fetchAllRecord()
        let cancelRow = Int(sender.accessibilityIdentifier!)
        let usr_dt = self.user_data.objectAtIndex(cancelRow!)
        let obj_id = (usr_dt.valueForKey("obj_Id")) as! String
        self.obj_databaseHandler.deleteUserData(obj_id)
        if((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))!.isKindOfClass(NSDictionary))
        {
            //self.navigationController?.popViewControllerAnimated(true)
            let destination = self.storyboard!.instantiateViewControllerWithIdentifier("searchViewController") as! searchViewController
            self.navigationController!.pushViewController(destination, animated: true)
        }
        else
        {
            (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))?.removeObjectAtIndex(cancelRow!)
                self.myTableView.reloadData()
        }
        })
        alertController.addAction(cancelAction)
        alertController.addAction(deleteAction)
        self.presentViewController(alertController, animated: true, completion:{})

    }
    
    // MARK: gps button action
    func gpsButtonClicked(sender : UIButton)
    {
        let button: UIButton = sender
        //print(sender.tag)
        if(button.tag != 420)
       {
        var latDeg : String = String()
        var latMin : String = String()
        var latSec : String = String()
        var longDeg : String = String()
        var longMin : String = String()
        var longSec : String = String()

        let gpsBtnRow = Int(sender.accessibilityIdentifier!)
        if((self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord"))!.isKindOfClass(NSDictionary))
        {
            //let checkForGps = self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("i:nil")
            latDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("text") as? String)!
            latMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeMinute")?.objectForKey("text") as? String)!
            latSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LatitudeSecond")?.objectForKey("text") as? String)!
            longDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LongitudeDegree")?.objectForKey("text") as? String)!
            longMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LongitudeMinute")?.objectForKey("text") as? String)!
            longSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectForKey("BiographicInformation")?.objectForKey("LongitudeSecond")?.objectForKey("text") as? String)!
        }
        else
        {
            latDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(gpsBtnRow!).objectForKey("BiographicInformation")?.objectForKey("LatitudeDegree")?.objectForKey("text") as? String)!
            latMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(gpsBtnRow!).objectForKey("BiographicInformation")?.objectForKey("LatitudeMinute")?.objectForKey("text") as? String)!
            latSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(gpsBtnRow!).objectForKey("BiographicInformation")?.objectForKey("LatitudeSecond")?.objectForKey("text") as? String)!
            longDeg = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(gpsBtnRow!).objectForKey("BiographicInformation")?.objectForKey("LongitudeDegree")?.objectForKey("text") as? String)!
            longMin = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(gpsBtnRow!).objectForKey("BiographicInformation")?.objectForKey("LongitudeMinute")?.objectForKey("text") as? String)!
            longSec = (self.respDict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("MatchedRecords")?.objectForKey("MatchedRecord")?.objectAtIndex(gpsBtnRow!).objectForKey("BiographicInformation")?.objectForKey("LongitudeSecond")?.objectForKey("text") as? String)!
        }
        var lattitude = Double(latDeg)!+(Double(latMin)!/60.0)+(Double(latSec)!/3600.0)
        var longitude = Double(longDeg)!+(Double(longMin)!/60.0)+(Double(longSec)!/3600.0)
        if(lattitude > 180){
            lattitude = lattitude - 360
        }
        if(longitude > 180){
            longitude = longitude - 360
        }
        let googleURL = "http://maps.google.com/maps?&z=20&q=\(lattitude),\(longitude)&ll=\(lattitude),\(longitude)"
        //print(googleURL)
        let openLink = NSURL(string : googleURL)
        UIApplication.sharedApplication().openURL(openLink!)
      }
    }

    // MARK: calculate age from birthday
    func calculateAge (birthday: NSDate) -> NSInteger
    {
        return NSCalendar.currentCalendar().components(.Year, fromDate: birthday, toDate: NSDate(), options: []).year
    }
    func calculateAgeInMonths (birthday: NSDate) -> NSInteger
    {
        return NSCalendar.currentCalendar().components(.Month, fromDate: birthday, toDate: NSDate(), options: []).month
    }
    func calculateAgeInDays (birthday: NSDate) -> NSInteger
    {
        return NSCalendar.currentCalendar().components(.Day, fromDate: birthday, toDate: NSDate(), options: []).day
    }
    
     // MARK: decode image to base64
    func decodeBase64ToImage(encodedImageData:String) -> UIImage {
        let imageData = NSData(base64EncodedString: encodedImageData, options: NSDataBase64DecodingOptions(rawValue: 0))
        let image1 = UIImage(data: imageData!)
        return image1!
    }
    
     // MARK: save image
    func saveImg(imageToSave : UIImage) -> String {
        var filename : String = String()
        if let image = imageToSave as UIImage? {
            if let data = UIImageJPEGRepresentation(image, 0.8) {
                let gtDocDir = getDocumentsDirectory()
                let rnd = String(randomStringWithLength())
                let slash : String = "/"
                let png : String = ".png"
                filename = gtDocDir+slash+rnd+png
                //print(filename)
                data.writeToFile(filename, atomically: true)
                
            }
        }
        return filename
    }
    // MARK: get document directory path
    func getDocumentsDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    // MARK: random string generator
    func randomStringWithLength () -> NSString
    {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: 15)
        
        for (var i=0; i < 15; i++)
        {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
        
        return randomString
    }
      /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
