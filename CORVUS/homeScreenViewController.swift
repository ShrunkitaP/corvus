//
//  homeScreenViewController.swift
//  Corvus Biometrics
//
//  Created by Riken Shah on 29/08/16.
//  Copyright © 2016 com.heypayless. All rights reserved.
//

import UIKit

class homeScreenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var homeTableView: UITableView!
    var detailsBtnRow = Int()
    let date = NSDate()
    let dateFormatter = NSDateFormatter()
    var convertedDate : String = String()
    var convertedTime : String = String()
    var F_nameEm : String = String()
    var L_nameEm : String = String()
    var ageEm :String = String()
    var heightEm : String = String()
    var weightEm : String = String()
    var eyeColorEm : String = String()
    var hairColorEm : String = String()
    var raceEm : String = String()
    var google : String = String()
    var new_img : String = String()
    var imageEm = UIImage()
    var imageDataEm : String = String()
    var emailRow : Int = Int()
    let obj_WebService = WebService()
    var dataObj = DatabaseHandler()
    var user_data : NSArray = NSArray()
    var user_data1 :NSArray = NSArray()
    var appdelegate: AppDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        homeTableView.hidden = true
       
        self.navigationController?.navigationBarHidden = true
         appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
       
        // Do any additional setup after loading the view.
        
        homeTableView.rowHeight = UITableViewAutomaticDimension
        homeTableView.estimatedRowHeight = 425
       
         self.homeTableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
        let value = UIInterfaceOrientation.Portrait.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        user_data1 = dataObj.fetchAllRecord()
        ////print(user_data1.count)
        user_data = user_data1.reverse()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
        homeTableView.hidden = false
        homeTableView.setNeedsLayout()
        homeTableView.layoutIfNeeded()
         self.homeTableView.reloadData()
    }
    @IBAction func longPress(sender: UILongPressGestureRecognizer)
    {
        if (sender.state == .Began){
        self.performSegueWithIdentifier("open_file", sender: nil)
        }
    }
    
//    func Long() {
//        //print("Long press")
//        self.performSegueWithIdentifier("open_file", sender: nil)
//    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return user_data.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let deviceType = UIDevice.currentDevice().model
        
       /* if (UIDevice().userInterfaceIdiom == .Phone && (UIScreen.mainScreen().nativeBounds.height == 960 || UIScreen.mainScreen().nativeBounds.height == 1136))
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("homeIphone4", forIndexPath: indexPath) as! homeTableViewCell
            let usr_dt = user_data.objectAtIndex(indexPath.row)
            let F_name = (usr_dt.valueForKey("firstName")) as! String
            let L_name = (usr_dt.valueForKey("lastName")) as! String
            var age = (usr_dt.valueForKey("age")) as! String
            let gender = (usr_dt.valueForKey("sex")) as! String
            let height = (usr_dt.valueForKey("height")) as! String
            let weight = (usr_dt.valueForKey("weight")) as! String
            let birth = (usr_dt.valueForKey("birth_palce")) as! String
            let citi = (usr_dt.valueForKey("citizenship")) as! String
            let score = (usr_dt.valueForKey("score")) as! String
            let EnrollDt = (usr_dt.valueForKey("enroll_date")) as! String
            let old_img = (usr_dt.valueForKey("oldImage")) as! String
            let new_img = (usr_dt.valueForKey("newImage")) as! String
            cell.NameOfPerson.text = F_name+" "+L_name
            if(age == "0"){
                age = "-"
            }
            cell.AgeAndGender.text = age+", "+gender
            if (deviceType == "iPhone") {
                let oldBounds:CGRect = cell.AgeAndGender.bounds
                cell.AgeAndGender.sizeToFit()
                cell.AgeAndGender.frame.size.width = oldBounds.size.width
            }
            
            if(height == "-"){
                cell.heightOfPerson.text = height
            }
            else
            {
                cell.heightOfPerson.text = height+"\""
            }
            cell.weightOfPerson.text = weight
            cell.birthOfPerson.text = birth
            if (deviceType == "iPhone") {
                let oldBounds:CGRect = cell.birthOfPerson.bounds
                cell.birthOfPerson.sizeToFit()
               // cell.birthOfPerson.frame.size.width = oldBounds.size.width
            }
            cell.citizenshipOfPerson.text = citi
            if (deviceType == "iPhone") {
                let oldBounds:CGRect = cell.citizenshipOfPerson.bounds
                cell.citizenshipOfPerson.sizeToFit()
               // cell.citizenshipOfPerson.frame.size.width = oldBounds.size.width
            }
            cell.scoreOfPerson.text = score
            cell.enrollDtOfPerson.text = EnrollDt
            cell.dateSearched.text = (usr_dt.valueForKey("search_date")) as! String
            let lattSearch_val = (usr_dt.valueForKey("lattitudeSearch")) as! Double
            let longSearch_val = (usr_dt.valueForKey("longitudeSearch")) as! Double
            if(lattSearch_val == 0 && longSearch_val == 0){
                cell.gpsSearched.setTitle("  No GPS available", forState: .Normal)
                cell.gpsSearched.setTitleColor(UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0), forState: .Normal)
            }
            else
            {
                //cell.gpsSearched.setTitleColor(UIColor.blueColor(), forState: .Normal)
                cell.gpsSearched.setTitle("  \(lattSearch_val) \(longSearch_val)", forState: .Normal)
                cell.gpsSearched.setTitleColor(UIColor.blueColor(), forState: .Normal)
                cell.gpsSearched.accessibilityIdentifier = String(indexPath.row)
                cell.gpsSearched.addTarget(self, action: Selector("GPSSearchButtonClickedFromHome:"), forControlEvents: UIControlEvents.TouchUpInside)
            }
            
            cell.viewFromDevice.layer.cornerRadius = 50
            cell.viewFromDevice.clipsToBounds = true
            cell.viewFromServer.layer.cornerRadius = 50
            cell.viewFromServer.clipsToBounds = true
            cell.ImageFromDevice.image = getImage(old_img)
            cell.ImageFromServer.image = getImage(new_img)
            cell.closeButton.accessibilityIdentifier = String(indexPath.row)
            cell.closeButton.addTarget(self, action: Selector("closeButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
            cell.detailsButton.accessibilityIdentifier = String(indexPath.row)
            cell.detailsButton.addTarget(self, action: Selector("detailsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
            cell.emailButton.accessibilityIdentifier = String(indexPath.row)
            cell.emailButton.addTarget(self, action: Selector("emailButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
            return cell

        }*/
//        else
      //  {
         if (deviceType == "iPhone")
         {
//            if((UIScreen.mainScreen().nativeBounds.height == 960 || UIScreen.mainScreen().nativeBounds.height == 1136))
//            {
            
                let cell : UITableViewCell = UITableViewCell(style: .Default, reuseIdentifier: "responsive")
                //let count = (user_data.count) - 1
                let usr_dt = user_data.objectAtIndex(indexPath.row)
                let F_name = (usr_dt.valueForKey("firstName")) as! String
                let L_name = (usr_dt.valueForKey("lastName")) as! String
                var age = (usr_dt.valueForKey("age")) as! String
                let gender = (usr_dt.valueForKey("sex")) as! String
                let height = (usr_dt.valueForKey("height")) as! String
                let weight = (usr_dt.valueForKey("weight")) as! String
                let birth = (usr_dt.valueForKey("birth_palce")) as! String
                let citi = (usr_dt.valueForKey("citizenship")) as! String
                let score = (usr_dt.valueForKey("score")) as! String
                let EnrollDt = (usr_dt.valueForKey("enroll_date")) as! String
                let old_img = (usr_dt.valueForKey("oldImage")) as! String
                let new_img = (usr_dt.valueForKey("newImage")) as! String
                
                // details view
                var detailView = UIView()
                var searchView = UIView()
                var searchLabel = UILabel()
                var searchImg = UIImageView()
                var matchView = UIView()
                var matchLabel = UILabel()
                var matchImg = UIImageView()
                var nameLabel = UILabel()
                var ageLabel  = UILabel()
                var ageAndGenderLabel  = UILabel()
                var heightLabel  = UILabel()
                var heightValueLabel  = UILabel()
                var weightLabel  = UILabel()
                var weightValueLabel  = UILabel()
                
                detailView = UIView(frame: CGRectMake(105, 5, self.view.frame.size.width - 105 , 425))
                searchView = UIView(frame: CGRectMake(5, 46, 100 , 100))
                searchImg = UIImageView(frame: CGRectMake(0, 0, 100 , 100))
                matchView = UIView(frame: CGRectMake(5, 183, 100 , 100))
                matchImg = UIImageView(frame: CGRectMake(0, 0, 100 , 100))
                searchLabel = UILabel(frame: CGRectMake(5, 154, 100, 20))
                matchLabel = UILabel(frame: CGRectMake(5, 291, 100, 20))
                // name
                
                nameLabel = UILabel(frame: CGRectMake(5, 8, 190, 20))
                nameLabel.numberOfLines = 0
                //nameLabel.font = nameLabel.font.fontWithSize(18)
                nameLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                nameLabel.font = UIFont.boldSystemFontOfSize(18)
                nameLabel.text = F_name+" "+L_name
                nameLabel.sizeToFit()
                
                // age
                ageLabel  = UILabel(frame: CGRectMake(5, nameLabel.frame.size.height + 16, 36, 20))
                ageLabel.text = "Age:"
                ageLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                ageLabel.font = UIFont.boldSystemFontOfSize(14)
                // age and gender
                ageAndGenderLabel  = UILabel(frame: CGRectMake(46, nameLabel.frame.size.height + 16, 150, 20))
                ageAndGenderLabel.numberOfLines = 0
                ageAndGenderLabel.font = ageAndGenderLabel.font.fontWithSize(14)
                ageAndGenderLabel.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                ageAndGenderLabel.text = age+", "+gender
                ageAndGenderLabel.sizeToFit()
            //ageAndGenderLabel.backgroundColor = UIColor.yellowColor()
                // height label
                heightLabel  = UILabel(frame: CGRectMake(5, nameLabel.frame.size.height + ageAndGenderLabel.frame.size.height + 24, 55, 20))
                heightLabel.text = "Height:"
                heightLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                heightLabel.font = UIFont.boldSystemFontOfSize(14)
                //heightLabel.backgroundColor = UIColor.redColor()
                // height value label
                
                heightValueLabel  = UILabel(frame: CGRectMake(heightLabel.frame.size.width + 5,nameLabel.frame.size.height + ageAndGenderLabel.frame.size.height + 24 , (detailView.frame.size.width/2) - (heightLabel.frame.size.width+5) , 20))
                heightValueLabel.numberOfLines = 0
                heightValueLabel.font = heightValueLabel.font.fontWithSize(14)
                ageAndGenderLabel.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                if(height == "-"){
                    heightValueLabel.text = height
                }
                else
                {
                    heightValueLabel.text = height+"\""
                }
                
                //ageAndGenderLabel.sizeToFit()
                
                //weight label
                             
                weightLabel  = UILabel(frame: CGRectMake(detailView.frame.size.width/2, nameLabel.frame.size.height + ageAndGenderLabel.frame.size.height + 24, 55, 20))
                weightLabel.text = "Weight:"
                weightLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                weightLabel.font = UIFont.boldSystemFontOfSize(14)

               //weight value label
                
                weightValueLabel  = UILabel(frame: CGRectMake((detailView.frame.size.width/2)+(weightLabel.frame.size.width+3), nameLabel.frame.size.height + ageAndGenderLabel.frame.size.height + 24, (detailView.frame.size.width/2) - (weightLabel.frame.size.width+10) , 20))
                weightValueLabel.numberOfLines = 0
                weightValueLabel.font = weightValueLabel.font.fontWithSize(14)
                weightValueLabel.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                
                weightValueLabel.text = weight
                
               //place of birth label
                let pobY = (nameLabel.frame.size.height + ageAndGenderLabel.frame.size.height + 24) + (weightLabel.frame.size.height + 8)
               let PobLabel  = UILabel(frame: CGRectMake(5, pobY, 104, 20))
                PobLabel.text = "Place of Birth:"
                PobLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                PobLabel.font = UIFont.boldSystemFontOfSize(14)
                
                // pob value
                var PobValue  = UILabel(frame: CGRectMake(PobLabel.frame.size.width + 8, pobY, detailView.frame.size.width - (PobLabel.frame.size.width+8), 20))
                PobValue.numberOfLines = 0
                PobValue.font = PobValue.font.fontWithSize(14)
                PobValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                PobValue.text = birth
                PobValue.sizeToFit()
                
                //citizenship Label
                let citiY = pobY + (PobValue.frame.size.height + 8)
                let citiLabel  = UILabel(frame: CGRectMake(5, citiY, 91, 20))
                citiLabel.text = "Citizenship:"
                citiLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                citiLabel.font = UIFont.boldSystemFontOfSize(14)
                
                //citizenship value
                var citiValue  = UILabel(frame: CGRectMake(citiLabel.frame.size.width + 8, citiY, detailView.frame.size.width - (citiLabel.frame.size.width+8), 20))
                citiValue.numberOfLines = 0
                citiValue.font = citiValue.font.fontWithSize(14)
                citiValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                citiValue.text = citi
                citiValue.sizeToFit()
                
                //score Label
                let scoreY = citiY + (citiValue.frame.size.height + 8)
                let scoreLabel  = UILabel(frame: CGRectMake(5, scoreY, 53, 20))
                scoreLabel.text = "Score:"
                scoreLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                scoreLabel.font = UIFont.boldSystemFontOfSize(14)

                //score value
                var scoreValue  = UILabel(frame: CGRectMake(scoreLabel.frame.size.width + 8, scoreY+3, detailView.frame.size.width - (scoreLabel.frame.size.width+8), 20))
                scoreValue.numberOfLines = 0
                scoreValue.font = scoreValue.font.fontWithSize(14)
                scoreValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                scoreValue.text = score
                scoreValue.sizeToFit()

                //enroll date label
                let enrollY = scoreY + (scoreValue.frame.size.height + 11)
                let enrollLabel  = UILabel(frame: CGRectMake(5, enrollY, 85, 20))
                enrollLabel.text = "Enroll Date:"
                enrollLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                enrollLabel.font = UIFont.boldSystemFontOfSize(14)
                
                // enroll value
                var enrollValue  = UILabel(frame: CGRectMake(enrollLabel.frame.size.width + 8, enrollY+3, detailView.frame.size.width - (enrollLabel.frame.size.width+8), 20))
                enrollValue.numberOfLines = 0
                enrollValue.font = enrollValue.font.fontWithSize(14)
                enrollValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                enrollValue.text = EnrollDt
                enrollValue.sizeToFit()
                //date seen label
                let seenY = enrollY + (enrollValue.frame.size.height + 14)
                let seenLabel  = UILabel(frame: CGRectMake(5, seenY, 85, 20))
                seenLabel.text = "Date Seen:"
                seenLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                seenLabel.font = UIFont.boldSystemFontOfSize(14)

                // date seen value
                var seenValue  = UILabel(frame: CGRectMake(seenLabel.frame.size.width + 8, seenY+3, detailView.frame.size.width - (seenLabel.frame.size.width+8), 20))
                seenValue.numberOfLines = 0
                seenValue.font = seenValue.font.fontWithSize(14)
                seenValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                seenValue.text = (usr_dt.valueForKey("search_date")) as! String
                seenValue.sizeToFit()
                
                //gps label
                let gpsY = seenY + (seenValue.frame.size.height + 19)
                let gpsLabel  = UILabel(frame: CGRectMake(5, gpsY, 103, 20))
            gpsLabel.text = "GPS (Search):"
                gpsLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                gpsLabel.font = UIFont.boldSystemFontOfSize(14)
                
                //gps Search button and title label
                var gpsValueLabel = UILabel(frame: CGRectMake(gpsLabel.frame.size.width + 8, gpsY, detailView.frame.size.width - (gpsLabel.frame.size.width+8), 20))
                gpsValueLabel.numberOfLines = 0
                gpsValueLabel.lineBreakMode = .ByCharWrapping
                gpsValueLabel.font = gpsValueLabel.font.fontWithSize(12)
            
                var gpsValue = UIButton(frame: CGRectMake(gpsLabel.frame.size.width + 8, gpsY, detailView.frame.size.width - (gpsLabel.frame.size.width+8), 20))
                //gpsValue.titleLabel?.preferredMaxLayoutWidth = 20
                gpsValue.titleLabel?.font = UIFont.systemFontOfSize(12)
                gpsValue.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
               // gpsValue.backgroundColor = UIColor.brownColor()
                let lattSearch_val = (usr_dt.valueForKey("lattitudeSearch")) as! Double
                let longSearch_val = (usr_dt.valueForKey("longitudeSearch")) as! Double
                if(lattSearch_val == 0 && longSearch_val == 0)
                {
//                    gpsValue.setTitle("No GPS available", forState: .Normal)
//                    gpsValue.titleLabel?.lineBreakMode = .ByCharWrapping
//                    gpsValue.titleLabel?.numberOfLines = 0
//                    gpsValue.setTitleColor(UIColor.blackColor(), forState: .Normal)
                    gpsValueLabel.textColor = UIColor.darkGrayColor()
                    gpsValueLabel.text = "No GPS available"
                }
                else
                {
//                    gpsValue.setTitle("\(lattSearch_val) \(longSearch_val)", forState: .Normal)
//                    gpsValue.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
//                    gpsValue.titleLabel?.numberOfLines = 0
//                    gpsValue.setTitleColor(UIColor.blueColor(), forState: .Normal)
                    gpsValueLabel.textColor = UIColor.blueColor()
                    gpsValueLabel.text = "\(lattSearch_val) \(longSearch_val)"
                    gpsValue.accessibilityIdentifier = String(indexPath.row)
                    gpsValue.addTarget(self, action: Selector("GPSSearchButtonClickedFromHome:"), forControlEvents: UIControlEvents.TouchUpInside)
                }
                
                gpsValueLabel.sizeToFit()
                gpsValue.layoutIfNeeded()
                gpsValue.frame = CGRectMake(gpsLabel.frame.size.width + 8, gpsY, detailView.frame.size.width - (gpsLabel.frame.size.width+8),gpsValueLabel.frame.size.height)

            // code by shrunkita
            
            //gps search label
            let gpsEnrollY = gpsY + (gpsValue.frame.size.height + 22)
            let gpsEnrollLabel  = UILabel(frame: CGRectMake(5, gpsEnrollY, 121, 20))
            gpsEnrollLabel.text = "GPS (Enroll):"
            gpsEnrollLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
            gpsEnrollLabel.font = UIFont.boldSystemFontOfSize(14)

            
            //gps Enroll button and title label
            var gpsEnrollValueLabel = UILabel(frame: CGRectMake(gpsEnrollLabel.frame.size.width + 8, gpsEnrollY, detailView.frame.size.width - (gpsEnrollLabel.frame.size.width+8), 20))
            gpsEnrollValueLabel.numberOfLines = 0
            gpsEnrollValueLabel.lineBreakMode = .ByCharWrapping
            gpsEnrollValueLabel.font = gpsEnrollValueLabel.font.fontWithSize(12)
            
            var gpsEnrollValue = UIButton(frame: CGRectMake(gpsEnrollValueLabel.frame.size.width + 8, gpsEnrollY, detailView.frame.size.width - (gpsEnrollValueLabel.frame.size.width+8), 20))
            //gpsValue.titleLabel?.preferredMaxLayoutWidth = 20
            gpsEnrollValue.titleLabel?.font = UIFont.systemFontOfSize(12)
            gpsEnrollValue.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
            // gpsValue.backgroundColor = UIColor.brownColor()
            let lattEnroll_val = (usr_dt.valueForKey("lattitude")) as! Double
            let longEnroll_val = (usr_dt.valueForKey("longitude")) as! Double
            if(lattEnroll_val == 0 && longEnroll_val == 0)
            {
                //                    gpsValue.setTitle("No GPS available", forState: .Normal)
                //                    gpsValue.titleLabel?.lineBreakMode = .ByCharWrapping
                //                    gpsValue.titleLabel?.numberOfLines = 0
                //                    gpsValue.setTitleColor(UIColor.blackColor(), forState: .Normal)
                gpsEnrollValueLabel.textColor = UIColor.darkGrayColor()
                gpsEnrollValueLabel.text = "No GPS available"
            }
            else
            {
                //                    gpsValue.setTitle("\(lattSearch_val) \(longSearch_val)", forState: .Normal)
                //                    gpsValue.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
                //                    gpsValue.titleLabel?.numberOfLines = 0
                //                    gpsValue.setTitleColor(UIColor.blueColor(), forState: .Normal)
                gpsEnrollValueLabel.textColor = UIColor.blueColor()
                gpsEnrollValueLabel.text = "\(lattEnroll_val) \(longEnroll_val)"
                gpsEnrollValue.accessibilityIdentifier = String(indexPath.row)
                gpsEnrollValue.addTarget(self, action: Selector("GPSSearchButtonClickedFromHome:"), forControlEvents: UIControlEvents.TouchUpInside)
            }
            
            gpsEnrollValueLabel.sizeToFit()
            gpsEnrollValueLabel.layoutIfNeeded()
            gpsEnrollValueLabel.frame = CGRectMake(gpsValueLabel.frame.size.width + 8, gpsEnrollY, detailView.frame.size.width - (gpsValueLabel.frame.size.width+8),gpsEnrollValueLabel.frame.size.height)

            // end with enroll gps
            
                //cancel button
                var cancelBtn = UIButton(frame: CGRectMake(detailView.frame.size.width - 40,0,40,40))
                cancelBtn.setBackgroundImage(UIImage(named: "smallCross"), forState: .Normal)
                cancelBtn.accessibilityIdentifier = String(indexPath.row)
                cancelBtn.addTarget(self, action: Selector("closeButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                
               // email and details button
               var buttonsView = UIView(frame: CGRectMake((self.view.frame.size.width/2)-50,detailView.frame.size.height+30,100,40))
                //buttonsView.backgroundColor = UIColor.redColor()
                var detailsBtn = UIButton(frame: CGRectMake(0,8,40,40))
        
                detailsBtn.setBackgroundImage(UIImage(named: "details"), forState: .Normal)
                detailsBtn.accessibilityIdentifier = String(indexPath.row)
                detailsBtn.addTarget(self, action: Selector("detailsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
            
                var emailBtn = UIButton(frame: CGRectMake(60,8,40,40))
                
                emailBtn.setBackgroundImage(UIImage(named: "smallAlert"), forState: .Normal)
                emailBtn.accessibilityIdentifier = String(indexPath.row)
                emailBtn.addTarget(self, action: Selector("emailButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
                
                buttonsView.addSubview(detailsBtn)
                buttonsView.addSubview(emailBtn)
                
                searchView.layer.cornerRadius = 50
                searchView.clipsToBounds = true
                matchView.layer.cornerRadius = 50
                matchView.clipsToBounds = true
                searchImg.image = getImage(old_img)
                matchImg.image = getImage(new_img)
                searchLabel.text = "Searched"
                searchLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                searchLabel.textAlignment = .Center
                searchLabel.font = searchLabel.font.fontWithSize(17)
                matchLabel.text = "Matched"
                matchLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                matchLabel.textAlignment = .Center
                matchLabel.font = matchLabel.font.fontWithSize(17)
                
                detailView.addSubview(nameLabel)
                detailView.addSubview(ageLabel)
                detailView.addSubview(ageAndGenderLabel)
                detailView.addSubview(heightLabel)
                detailView.addSubview(heightValueLabel)
                detailView.addSubview(weightLabel)
                detailView.addSubview(weightValueLabel)
                detailView.addSubview(PobLabel)
                detailView.addSubview(PobValue)
                detailView.addSubview(citiLabel)
                detailView.addSubview(citiValue)
                detailView.addSubview(scoreLabel)
                detailView.addSubview(scoreValue)
                detailView.addSubview(enrollLabel)
                detailView.addSubview(enrollValue)
                detailView.addSubview(seenLabel)
                detailView.addSubview(seenValue)
                detailView.addSubview(gpsLabel)
                detailView.addSubview(gpsValueLabel)
                detailView.addSubview(gpsValue)
                detailView.addSubview(cancelBtn)
                detailView.addSubview(gpsEnrollLabel)
                detailView.addSubview(gpsEnrollValueLabel)
                detailView.addSubview(gpsEnrollValue)

            
                
                //detailView.backgroundColor = UIColor.blueColor()
                //detailView.backgroundColor = UIColor.blueColor()
                //nameLabel.backgroundColor = UIColor.whiteColor()
                //ageAndGenderLabel.backgroundColor = UIColor.whiteColor()
                //ageLabel.backgroundColor = UIColor.whiteColor()
                searchView.addSubview(searchImg)
                matchView.addSubview(matchImg)
                cell.contentView.addSubview(searchView)
                cell.contentView.addSubview(matchView)
                cell.contentView.addSubview(searchLabel)
                cell.contentView.addSubview(matchLabel)
                detailView.frame = CGRectMake(105, 5, self.view.frame.size.width - 105, gpsEnrollY + (gpsEnrollValue.frame.size.height + 5) )
                cell.contentView.addSubview(detailView)
                buttonsView.frame = CGRectMake((self.view.frame.size.width/2)-50,detailView.frame.size.height+10,100,40)
                cell.contentView.addSubview(buttonsView)
                cell.contentView.backgroundColor = UIColor(red: 238/255.0, green: 238/255.0, blue: 238/255.0, alpha: 1)
                cell.selectionStyle = .None
                
//                cell.closeBtn.accessibilityIdentifier = String(indexPath.row)
//                cell.closeBtn.addTarget(self, action: Selector("closeButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
//                
//                
//                cell.detailsBtn.accessibilityIdentifier = String(indexPath.row)
//                cell.detailsBtn.addTarget(self, action: Selector("detailsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
//                cell.emailBtn.accessibilityIdentifier = String(indexPath.row)
//                cell.emailBtn.addTarget(self, action: Selector("emailButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
//                cell.layoutSubviews()
//                cell.setNeedsLayout()
                homeTableView.separatorStyle = .SingleLine
                homeTableView.tableFooterView = UIView()
                return cell
//            }
//            else
//            {
//        let cell = tableView.dequeueReusableCellWithIdentifier("homeScreen", forIndexPath: indexPath) as! homeTableViewCell
//        //let count = (user_data.count) - 1
//        let usr_dt = user_data.objectAtIndex(indexPath.row)
//        let F_name = (usr_dt.valueForKey("firstName")) as! String
//        let L_name = (usr_dt.valueForKey("lastName")) as! String
//        var age = (usr_dt.valueForKey("age")) as! String
//        let gender = (usr_dt.valueForKey("sex")) as! String
//        let height = (usr_dt.valueForKey("height")) as! String
//        let weight = (usr_dt.valueForKey("weight")) as! String
//        let birth = (usr_dt.valueForKey("birth_palce")) as! String
//        let citi = (usr_dt.valueForKey("citizenship")) as! String
//        let score = (usr_dt.valueForKey("score")) as! String
//        let EnrollDt = (usr_dt.valueForKey("enroll_date")) as! String
//        let old_img = (usr_dt.valueForKey("oldImage")) as! String
//        let new_img = (usr_dt.valueForKey("newImage")) as! String
//        cell.NameOfPerson.text = F_name+" "+L_name
//        if(age == "0"){
//            age = "-"
//        }
//       // cell.NameOfPerson.sizeToFit()
//        cell.AgeAndGender.text = age+", "+gender
//        if (deviceType == "iPhone")
//        {
//            let oldBounds:CGRect = cell.AgeAndGender.bounds
//            cell.AgeAndGender.sizeToFit()
//            cell.AgeAndGender.frame.size.width = oldBounds.size.width
//        }
//
//        if(height == "-")
//        {
//            cell.heightOfPerson.text = height
//        }
//        else
//        {
//        cell.heightOfPerson.text = height+"\""
//        }
//        cell.weightOfPerson.text = weight
//        cell.birthOfPerson.text = birth
//        if (deviceType == "iPhone") {
//            let oldBounds:CGRect = cell.birthOfPerson.bounds
//            cell.birthOfPerson.sizeToFit()
//            cell.birthOfPerson.frame.size.width = oldBounds.size.width
//        }
//        cell.citizenshipOfPerson.text = citi
//        if (deviceType == "iPhone") {
//            let oldBounds:CGRect = cell.citizenshipOfPerson.bounds
//            cell.citizenshipOfPerson.sizeToFit()
//            cell.citizenshipOfPerson.frame.size.width = oldBounds.size.width
//        }
//        cell.scoreOfPerson.text = score
//        cell.enrollDtOfPerson.text = EnrollDt
//        cell.dateSearched.text = (usr_dt.valueForKey("search_date")) as! String
//        let lattSearch_val = (usr_dt.valueForKey("lattitudeSearch")) as! Double
//        let longSearch_val = (usr_dt.valueForKey("longitudeSearch")) as! Double
//        if(lattSearch_val == 0 && longSearch_val == 0){
//            cell.gpsSearched.setTitle("  No GPS available", forState: .Normal)
//             cell.gpsSearched.setTitleColor(UIColor.blackColor(), forState: .Normal)
//        }
//        else{
//            //cell.gpsSearched.setTitleColor(UIColor.blueColor(), forState: .Normal)
//            cell.gpsSearched.setTitle("  \(lattSearch_val) \(longSearch_val)", forState: .Normal)
//            cell.gpsSearched.setTitleColor(UIColor.blueColor(), forState: .Normal)
//            cell.gpsSearched.accessibilityIdentifier = String(indexPath.row)
//            cell.gpsSearched.addTarget(self, action: Selector("GPSSearchButtonClickedFromHome:"), forControlEvents: UIControlEvents.TouchUpInside)
//        }
//
//        cell.viewFromDevice.layer.cornerRadius = 50
//        cell.viewFromDevice.clipsToBounds = true
//        cell.viewFromServer.layer.cornerRadius = 50
//        cell.viewFromServer.clipsToBounds = true
//        cell.ImageFromDevice.image = getImage(old_img)
//        cell.ImageFromServer.image = getImage(new_img)
//        cell.closeButton.accessibilityIdentifier = String(indexPath.row)
//        cell.closeButton.addTarget(self, action: Selector("closeButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
//        cell.detailsButton.accessibilityIdentifier = String(indexPath.row)
//        cell.detailsButton.addTarget(self, action: Selector("detailsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
//        cell.emailButton.accessibilityIdentifier = String(indexPath.row)
//        cell.emailButton.addTarget(self, action: Selector("emailButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
//            cell.layoutSubviews()
//            cell.setNeedsLayout()
//        return cell
//        }
    }
        else
         {
            //for ipad
            let cell = tableView.dequeueReusableCellWithIdentifier("homeIphone4", forIndexPath: indexPath) as! homeTableViewCell
            let usr_dt = user_data.objectAtIndex(indexPath.row)
            let F_name = (usr_dt.valueForKey("firstName")) as! String
            let L_name = (usr_dt.valueForKey("lastName")) as! String
            var age = (usr_dt.valueForKey("age")) as! String
            let gender = (usr_dt.valueForKey("sex")) as! String
            let height = (usr_dt.valueForKey("height")) as! String
            let weight = (usr_dt.valueForKey("weight")) as! String
            let birth = (usr_dt.valueForKey("birth_palce")) as! String
            let citi = (usr_dt.valueForKey("citizenship")) as! String
            let score = (usr_dt.valueForKey("score")) as! String
            let EnrollDt = (usr_dt.valueForKey("enroll_date")) as! String
            let old_img = (usr_dt.valueForKey("oldImage")) as! String
            let new_img = (usr_dt.valueForKey("newImage")) as! String
            cell.NameOfPerson.text = F_name+" "+L_name
            if(age == "0"){
                age = "-"
            }
            cell.AgeAndGender.text = age+", "+gender
            if (deviceType == "iPhone") {
                let oldBounds:CGRect = cell.AgeAndGender.bounds
                cell.AgeAndGender.sizeToFit()
                cell.AgeAndGender.frame.size.width = oldBounds.size.width
            }
            
            if(height == "-"){
                cell.heightOfPerson.text = height
            }
            else
            {
                cell.heightOfPerson.text = height+"\""
            }
            cell.weightOfPerson.text = weight
            cell.birthOfPerson.text = birth
            if (deviceType == "iPhone") {
                let oldBounds:CGRect = cell.birthOfPerson.bounds
                cell.birthOfPerson.sizeToFit()
                // cell.birthOfPerson.frame.size.width = oldBounds.size.width
            }
            cell.citizenshipOfPerson.text = citi
            if (deviceType == "iPhone") {
                let oldBounds:CGRect = cell.citizenshipOfPerson.bounds
                cell.citizenshipOfPerson.sizeToFit()
                // cell.citizenshipOfPerson.frame.size.width = oldBounds.size.width
            }
            cell.scoreOfPerson.text = score
            cell.enrollDtOfPerson.text = EnrollDt
            cell.dateSearched.text = (usr_dt.valueForKey("search_date")) as! String
            let lattSearch_val = (usr_dt.valueForKey("lattitudeSearch")) as! Double
            let longSearch_val = (usr_dt.valueForKey("longitudeSearch")) as! Double
            
            let lattEnroll_val = (usr_dt.valueForKey("lattitude")) as! Double
            let longEnroll_val = (usr_dt.valueForKey("longitude")) as! Double

            if(lattSearch_val == 0 && longSearch_val == 0){
                cell.gpsSearched.setTitle("  No GPS available", forState: .Normal)
                cell.gpsSearched.setTitleColor(UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0), forState: .Normal)
            }
            else
            {
                //cell.gpsSearched.setTitleColor(UIColor.blueColor(), forState: .Normal)
                cell.gpsSearched.setTitle("  \(lattSearch_val) \(longSearch_val)", forState: .Normal)
                cell.gpsSearched.setTitleColor(UIColor.blueColor(), forState: .Normal)
                cell.gpsSearched.accessibilityIdentifier = String(indexPath.row)
                cell.gpsSearched.addTarget(self, action: #selector(homeScreenViewController.GPSSearchButtonClickedFromHome(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            }
            
            if(lattEnroll_val == 0 && longEnroll_val == 0){
                cell.gpsEnrollButton.setTitle("  No GPS available", forState: .Normal)
                cell.gpsEnrollButton.setTitleColor(UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0), forState: .Normal)
            }
            else
            {
                //cell.gpsSearched.setTitleColor(UIColor.blueColor(), forState: .Normal)
                cell.gpsEnrollButton.setTitle("  \(lattSearch_val) \(longSearch_val)", forState: .Normal)
                cell.gpsEnrollButton.setTitleColor(UIColor.blueColor(), forState: .Normal)
                cell.gpsEnrollButton.accessibilityIdentifier = String(indexPath.row)
                cell.gpsEnrollButton.addTarget(self, action: #selector(homeScreenViewController.GPSEnrollButtonClickedFromHome(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            }
            
            
            
            cell.viewFromDevice.layer.cornerRadius = 50
            cell.viewFromDevice.clipsToBounds = true
            cell.viewFromServer.layer.cornerRadius = 50
            cell.viewFromServer.clipsToBounds = true
            cell.ImageFromDevice.image = getImage(old_img)
            cell.ImageFromServer.image = getImage(new_img)
            cell.closeButton.accessibilityIdentifier = String(indexPath.row)
            cell.closeButton.addTarget(self, action: Selector("closeButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
            cell.detailsButton.accessibilityIdentifier = String(indexPath.row)
            cell.detailsButton.addTarget(self, action: Selector("detailsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
            cell.emailButton.accessibilityIdentifier = String(indexPath.row)
            cell.emailButton.addTarget(self, action: Selector("emailButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
            return cell
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if (UIDevice().userInterfaceIdiom == .Phone)
        {
//            if((UIScreen.mainScreen().nativeBounds.height == 960 || UIScreen.mainScreen().nativeBounds.height == 1136))
//            {
               let cell : UITableViewCell = UITableViewCell(style: .Default, reuseIdentifier: "responsive")
                let usr_dt = user_data.objectAtIndex(indexPath.row)
                let F_name = (usr_dt.valueForKey("firstName")) as! String
                let L_name = (usr_dt.valueForKey("lastName")) as! String
                var age = (usr_dt.valueForKey("age")) as! String
                let gender = (usr_dt.valueForKey("sex")) as! String
                let height = (usr_dt.valueForKey("height")) as! String
                let weight = (usr_dt.valueForKey("weight")) as! String
                let birth = (usr_dt.valueForKey("birth_palce")) as! String
                let citi = (usr_dt.valueForKey("citizenship")) as! String
                let score = (usr_dt.valueForKey("score")) as! String
                let EnrollDt = (usr_dt.valueForKey("enroll_date")) as! String
                let old_img = (usr_dt.valueForKey("oldImage")) as! String
                let new_img = (usr_dt.valueForKey("newImage")) as! String
                
                // details view
                var detailView = UIView()
                var nameLabel = UILabel()
                var ageLabel  = UILabel()
                var ageAndGenderLabel  = UILabel()
                var heightLabel  = UILabel()
                var heightValueLabel  = UILabel()
                var weightLabel  = UILabel()
                var weightValueLabel  = UILabel()

                 detailView = UIView(frame: CGRectMake(105, 5, self.view.frame.size.width - 105 , 425))
                nameLabel = UILabel(frame: CGRectMake(5, 8, 190, 20))
                nameLabel.numberOfLines = 0
                //nameLabel.font = nameLabel.font.fontWithSize(18)
                nameLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                nameLabel.font = UIFont.boldSystemFontOfSize(18)
                nameLabel.text = F_name+" "+L_name
                nameLabel.sizeToFit()
                
                // age
                ageLabel  = UILabel(frame: CGRectMake(5, nameLabel.frame.size.height + 16, 36, 20))
                ageLabel.text = "Age:"
                ageLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                ageLabel.font = UIFont.boldSystemFontOfSize(14)
                // age and gender
                ageAndGenderLabel  = UILabel(frame: CGRectMake(46, nameLabel.frame.size.height + 16, 150, 20))
                ageAndGenderLabel.numberOfLines = 0
                ageAndGenderLabel.font = ageAndGenderLabel.font.fontWithSize(14)
                ageAndGenderLabel.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                ageAndGenderLabel.text = age+", "+gender
                ageAndGenderLabel.sizeToFit()
                // height label
                heightLabel  = UILabel(frame: CGRectMake(5, nameLabel.frame.size.height + ageAndGenderLabel.frame.size.height + 24, 55, 20))
                heightLabel.text = "Height:"
                heightLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                heightLabel.font = UIFont.boldSystemFontOfSize(14)
                
                // height value label
                
                heightValueLabel  = UILabel(frame: CGRectMake(heightLabel.frame.size.width + 5,nameLabel.frame.size.height + ageAndGenderLabel.frame.size.height + 24 , (detailView.frame.size.width/2) - (heightLabel.frame.size.width+5) , 20))
                heightValueLabel.numberOfLines = 0
                heightValueLabel.font = heightValueLabel.font.fontWithSize(14)
                ageAndGenderLabel.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                if(height == "-"){
                    heightValueLabel.text = height
                }
                else
                {
                    heightValueLabel.text = height+"\""
                }
                
                //ageAndGenderLabel.sizeToFit()
                
                //weight label
                
                weightLabel  = UILabel(frame: CGRectMake(detailView.frame.size.width/2, nameLabel.frame.size.height + ageAndGenderLabel.frame.size.height + 24, 55, 20))
                weightLabel.text = "Weight:"
                weightLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                weightLabel.font = UIFont.boldSystemFontOfSize(14)
                
                //weight value label
                
                weightValueLabel  = UILabel(frame: CGRectMake((detailView.frame.size.width/2)+(weightLabel.frame.size.width+3), nameLabel.frame.size.height + ageAndGenderLabel.frame.size.height + 24, (detailView.frame.size.width/2) - (weightLabel.frame.size.width+10) , 20))
                weightValueLabel.numberOfLines = 0
                weightValueLabel.font = weightValueLabel.font.fontWithSize(14)
                weightValueLabel.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                
                weightValueLabel.text = weight
                
                //place of birth label
                let pobY = (nameLabel.frame.size.height + ageAndGenderLabel.frame.size.height + 24) + (weightLabel.frame.size.height + 8)
                let PobLabel  = UILabel(frame: CGRectMake(5, pobY, 104, 20))
                PobLabel.text = "Place of Birth:"
                PobLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                PobLabel.font = UIFont.boldSystemFontOfSize(14)
                
                // pob value
                var PobValue  = UILabel(frame: CGRectMake(PobLabel.frame.size.width + 8, pobY, detailView.frame.size.width - (PobLabel.frame.size.width+8), 20))
                PobValue.numberOfLines = 0
                PobValue.font = PobValue.font.fontWithSize(14)
                PobValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                PobValue.text = birth
                PobValue.sizeToFit()
                
                //citizenship Label
                let citiY = pobY + (PobValue.frame.size.height + 8)
                let citiLabel  = UILabel(frame: CGRectMake(5, citiY, 91, 20))
                citiLabel.text = "Citizenship:"
                citiLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                citiLabel.font = UIFont.boldSystemFontOfSize(14)
                
                //citizenship value
                
                var citiValue  = UILabel(frame: CGRectMake(citiLabel.frame.size.width + 8, citiY, detailView.frame.size.width - (citiLabel.frame.size.width+8), 20))
                citiValue.numberOfLines = 0
                citiValue.font = citiValue.font.fontWithSize(14)
                citiValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                citiValue.text = citi
                citiValue.sizeToFit()
                
                //score Label
                let scoreY = citiY + (citiValue.frame.size.height + 8)
                let scoreLabel  = UILabel(frame: CGRectMake(5, scoreY, 53, 20))
                scoreLabel.text = "Score:"
                scoreLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                scoreLabel.font = UIFont.boldSystemFontOfSize(14)
                
                //score value
                var scoreValue  = UILabel(frame: CGRectMake(scoreLabel.frame.size.width + 8, scoreY+3, detailView.frame.size.width - (scoreLabel.frame.size.width+8), 20))
                scoreValue.numberOfLines = 0
                scoreValue.font = scoreValue.font.fontWithSize(14)
                scoreValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                scoreValue.text = score
                scoreValue.sizeToFit()
                
                //enroll date label
                let enrollY = scoreY + (scoreValue.frame.size.height + 11)
                let enrollLabel  = UILabel(frame: CGRectMake(5, enrollY, 85, 20))
                enrollLabel.text = "Enroll Date:"
                enrollLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                enrollLabel.font = UIFont.boldSystemFontOfSize(14)
                
                // enroll value
                var enrollValue  = UILabel(frame: CGRectMake(enrollLabel.frame.size.width + 8, enrollY+3, detailView.frame.size.width - (enrollLabel.frame.size.width+8), 20))
                enrollValue.numberOfLines = 0
                enrollValue.font = enrollValue.font.fontWithSize(14)
                enrollValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                enrollValue.text = EnrollDt
                enrollValue.sizeToFit()
                //date seen label
                let seenY = enrollY + (enrollValue.frame.size.height + 14)
                let seenLabel  = UILabel(frame: CGRectMake(5, seenY, 85, 20))
                seenLabel.text = "Date Seen:"
                seenLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                seenLabel.font = UIFont.boldSystemFontOfSize(14)
                
                // date seen value
                var seenValue  = UILabel(frame: CGRectMake(seenLabel.frame.size.width + 8, seenY+3, detailView.frame.size.width - (seenLabel.frame.size.width+8), 20))
                seenValue.numberOfLines = 0
                seenValue.font = seenValue.font.fontWithSize(14)
                seenValue.textColor = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
                seenValue.text = (usr_dt.valueForKey("search_date")) as! String
                seenValue.sizeToFit()
                
                //gps label
                let gpsY = seenY + (seenValue.frame.size.height + 19)
                let gpsLabel  = UILabel(frame: CGRectMake(5, gpsY, 103, 20))
                gpsLabel.text = "GPS (search):"
                gpsLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
                gpsLabel.font = UIFont.boldSystemFontOfSize(14)
                
                //gps button and title label
                var gpsValueLabel = UILabel(frame: CGRectMake(gpsLabel.frame.size.width + 8, gpsY, detailView.frame.size.width - (gpsLabel.frame.size.width+8), 20))
                gpsValueLabel.numberOfLines = 0
                gpsValueLabel.lineBreakMode = .ByCharWrapping
                gpsValueLabel.font = gpsValueLabel.font.fontWithSize(12)
            
                var gpsValue = UIButton(frame: CGRectMake(gpsLabel.frame.size.width + 8, gpsY, detailView.frame.size.width - (gpsLabel.frame.size.width+8), 20))
                //gpsValue.titleLabel?.preferredMaxLayoutWidth = 20
                gpsValue.titleLabel?.font = UIFont.systemFontOfSize(12)
                gpsValue.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
                // gpsValue.backgroundColor = UIColor.brownColor()
                let lattSearch_val = (usr_dt.valueForKey("lattitudeSearch")) as! Double
                let longSearch_val = (usr_dt.valueForKey("longitudeSearch")) as! Double
                if(lattSearch_val == 0 && longSearch_val == 0)
                {
//                    gpsValue.setTitle("No GPS available", forState: .Normal)
//                    gpsValue.titleLabel?.lineBreakMode = .ByCharWrapping
//                    gpsValue.titleLabel?.numberOfLines = 0
//                    gpsValue.setTitleColor(UIColor.blackColor(), forState: .Normal)
                    gpsValueLabel.textColor = UIColor.darkGrayColor()
                    gpsValueLabel.text = "No GPS available"
                }
                else
                {
//                    gpsValue.setTitle("\(lattSearch_val) \(longSearch_val)", forState: .Normal)
//                    gpsValue.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
//                    gpsValue.titleLabel?.numberOfLines = 0
//                    gpsValue.setTitleColor(UIColor.blueColor(), forState: .Normal)
                    gpsValueLabel.textColor = UIColor.blueColor()
                    gpsValueLabel.text = "\(lattSearch_val) \(longSearch_val)"

                    gpsValue.accessibilityIdentifier = String(indexPath.row)
                    gpsValue.addTarget(self, action: Selector("GPSSearchButtonClickedFromHome:"), forControlEvents: UIControlEvents.TouchUpInside)
                }
                gpsValueLabel.sizeToFit()
                gpsValue.layoutIfNeeded()
                gpsValue.frame = CGRectMake(gpsLabel.frame.size.width + 8, gpsY, detailView.frame.size.width - (gpsLabel.frame.size.width+8),gpsValueLabel.frame.size.height)
            
            // code by shrunkita
            
            //gps search label
            let gpsEnrollY = gpsY + (gpsValue.frame.size.height + 22)
            let gpsEnrollLabel  = UILabel(frame: CGRectMake(5, gpsEnrollY, 121, 20))
            gpsEnrollLabel.text = "GPS (Enroll):"
            gpsEnrollLabel.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
            gpsEnrollLabel.font = UIFont.boldSystemFontOfSize(14)
            
            
            //gps Enroll button and title label
            var gpsEnrollValueLabel = UILabel(frame: CGRectMake(gpsEnrollLabel.frame.size.width + 8, gpsEnrollY, detailView.frame.size.width - (gpsEnrollLabel.frame.size.width+8), 20))
            gpsEnrollValueLabel.numberOfLines = 0
            gpsEnrollValueLabel.lineBreakMode = .ByCharWrapping
            gpsEnrollValueLabel.font = gpsEnrollValueLabel.font.fontWithSize(12)
            
            var gpsEnrollValue = UIButton(frame: CGRectMake(gpsEnrollValueLabel.frame.size.width + 8, gpsEnrollY, detailView.frame.size.width - (gpsEnrollValueLabel.frame.size.width+8), 20))
            //gpsValue.titleLabel?.preferredMaxLayoutWidth = 20
            gpsEnrollValue.titleLabel?.font = UIFont.systemFontOfSize(12)
            gpsEnrollValue.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
            // gpsValue.backgroundColor = UIColor.brownColor()
            let lattEnroll_val = (usr_dt.valueForKey("lattitude")) as! Double
            let longEnroll_val = (usr_dt.valueForKey("longitude")) as! Double
            if(lattEnroll_val == 0 && longEnroll_val == 0)
            {
                //                    gpsValue.setTitle("No GPS available", forState: .Normal)
                //                    gpsValue.titleLabel?.lineBreakMode = .ByCharWrapping
                //                    gpsValue.titleLabel?.numberOfLines = 0
                //                    gpsValue.setTitleColor(UIColor.blackColor(), forState: .Normal)
                gpsEnrollValueLabel.textColor = UIColor.darkGrayColor()
                gpsEnrollValueLabel.text = "No GPS available"
            }
            else
            {
                //                    gpsValue.setTitle("\(lattSearch_val) \(longSearch_val)", forState: .Normal)
                //                    gpsValue.titleLabel?.lineBreakMode = NSLineBreakMode.ByCharWrapping
                //                    gpsValue.titleLabel?.numberOfLines = 0
                //                    gpsValue.setTitleColor(UIColor.blueColor(), forState: .Normal)
                gpsEnrollValueLabel.textColor = UIColor.blueColor()
                gpsEnrollValueLabel.text = "\(lattEnroll_val) \(longEnroll_val)"
                gpsEnrollValue.accessibilityIdentifier = String(indexPath.row)
                gpsEnrollValue.addTarget(self, action: Selector("GPSEnrollButtonClickedFromHome:"), forControlEvents: UIControlEvents.TouchUpInside)
            }
            
            gpsEnrollValueLabel.sizeToFit()
            gpsEnrollValueLabel.layoutIfNeeded()
            gpsEnrollValueLabel.frame = CGRectMake(gpsValueLabel.frame.size.width + 8, gpsEnrollY, detailView.frame.size.width - (gpsValueLabel.frame.size.width+8),gpsEnrollValueLabel.frame.size.height)
            
            // end with enroll gps

            
                detailView.addSubview(nameLabel)
                detailView.addSubview(ageLabel)
                detailView.addSubview(ageAndGenderLabel)
                detailView.addSubview(heightLabel)
                detailView.addSubview(heightValueLabel)
                detailView.addSubview(weightLabel)
                detailView.addSubview(weightValueLabel)
                detailView.addSubview(PobLabel)
                detailView.addSubview(PobValue)
                detailView.addSubview(citiLabel)
                detailView.addSubview(citiValue)
                detailView.addSubview(scoreLabel)
                detailView.addSubview(scoreValue)
                detailView.addSubview(enrollLabel)
                detailView.addSubview(enrollValue)
                detailView.addSubview(seenLabel)
                detailView.addSubview(seenValue)
                detailView.addSubview(gpsLabel)
                detailView.addSubview(gpsValueLabel)
                detailView.addSubview(gpsValue)
                detailView.addSubview(gpsEnrollLabel)
                detailView.addSubview(gpsEnrollValueLabel)
                detailView.addSubview(gpsEnrollValue)

                detailView.frame = CGRectMake(105, 5, self.view.frame.size.width - 105, gpsEnrollY + (gpsEnrollValue.frame.size.height + 5) )
                
                return detailView.frame.size.height+60
//            }
//            else
//            {
//                return UITableViewAutomaticDimension
//            }
        }
        else
        {
            return 425.0
        }
//        if (UIDevice().userInterfaceIdiom == .Phone && (UIScreen.mainScreen().nativeBounds.height == 960 || UIScreen.mainScreen().nativeBounds.height == 1136))
//        {
//            return 445.0
//        }
//        else
//        {
//             return 425.0
//        }
       
    }
//    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
//        
//        let cell = tableView.dequeueReusableCellWithIdentifier("homeScreen", forIndexPath: indexPath) as! homeTableViewCell
//         //print(cell.viewFromDevice.frame.size.width)
//    }
    func GPSSearchButtonClickedFromHome(sender : UIButton){
        let gpsRow = Int(sender.accessibilityIdentifier!)
        let usr_dtGp = self.user_data.objectAtIndex(gpsRow!)
        let lattHome = (usr_dtGp.valueForKey("lattitudeSearch")) as! Double
        let longHome = (usr_dtGp.valueForKey("longitudeSearch")) as! Double
        let googleURL = "http://maps.google.com/maps?&z=20&q=\(lattHome),\(longHome)&ll=\(lattHome),\(longHome)"
        let openLink = NSURL(string : googleURL)
        UIApplication.sharedApplication().openURL(openLink!)
    }
    
    func GPSEnrollButtonClickedFromHome(sender : UIButton){
        let gpsRow = Int(sender.accessibilityIdentifier!)
        let usr_dtGp = self.user_data.objectAtIndex(gpsRow!)
        let lattHome = (usr_dtGp.valueForKey("lattitude")) as! Double
        let longHome = (usr_dtGp.valueForKey("longitude")) as! Double
        let googleURL = "http://maps.google.com/maps?&z=20&q=\(lattHome),\(longHome)&ll=\(lattHome),\(longHome)"
        let openLink = NSURL(string : googleURL)
        UIApplication.sharedApplication().openURL(openLink!)
    }

    func closeButtonClicked(sender : UIButton)
    {
        let alertController = UIAlertController(title: "Delete", message: "Are you sure you want to delete this matched profile?", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
        })
        let deleteAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in
            let closeBtnRow = Int(sender.accessibilityIdentifier!)
            let usr_dt = self.user_data.objectAtIndex(closeBtnRow!)
            let obj_id = (usr_dt.valueForKey("obj_Id")) as! String
            self.dataObj.deleteUserData(obj_id)
            self.user_data1  = self.dataObj.fetchAllRecord()
            self.user_data = self.user_data1.reverse()
            self.homeTableView.reloadData()
        })

        alertController.addAction(cancelAction)
        alertController.addAction(deleteAction)
        self.presentViewController(alertController, animated: true, completion:{})
    }
    
    func emailButtonClicked(sender : UIButton)
    {
         let alertController = UIAlertController(title: "Send Alert", message: "Do you want to send alert?", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
        })
        let sendAction = UIAlertAction(title: "SEND", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in
            self.appdelegate!.showHUD(self.view)
            
            self.dateFormatter.dateFormat = "MMM dd, yyyy"
            self.convertedDate = self.dateFormatter.stringFromDate(self.date)
            
            self.dateFormatter.dateFormat = "HH:mm:ss"
            self.convertedTime = self.dateFormatter.stringFromDate(self.date)
            
            self.emailRow = Int(sender.accessibilityIdentifier!)!
            let usr_dtEm = self.user_data.objectAtIndex(self.emailRow)
            self.F_nameEm = (usr_dtEm.valueForKey("firstName")) as! String
            self.L_nameEm = (usr_dtEm.valueForKey("lastName")) as! String
            self.ageEm = (usr_dtEm.valueForKey("age")) as! String
            self.heightEm = (usr_dtEm.valueForKey("height")) as! String
            if(self.heightEm != "-")
            {
                self.heightEm = "\(self.heightEm)\""
            }
            print(self.heightEm)
            self.weightEm = (usr_dtEm.valueForKey("weight")) as! String
            print(self.weightEm)
            self.eyeColorEm = (usr_dtEm.valueForKey("eye_color")) as! String
            self.hairColorEm = (usr_dtEm.valueForKey("hair_color")) as! String
            self.raceEm = (usr_dtEm.valueForKey("race")) as! String
            let lattEml = ((usr_dtEm.valueForKey("lattitudeSearch")) as! Double)
            let longEml = ((usr_dtEm.valueForKey("longitudeSearch")) as! Double)
            self.google = "http://maps.google.com/maps?&z=20&q=\(lattEml),\(longEml)&ll=\(lattEml),\(longEml)"
            self.new_img = (usr_dtEm.valueForKey("newImage")) as! String
            self.imageEm = self.getImage(self.new_img)
            self.imageDataEm = self.encodeToBase64String(self.imageEm)
            
            let dispatchQueue : dispatch_queue_t = dispatch_queue_create("dispatchQueue", nil)
            dispatch_async(dispatchQueue) {
                self.sendEmailService()
            }
        })
        alertController.addAction(cancelAction)
        alertController.addAction(sendAction)
        self.presentViewController(alertController, animated: true, completion:{})
    }
    func sendEmailService(){
        print(google)
        obj_WebService.sendMail(imageDataEm, date: convertedDate, time: convertedTime, location: google, surname: L_nameEm, firstName: F_nameEm, age: ageEm, height: heightEm, weight: weightEm, ethnicity: raceEm, hairColor: hairColorEm, eyeColor: eyeColorEm){ (block) in
            print(block);
            self.appdelegate?.dismissView(self.view)
            if(block == nil){
                let Cust = CustomClass()
                var alert = UIAlertController()
                alert = Cust.displayMyAlertMessage("Check Your Network Connection")
                self.presentViewController(alert, animated: true, completion: nil)
                self.appdelegate?.dismissView(self.view)
            }
            else
            {
            if(block!.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("SendAlertResponse")?.objectForKey("SendAlertResult")?.objectForKey("Code")?.objectForKey("text") as? String == "sendalert.success"){
                
                let alertController = UIAlertController(title: "Success", message: "SendAlert request successfully processed", preferredStyle: UIAlertControllerStyle.Alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
                    //delete object
                    let usr_dtEm = self.user_data.objectAtIndex(self.emailRow)
                    let obj_id = (usr_dtEm.valueForKey("obj_Id")) as! String
                    self.dataObj.deleteUserData(obj_id)
                    self.user_data1  = self.dataObj.fetchAllRecord()
                    self.user_data = self.user_data1.reverse()
                    self.homeTableView.reloadData()
                })
                alertController.addAction(okAction)
                self.presentViewController(alertController, animated: true, completion:{})
            }
            else
            {
                let Cust = CustomClass()
                var alert = UIAlertController( )
                alert = Cust.displayMyAlertMessage("SendAlert request failed")
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
      }
    }
    func detailsButtonClicked(sender : UIButton)
    {
        detailsBtnRow = Int(sender.accessibilityIdentifier!)!
        //print(detailsBtnRow)
        self.performSegueWithIdentifier("show_details", sender: nil)
    }
    func encodeToBase64String(image: UIImage) -> String
    {
        let imageData = UIImagePNGRepresentation(image)
        let imag_str:String = imageData!.base64EncodedStringWithOptions([])
        ////print(imag_str)
        return imag_str
    }

    
    func getImage(imagePath:String) -> UIImage{
        let fileManager = NSFileManager.defaultManager()
        let imagePAth = imagePath
        var image = UIImage()
        if fileManager.fileExistsAtPath(imagePAth){
            image = UIImage(contentsOfFile: imagePAth)!
        }
         return image
    }
//    func calculateAge (birthday: NSDate) -> NSInteger {
//        
//        return NSCalendar.currentCalendar().components(.Year, fromDate: birthday, toDate: NSDate(), options: []).year
//    }

    
    @IBAction func searchButtonTapped(sender: AnyObject)
    {
       performSegueWithIdentifier("searchScreen", sender: nil)
    }
    @IBAction func EnrollButtonTapped(sender: AnyObject)
    {
    performSegueWithIdentifier("EnrollScreen", sender: nil)

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if(segue.identifier == "show_details")
        {
            let details : showDetailsViewController =  (segue.destinationViewController as! showDetailsViewController)
            
            details.detailsArr = user_data
            details.rowOfDetail = detailsBtnRow
        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
