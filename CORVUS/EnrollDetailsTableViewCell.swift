//
//  EnrollDetailsTableViewCell.swift
//  CORVUS
//
//  Created by Riken Shah on 30/08/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit

class EnrollDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var sexButton: UIButton!
    @IBOutlet weak var dobButton: UIButton!
    @IBOutlet weak var heightButton: UIButton!
    @IBOutlet weak var weightButton: UIButton!
    @IBOutlet weak var eyeButton: UIButton!
    @IBOutlet weak var hairButton: UIButton!
    @IBOutlet weak var countryOfBirthButton: UIButton!
    @IBOutlet weak var citizenshipButton: UIButton!
    @IBOutlet weak var birthdateTxt: UITextField!
    @IBOutlet weak var raceButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
