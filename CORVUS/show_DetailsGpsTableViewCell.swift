//
//  show_DetailsGpsTableViewCell.swift
//  CORVUS
//
//  Created by Riken Shah on 23/09/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit

class show_DetailsGpsTableViewCell: UITableViewCell {

    @IBOutlet weak var GpsView: UIView!
    @IBOutlet weak var gpsLabel: UILabel!
    @IBOutlet weak var gpsBtn: UIButton!
        @IBOutlet weak var gpsBtnView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
