//
//  show_DetailsTableViewCell.swift
//  CORVUS
//
//  Created by Riken Shah on 19/09/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit

class show_DetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var SView: UIView!
    @IBOutlet weak var FView: UIView!
    @IBOutlet weak var FLabel: UILabel!
    @IBOutlet weak var SLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
