//
//  homeTableViewCell.swift
//  CORVUS
//
//  Created by Riken Shah on 29/08/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit

class homeTableViewCell: UITableViewCell {

    @IBOutlet weak var gpsSearched: UIButton!
    @IBOutlet weak var dateSearched: UILabel!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var viewFromServer: UIView!
    @IBOutlet weak var viewFromDevice: UIView!
    @IBOutlet weak var NameOfPerson: UILabel!
    @IBOutlet weak var ImageFromDevice: UIImageView!
    @IBOutlet weak var ImageFromServer: UIImageView!
    @IBOutlet weak var AgeAndGender: UILabel!
    @IBOutlet weak var heightOfPerson: UILabel!
    @IBOutlet weak var weightOfPerson: UILabel!
    @IBOutlet weak var birthOfPerson: UILabel!
    @IBOutlet weak var citizenshipOfPerson: UILabel!
    @IBOutlet weak var scoreOfPerson: UILabel!
    @IBOutlet weak var enrollDtOfPerson: UILabel!
    @IBOutlet weak var gpsEnroll: UILabel!
    @IBOutlet weak var gpsEnrollButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        viewFromDevice.layer.cornerRadius = viewFromDevice.frame.size.width/2
//        viewFromDevice.clipsToBounds = true
//        viewFromServer.layer.cornerRadius = viewFromServer.frame.size.width/2
//        viewFromServer.clipsToBounds = true
//        ImageFromDevice.layer.cornerRadius = ImageFromDevice.frame.size.width/2
//        ImageFromDevice.clipsToBounds = true
//        ImageFromServer.layer.cornerRadius = ImageFromServer.frame.size.width/2
//        ImageFromServer.clipsToBounds = true

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    override func layoutSubviews() {
    }
}
