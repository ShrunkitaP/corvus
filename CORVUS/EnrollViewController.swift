//
//  EnrollViewController.swift
//  CORVUS
//
//  Created by Riken Shah on 30/08/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit

class EnrollViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var picker:UIImagePickerController?=UIImagePickerController()
    var imageOnNextScreenEnroll = UIImage()
    var imag_str = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        picker?.delegate=self

        // Do any additional setup after loading the view.
    }
   override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
     self.navigationController?.navigationBarHidden = true
    let value = UIInterfaceOrientation.Portrait.rawValue
    UIDevice.currentDevice().setValue(value, forKey: "orientation")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openCamera(sender: AnyObject) {
        openCamera()
    }

    @IBAction func openGallary(sender: AnyObject) {
        openGallary()
    }
    func openGallary()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        presentViewController(picker!, animated: true, completion: nil)
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerControllerSourceType.Camera
            picker!.cameraCaptureMode = .Photo
            presentViewController(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .Alert)
            let ok = UIAlertAction(title: "OK", style:.Default, handler: nil)
            alert.addAction(ok)
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        var chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        //imageView.contentMode = .ScaleAspectFit
        //imageOnNextScreenEnroll = chosenImage
        chosenImage = fixOrientation(chosenImage)
        var dataComp = UIImageJPEGRepresentation(chosenImage, 1)
        let imageSize: Int = dataComp!.length
        let img = Double(imageSize)/1024.0
        //print(img)
        imag_str = dataComp!.base64EncodedStringWithOptions([])
        imageOnNextScreenEnroll = UIImage(data: dataComp!)!
        if(img > 500){
//            let dataComp1 = UIImageJPEGRepresentation(imageOnNextScreenEnroll, 0.3)
//            let imageSize1: Int = dataComp1!.length
//            
//            let img1 = Double(imageSize1)
            //print(img1 / 1024.0)
            dataComp = UIImageJPEGRepresentation(chosenImage, 0.5)
            imag_str = dataComp!.base64EncodedStringWithOptions([])
            imageOnNextScreenEnroll = UIImage(data: dataComp!)!
        }
        dismissViewControllerAnimated(true, completion: nil)
        performSegueWithIdentifier("dispimageEnroll", sender: nil)
    }
    
    func fixOrientation(img:UIImage) -> UIImage {
        
        if (img.imageOrientation == UIImageOrientation.Up) {
            return img;
        }
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale);
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.drawInRect(rect)
        
        let normalizedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return normalizedImage;
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "dispimageEnroll")
        {
            let enrollPhoto : EnrollPhotoViewController =  (segue.destinationViewController as! EnrollPhotoViewController)
            enrollPhoto.imageToBeEnroll = imageOnNextScreenEnroll
            enrollPhoto.img_data = imag_str
        }
    }
    
    @IBAction func homeButtonLongPress(sender: UILongPressGestureRecognizer) {
        if (sender.state == .Began){
            let destination = self.storyboard!.instantiateViewControllerWithIdentifier("openFileViewController") as! openFileViewController
            self.navigationController!.pushViewController(destination, animated: true)
        }
    }

    @IBAction func homeButtonTapped(sender: AnyObject) {
        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("homeScreenViewController") as! homeScreenViewController
        self.navigationController!.pushViewController(destination, animated: true)
    }

    @IBAction func searchbuttonTapped(sender: AnyObject) {
        
        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("searchViewController") as! searchViewController
        self.navigationController!.pushViewController(destination, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
