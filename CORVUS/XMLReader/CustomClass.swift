//
//  CustomClass.swift
//  CORVUS
//
//  Created by Riken Shah on 08/09/16.
//  Copyright © 2016 HPL. All rights reserved.
//

class CustomClass: NSObject {
    
    func displayMyAlertMessage(userMessage:String) -> UIAlertController {
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.Alert);
        let okAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.Default, handler: nil);
        myAlert.addAction(okAction);
        
        return myAlert
        
        
    }

}
