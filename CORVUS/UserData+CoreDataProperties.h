//
//  UserData+CoreDataProperties.h
//  CORVUS
//
//  Created by Riken Shah on 13/09/16.
//  Copyright © 2016 HPL. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "UserData.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserData (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *age;
@property (nullable, nonatomic, retain) NSString *birth_palce;
@property (nullable, nonatomic, retain) NSString *citizenship;
@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *height;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSString *newImage;
@property (nullable, nonatomic, retain) NSString *oldImage;
@property (nullable, nonatomic, retain) NSString *score;
@property (nullable, nonatomic, retain) NSString *weight;
@property (nullable, nonatomic, retain) NSString *obj_Id;

@end

NS_ASSUME_NONNULL_END
