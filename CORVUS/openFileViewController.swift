//
//  openFileViewController.swift
//  CORVUS
//
//  Created by Riken Shah on 04/10/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit

class openFileViewController: UIViewController,UITextViewDelegate {
    var appdelegate: AppDelegate?
    var text  : String = String()
    @IBOutlet weak var configText: UITextView!
    var xml_dict : NSDictionary = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
         self.navigationController?.navigationBarHidden = false
        self.configText.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
//
//        let DocumentDirURL = try! NSFileManager.defaultManager().URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
//        let fileURL = DocumentDirURL.URLByAppendingPathComponent(file)
//        //print("FilePath: \(fileURL.path)")
//        var readString = "" // Used to store the file contents
//        do {
//            // Read the file contents
//            readString = try String(contentsOfURL: fileURL)
//        } catch let error as NSError {
//            //print("Failed reading from URL: \(fileURL), Error: " + error.localizedDescription)
//            
//        }
//        //print("File Text: \(readString)")
//        
//       }
        
        
        text = (appdelegate?.readFromContainer())!
        //print(text)
        configText.text = text
    }

    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }   /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func saveConfigFile(sender: AnyObject) {
        let textToSave = configText.text
        let DocumentDirURL = try! NSFileManager.defaultManager().URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
        
//        let fileURL = DocumentDirURL.URLByAppendingPathComponent("SurveillanceConsole.config.xml")
//        do {
//            // Write to the file
//            
//            try textToSave.writeToURL(fileURL, atomically: true, encoding: NSUTF8StringEncoding)
//        } catch let error as NSError {
//            //print("Failed writing to URL: \(fileURL), Error: " + error.localizedDescription)
//        }
        
        //let xml_str = (appdelegate?.readFromContainer())!
        let xml_data = textToSave.dataUsingEncoding(NSUTF8StringEncoding)
        do
        {
            xml_dict =  try XMLReader.dictionaryForXMLData(xml_data)
                    let fileURL = DocumentDirURL.URLByAppendingPathComponent("SurveillanceConsole.config.xml")
                    do {
                        // Write to the file
            
                        try textToSave.writeToURL(fileURL, atomically: true, encoding: NSUTF8StringEncoding)
                    } catch let error as NSError {
                        //print("Failed writing to URL: \(fileURL), Error: " + error.localizedDescription)
                    }

            self.navigationController?.popViewControllerAnimated(true)
        }
        catch let error as NSError
        {
            //print(error)
            let alertController = UIAlertController(title: "Fail", message: "Invalid XML Data", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
                
            })
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion:{})

        }
        
    }

}