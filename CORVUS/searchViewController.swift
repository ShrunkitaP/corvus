//
//  searchViewController.swift
//  CORVUS
//
//  Created by Riken Shah on 29/08/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit

class searchViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    var imageOnNextScreen = UIImage()
    var imag_str = String()
    var picker:UIImagePickerController?=UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.navigationBarHidden = false
        picker?.delegate=self

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
        let value = UIInterfaceOrientation.Portrait.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func EnrollButtonTapped(sender: AnyObject) {
        performSegueWithIdentifier("EnrollScreen", sender: nil)
    }
    @IBAction func cameraOpen(sender: AnyObject) {
        openCamera()
        //performSegueWithIdentifier("dispImageOnScreen", sender: nil)
    }
    @IBAction func galleryOpen(sender: AnyObject) {
        openGallary()
    }
    
    @IBAction func longPressHome(sender: UILongPressGestureRecognizer) {
        if (sender.state == .Began){
            let destination = self.storyboard!.instantiateViewControllerWithIdentifier("openFileViewController") as! openFileViewController
            self.navigationController!.pushViewController(destination, animated: true)
        }

    }
    
    @IBAction func HomeButtonTapped(sender: AnyObject) {
//        var nav : UINavigationController!
//        let destination = self.storyboard?.instantiateViewControllerWithIdentifier("homeScreenViewController") as! homeScreenViewController
//        
//        nav = UINavigationController(rootViewController: destination)
        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("homeScreenViewController") as! homeScreenViewController
        self.navigationController!.pushViewController(destination, animated: true)
    }
    func openGallary()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        presentViewController(picker!, animated: true, completion: nil)
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerControllerSourceType.Camera
            picker!.cameraCaptureMode = .Photo
            presentViewController(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .Alert)
            let ok = UIAlertAction(title: "OK", style:.Default, handler: nil)
            alert.addAction(ok)
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
   func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        var chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        //imageView.contentMode = .ScaleAspectFit
      //  imageOnNextScreen = chosenImage
        chosenImage = fixOrientation(chosenImage)
        var dataComp = UIImageJPEGRepresentation(chosenImage, 1)
        let imageSize: Int = dataComp!.length
        let img = Double(imageSize)/1024.0
        print(img)
       //print(imageSize)
    imag_str = dataComp!.base64EncodedStringWithOptions([])
    //print(imag_str)
    
        imageOnNextScreen = UIImage(data: dataComp!)!
        if(img > 500){
//            let dataComp1 = UIImageJPEGRepresentation(imageOnNextScreen, 0.3)
//            let imageSize1: Int = dataComp1!.length
//            let img1 = Double(imageSize1)
//            print(img1.description)
//            //print(img1 / 1024.0)
            dataComp = UIImageJPEGRepresentation(chosenImage, 0.5)
            imag_str = dataComp!.base64EncodedStringWithOptions([])
            imageOnNextScreen = UIImage(data: dataComp!)!
        }
        dismissViewControllerAnimated(true, completion: nil)
        performSegueWithIdentifier("dispImageOnScreen", sender: nil)
    }
    
    func fixOrientation(img:UIImage) -> UIImage {
        
        if (img.imageOrientation == UIImageOrientation.Up) {
            return img;
        }
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale);
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.drawInRect(rect)
        
        let normalizedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return normalizedImage;
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "dispImageOnScreen")
        {
            let searchPhoto : searchPhotoViewController =  (segue.destinationViewController as! searchPhotoViewController)
            searchPhoto.imageToBeSearched = imageOnNextScreen
            searchPhoto.img_data = imag_str
        }
        
    }

//    @IBAction func tp(sender: AnyObject) {
//        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("openFileViewController") as! openFileViewController
//        self.navigationController!.pushViewController(destination, animated: true)
//    }
}

