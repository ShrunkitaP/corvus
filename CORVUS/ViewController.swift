//
//  ViewController.swift
//  CORVUS
//
//  Created by HPL on 29/08/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit
import LocalAuthentication
import FLAnimatedImage


class ViewController: UIViewController {

    @IBOutlet weak var gif_img: FLAnimatedImageView!
   // @IBOutlet weak var gif_img: UIImageView!
    @IBOutlet weak var scanFingerView: UIView!
    //var appdelegate: AppDelegate?
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        scanFingerView.hidden = true
        let image = FLAnimatedImage.init(animatedGIFData: NSData(contentsOfURL:NSBundle.mainBundle().URLForResource("Final-Animation-GIF-1", withExtension: "gif")!))
         self.gif_img.animatedImage = image
        
//        
//        let imageData = NSData(contentsOfURL:NSBundle.mainBundle().URLForResource("Final-Animation-GIF-1", withExtension: "gif")!)
//        
//        let image = UIImage(AImageData:imageData!)
//        
//        gif_img = UIImageView(AImage: image)
//          gif_img.APlay();
        
        
       //let url = NSBundle.mainBundle().URLForResource("Final-Animation-GIF-1", withExtension: "gif")!
        //self.gif_img.image = UIImage.animatedImageWithAnimatedGIFURL(url)
        // Do any additional setup after loading the view, typically from a nib.
         //appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
         //appdelegate?.readFileFromBundle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        NSLog("*********-application comes here**********")
        // Dispose of any resources that can be recreated.
    }

    @IBAction func fingerprintButtonClicked(sender: AnyObject)
    {
        scanFingerView.hidden = false
        authenticateUser()
    }
    
    func authenticateUser() {
        let context : LAContext = LAContext()
        var error : NSError?
        let myLocalizedReasonString : NSString = "Authentication is required"
        if context.canEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, error: &error) {
            context.evaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString as String, reply: { (success : Bool, evaluationError : NSError?) -> Void in
            
            if success
            {
                dispatch_async(dispatch_get_main_queue(),
                    {
                       self.performSegueWithIdentifier("homeScreen", sender: nil)
                })
                }
            else
            {
                // Authentification failed
                //print(evaluationError?.localizedDescription)
                
                switch evaluationError!.code {
                case LAError.SystemCancel.rawValue: break
                    //print("Authentication cancelled by the system")
                case LAError.UserCancel.rawValue:
                    //print("Authentication cancelled by the user")
                    dispatch_async(dispatch_get_main_queue(),
                        {
                    let Cust = CustomClass()
                    var alert = UIAlertController( )
                    alert = Cust.displayMyAlertMessage("Authentication cancelled by the user")
                    self.presentViewController(alert, animated: true, completion: nil)
                              })
                case LAError.UserFallback.rawValue:
                    //print("User wants to use a password")
                    // We show the alert view in the main thread (always update the UI in the main thread)
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        //self.showPasswordAlert()
                    })
                default:
                    //print("Authentication failed")
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        //self.showPasswordAlert()
                    })
                }
                }
            })
        }
        else
        {
            switch error!.code {
            case LAError.TouchIDNotEnrolled.rawValue:
                //print("TouchID not enrolled")
                let Cust = CustomClass()
                var alert = UIAlertController( )
                alert = Cust.displayMyAlertMessage("TouchID not enrolled")
                self.presentViewController(alert, animated: true, completion: nil)
            case LAError.PasscodeNotSet.rawValue:
                //print("Passcode not set")
                let Cust = CustomClass()
                var alert = UIAlertController( )
                alert = Cust.displayMyAlertMessage("TouchID not enrolled")
                self.presentViewController(alert, animated: true, completion: nil)
            default:
                //print("TouchID not available")
                self.performSegueWithIdentifier("homeScreen", sender: nil)
            }
        }
    }

    @IBAction func seeResult(sender: AnyObject)
    {
         performSegueWithIdentifier("homeScreen", sender: nil)
    }
}

