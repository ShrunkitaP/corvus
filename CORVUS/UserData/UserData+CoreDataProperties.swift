//
//  UserData+CoreDataProperties.swift
//  CORVUS
//
//  Created by Riken Shah on 29/12/16.
//  Copyright © 2016 HPL. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension UserData {

    @NSManaged var age: String?
    @NSManaged var birth_palce: String?
    @NSManaged var citizenship: String?
    @NSManaged var dob: String?
    @NSManaged var enroll_date: String?
    @NSManaged var eye_color: String?
    @NSManaged var firstName: String?
    @NSManaged var google_map: String?
    @NSManaged var hair_color: String?
    @NSManaged var height: String?
    @NSManaged var lastName: String?
    @NSManaged var lattitude: NSNumber?
    @NSManaged var lattitudeSearch: NSNumber?
    @NSManaged var longitude: NSNumber?
    @NSManaged var longitudeSearch: NSNumber?
    @NSManaged var newImage: String?
    @NSManaged var obj_Id: String?
    @NSManaged var oldImage: String?
    @NSManaged var race: String?
    @NSManaged var score: String?
    @NSManaged var search_date: String?
    @NSManaged var sex: String?
    @NSManaged var signature: String?
    @NSManaged var weight: String?

}
