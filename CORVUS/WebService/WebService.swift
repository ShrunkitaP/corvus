//
//  WebService.swift
//  CORVUS
//
//  Created by HPL on 31/08/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit
import Alamofire
import SWXMLHash

typealias complition = (block:NSDictionary?)->Void
class WebService: NSObject
{
    var xmlParser: NSXMLParser!
    static let sharedInstance = WebService()
    
    func getResponse(methodType:String,imageData:String,block:complition)
    {
        print(imageData)
        var url : String = String()
        var appdelegate: AppDelegate?
        appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var xml_dict : NSDictionary = NSDictionary()
        let xml_strFrmFile = (appdelegate?.readFromContainer())!
        //let xml_data = NSData(contentsOfFile: xml_str)
        let xml_data = xml_strFrmFile.dataUsingEncoding(NSUTF8StringEncoding)
        do
        {
            xml_dict =  try XMLReader.dictionaryForXMLData(xml_data)
            url = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("IdentityServer")?.objectForKey("text"))! as! String
            url = "http://\(url):8088/RavenBiometricServer/WebService"
            print(url)
        }
        catch let error as NSError
        {
            //print(error)
        }
        
        let xml_str = NSString(format:"<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:rav=\"http://schemas.corvusid.com/RavenBiometricServer\"><soap:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"><wsa:To>http://52.32.101.199:8088/RavenBiometricServer/WebService</wsa:To><wsa:Action>http://schemas.corvusid.com/RavenBiometricServer/Raven/ProcessSearch</wsa:Action></soap:Header><soap:Body><rav:ProcessSearch><!--Optional:--><rav:SearchRequest><rav:BiomImages><!--Zero or more repetitions:--><rav:BiomImage><rav:Data>%@</rav:Data><rav:Type>FrontalFace</rav:Type></rav:BiomImage></rav:BiomImages><!--Optional:--><rav:MaxNumberOfMatches>20</rav:MaxNumberOfMatches></rav:SearchRequest></rav:ProcessSearch></soap:Body></soap:Envelope>",imageData)
        Alamofire.request(.POST, url, parameters: Dictionary(), encoding: .Custom({
            (convertible, params) in
            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
            mutableRequest.URL = NSURL(string: url)
            mutableRequest.HTTPBody = xml_str.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
            return (mutableRequest, nil)}),
            headers:
            ["Content-Type" : "application/soap+xml"]).response{ (request, response, data, error) in
                let xml = SWXMLHash.parse(data!)
                 do
                 {
                    let xml_dict :NSDictionary =  try XMLReader.dictionaryForXMLData(data)
                    block(block: xml_dict)
                }
                 catch let error as NSError
                 {
                    //print(error)
                    block(block: nil)
                }
            }
    }
    
    func sendResponse(imageData:String,firstName:String,lastName:String,dob:String,pob:String,citizenship:String,gender:String,race:String,height:String,weight:String,eye:String,hair:String,LatitudeDegree:String,LatitudeMinute:String,LatitudeSecond:String,LongitudeDegree:String,LongitudeMinute:String,LongitudeSecond:String,block:complition)
    {
        var url : String = String()
        var appdelegate: AppDelegate?
        appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var xml_dict : NSDictionary = NSDictionary()
        let xml_strFrmFile = (appdelegate?.readFromContainer())!
        //let xml_data = NSData(contentsOfFile: xml_str)
        let xml_data = xml_strFrmFile.dataUsingEncoding(NSUTF8StringEncoding)
        do
        {
            xml_dict =  try XMLReader.dictionaryForXMLData(xml_data)
            url = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("IdentityServer")?.objectForKey("text"))! as! String
            url = "http://\(url):8088/RavenBiometricServer/WebService"
        }
        catch let error as NSError
        {
            //print(error)
        }
        let xml_str = NSString(format:" <soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:rav=\"http://schemas.corvusid.com/RavenBiometricServer\"><soap:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"><wsa:To>http://52.32.101.199:8088/RavenBiometricServer/WebService</wsa:To><wsa:Action>http://schemas.corvusid.com/RavenBiometricServer/Raven/ProcessEnroll</wsa:Action></soap:Header><soap:Body><rav:ProcessEnroll><!--Optional:--><rav:EnrollRequest><rav:BiomImages><!--Zero or more repetitions:--><rav:BiomImage><rav:Data>%@</rav:Data><rav:Type>FrontalFace</rav:Type></rav:BiomImage></rav:BiomImages><rav:BiographicInformation><rav:FirstName>%@</rav:FirstName><rav:LastName>%@</rav:LastName><!--Optional:--><rav:DOB>%@</rav:DOB><!--Optional:--><rav:PlaceOfBirth>%@</rav:PlaceOfBirth><!--Optional:--><rav:Citizenship>%@</rav:Citizenship><!--Optional:--><rav:Gender>%@</rav:Gender><!--Optional:--><rav:Race>%@</rav:Race><!--Optional:--><rav:Height>%@</rav:Height><!--Optional:--><rav:Weight>%@</rav:Weight><!--Optional:--><rav:EyeColor>%@</rav:EyeColor><!--Optional:--><rav:HairColor>%@</rav:HairColor><!--Optional:--><rav:LatitudeDegree>%@</rav:LatitudeDegree><!--Optional:--><rav:LatitudeMinute>%@</rav:LatitudeMinute><!--Optional:--><rav:LatitudeSecond>%@</rav:LatitudeSecond><!--Optional:--><rav:LongitudeDegree>%@</rav:LongitudeDegree><!--Optional:--><rav:LongitudeMinute>%@</rav:LongitudeMinute><!--Optional:--><rav:LongitudeSecond>%@</rav:LongitudeSecond></rav:BiographicInformation></rav:EnrollRequest></rav:ProcessEnroll></soap:Body></soap:Envelope>",imageData,firstName,lastName,dob,pob,citizenship,gender,race,height,weight,eye,hair,LatitudeDegree,LatitudeMinute,LatitudeSecond,LongitudeDegree,LongitudeMinute,LongitudeSecond)
        
        Alamofire.request(.POST, url, parameters: Dictionary(), encoding: .Custom({
            (convertible, params) in
            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
            mutableRequest.URL = NSURL(string: url)
            mutableRequest.HTTPBody = xml_str.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
            return (mutableRequest, nil)}),
            headers:
            ["Content-Type" : "application/soap+xml"]).response{ (request, response, data, error) in
                let xml = SWXMLHash.parse(data!)
                do
                {
                    let xml_dict :NSDictionary =  try XMLReader.dictionaryForXMLData(data)
                    block(block: xml_dict)
                }
                catch let error as NSError
                {
                    //print(error)
                    block(block: nil)
                }
        }
    }
    
    func sendResponseWithSign(imageData:String,sign_str:String,firstName:String,lastName:String,dob:String,pob:String,citizenship:String,gender:String,race:String,height:String,weight:String,eye:String,hair:String,LatitudeDegree:String,LatitudeMinute:String,LatitudeSecond:String,LongitudeDegree:String,LongitudeMinute:String,LongitudeSecond:String,block:complition)
    {
        var url : String = String()
        var appdelegate: AppDelegate?
        appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var xml_dict : NSDictionary = NSDictionary()
        let xml_strFrmFile = (appdelegate?.readFromContainer())!
        //let xml_data = NSData(contentsOfFile: xml_str)
        let xml_data = xml_strFrmFile.dataUsingEncoding(NSUTF8StringEncoding)
        do
        {
            xml_dict =  try XMLReader.dictionaryForXMLData(xml_data)
            url = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("IdentityServer")?.objectForKey("text"))! as! String
            url = "http://\(url):8088/RavenBiometricServer/WebService"
        }
        catch let error as NSError
        {
            //print(error)
        }
        let xml_str = NSString(format:" <soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:rav=\"http://schemas.corvusid.com/RavenBiometricServer\"><soap:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"><wsa:To>http://52.32.101.199:8088/RavenBiometricServer/WebService</wsa:To><wsa:Action>http://schemas.corvusid.com/RavenBiometricServer/Raven/ProcessEnroll</wsa:Action></soap:Header><soap:Body><rav:ProcessEnroll><!--Optional:--><rav:EnrollRequest><rav:BiomImages><!--Zero or more repetitions:--><rav:BiomImage><rav:Data>%@</rav:Data><rav:Type>FrontalFace</rav:Type></rav:BiomImage><rav:BiomImage><rav:Data>%@</rav:Data><rav:Type>Sign</rav:Type></rav:BiomImage></rav:BiomImages><rav:BiographicInformation><rav:FirstName>%@</rav:FirstName><rav:LastName>%@</rav:LastName><!--Optional:--><rav:DOB>%@</rav:DOB><!--Optional:--><rav:PlaceOfBirth>%@</rav:PlaceOfBirth><!--Optional:--><rav:Citizenship>%@</rav:Citizenship><!--Optional:--><rav:Gender>%@</rav:Gender><!--Optional:--><rav:Race>%@</rav:Race><!--Optional:--><rav:Height>%@</rav:Height><!--Optional:--><rav:Weight>%@</rav:Weight><!--Optional:--><rav:EyeColor>%@</rav:EyeColor><!--Optional:--><rav:HairColor>%@</rav:HairColor><!--Optional:--><rav:LatitudeDegree>%@</rav:LatitudeDegree><!--Optional:--><rav:LatitudeMinute>%@</rav:LatitudeMinute><!--Optional:--><rav:LatitudeSecond>%@</rav:LatitudeSecond><!--Optional:--><rav:LongitudeDegree>%@</rav:LongitudeDegree><!--Optional:--><rav:LongitudeMinute>%@</rav:LongitudeMinute><!--Optional:--><rav:LongitudeSecond>%@</rav:LongitudeSecond></rav:BiographicInformation></rav:EnrollRequest></rav:ProcessEnroll></soap:Body></soap:Envelope>",imageData,sign_str,firstName,lastName,dob,pob,citizenship,gender,race,height,weight,eye,hair,LatitudeDegree,LatitudeMinute,LatitudeSecond,LongitudeDegree,LongitudeMinute,LongitudeSecond)
        
        Alamofire.request(.POST, url, parameters: Dictionary(), encoding: .Custom({
            (convertible, params) in
            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
            mutableRequest.URL = NSURL(string: url)
            mutableRequest.HTTPBody = xml_str.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
            return (mutableRequest, nil)}),
            headers:
            ["Content-Type" : "application/soap+xml"]).response{ (request, response, data, error) in
                let xml = SWXMLHash.parse(data!)
                do
                {
                    let xml_dict :NSDictionary =  try XMLReader.dictionaryForXMLData(data)
                    block(block: xml_dict)
                    //print(xml_dict.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult"))
                }
                catch let error as NSError
                {
                    //print(error)
                    block(block: nil)
                }
        }
    }
    
    func sendMail(imageData:String,date:String,time:String,location:String,surname:String,firstName:String,age:String,height:String,weight:String,ethnicity:String,hairColor:String,eyeColor:String,block:complition)
    {
        var EmailFrom : String = String()
        var EmailTo : String = String()
        var url : String = String()
        var appdelegate: AppDelegate?
        appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var xml_dict : NSDictionary = NSDictionary()
        let xml_strFrmFile = (appdelegate?.readFromContainer())!
        //let xml_data = NSData(contentsOfFile: xml_str)
        let xml_data = xml_strFrmFile.dataUsingEncoding(NSUTF8StringEncoding)
        do
        {
            xml_dict =  try XMLReader.dictionaryForXMLData(xml_data)
            //print(xml_dict)
            EmailFrom = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("EmailFrom")?.objectForKey("text"))! as! String
            EmailTo = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("EmailTo")?.objectForKey("text"))! as! String
            print(EmailTo)
            url = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("IdentityServer")?.objectForKey("text"))! as! String
            url = "http://\(url):8088/RavenBiometricServer/WebService"
        }
        catch let error as NSError
        {
            //print(error)
        }
        var newLocation = location
        newLocation = "<a href=\(location)>Click here to see location</a>"

        let xml_str = NSString(format:"<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:rav=\"http://schemas.corvusid.com/RavenBiometricServer\"><soap:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"><wsa:To>http://52.32.101.199:8088/RavenBiometricServer/WebService</wsa:To><wsa:Action>http://schemas.corvusid.com/RavenBiometricServer/Raven/SendAlert</wsa:Action></soap:Header><soap:Body><rav:SendAlert><!--Optional:--><rav:SendAlertRequest><rav:FaceImage>%@</rav:FaceImage><rav:EmailTo>%@</rav:EmailTo><rav:EmailFrom>%@</rav:EmailFrom><rav:EmailSubject>ALERT! Surveillance Face Match.</rav:EmailSubject><rav:EmailBody><![CDATA[<b>Date: </b>%@<br/><b>Time: </b>%@<br/><b>Location Seen:%@</b><br/><br/><p><img src=\"cid:personImage\" /></p><b>Surname:</b><br/>%@<br/><b>Given Name:</b><br/>%@<br/><b>Age:</b><br/>%@<br/><b>Height:</b><br/>%@<br/><b>Weight:</b><br/>%@<br/><b>Ethnicity:</b><br/>%@<br/><b>Hair Color:</b><br/>%@<br/><b>Eye Color:</b><br/>%@<br/>]]></rav:EmailBody></rav:SendAlertRequest></rav:SendAlert></soap:Body></soap:Envelope>",imageData,EmailTo,EmailFrom,date,time,newLocation,surname,firstName,age,height,weight,ethnicity,hairColor,eyeColor)
        
        Alamofire.request(.POST, url, parameters: Dictionary(), encoding: .Custom({
            (convertible, params) in
            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
            mutableRequest.URL = NSURL(string: url)
            mutableRequest.HTTPBody = xml_str.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
            return (mutableRequest, nil)}),
            headers:
            ["Content-Type" : "application/soap+xml"]).response{ (request, response, data, error) in
                let xml = SWXMLHash.parse(data!)
                do
                {
                    let xml_dict :NSDictionary =  try XMLReader.dictionaryForXMLData(data)
                    block(block: xml_dict)
                }
                catch let error as NSError
                {
                    //print(error)
                    block(block: nil)
                }
        }
        
    }
    
}






