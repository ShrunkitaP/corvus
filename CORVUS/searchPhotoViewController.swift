//
//  searchPhotoViewController.swift
//  CORVUS
//
//  Created by Riken Shah on 29/08/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

//typealias CompletionBlock = (result: String?) -> Void

class searchPhotoViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var displayImage: UIImageView!
    @IBOutlet weak var searchImage: UIButton!
    @IBOutlet weak var cancelImage: UIButton!
    var imageToBeSearched = UIImage()
    var img_data = String()
     var appdelegate: AppDelegate?
    let obj_WebService = WebService()
    var noOfMatch = Int()
    var locationManager = CLLocationManager()
    var lattitude = Double()
    var longitude = Double()
    var respDictOld : NSDictionary = NSDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.navigationBarHidden = false
//        displayImage.layer.cornerRadius = 5
//        displayImage.clipsToBounds = true
        displayImage.image = imageToBeSearched
        appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        //locationManager.startMonitoringSignificantLocationChanges()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let userLocation:CLLocation = locations[0]
        longitude = userLocation.coordinate.longitude;
        lattitude = userLocation.coordinate.latitude;
        print(longitude)
        print(lattitude)
        locationManager.stopUpdatingLocation()
        //self.locationManager.stopMonitoringSignificantLocationChanges()
    }
    
    func encodeToBase64String(image: UIImage) -> String
    {
        let imageData = UIImagePNGRepresentation(image)
        let imag_str:String = imageData!.base64EncodedStringWithOptions([])
        print(imag_str)
        return imag_str
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func longPressHome(sender: UILongPressGestureRecognizer) {
        if (sender.state == .Began){
            let destination = self.storyboard!.instantiateViewControllerWithIdentifier("openFileViewController") as! openFileViewController
            self.navigationController!.pushViewController(destination, animated: true)
        }
    }
    
    @IBAction func homeButtonTapped(sender: AnyObject) {
        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("homeScreenViewController") as! homeScreenViewController
        self.navigationController!.pushViewController(destination, animated: true)

    }
    @IBAction func cancelButtonTapped(sender: AnyObject) {
         self.navigationController?.popViewControllerAnimated(true)
        //let img = fixOrientation(imageToBeSearched)
        //UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil)
    }
    
    @IBAction func searchByImageButtonTapped(sender: AnyObject) {
        if recheablityCheck() == true
        {
        appdelegate!.showHUD(self.view)
        //let img_data = encodeToBase64String(imageToBeSearched)
            
        obj_WebService.getResponse("POST",imageData:img_data) { (block) in
            //print(block);
            if(block == nil){
                let Cust = CustomClass()
                var alert = UIAlertController()
                alert = Cust.displayMyAlertMessage("Not able to connect to Server")
                self.presentViewController(alert, animated: true, completion: nil)
                self.appdelegate?.dismissView(self.view)
            }
            else
            {
            self.respDictOld = block!
            self.appdelegate?.dismissView(self.view)
            print(self.respDictOld)
            self.noOfMatch = Int(((self.respDictOld.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessSearchResponse")?.objectForKey("ProcessSearchResult")?.objectForKey("NumberOfMatches")?.objectForKey("text")) as? String)!)!
            //print(self.noOfMatch)
            if(self.noOfMatch == 0)
            {
                let alertController = UIAlertController(title: "Not Found", message: "No match found for this person", preferredStyle: UIAlertControllerStyle.Alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
                })
                alertController.addAction(okAction)
                self.presentViewController(alertController, animated: true, completion:{})
            }
            else
            {
                 self.performSegueWithIdentifier("searchAndMatchResult", sender: nil)
            }
        }
      }
    }
        else
        {
            let myAlert = UIAlertController(title: "Internet connection FAILED", message: "Your device is not connected to internet", preferredStyle: UIAlertControllerStyle.Alert);
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil);
            myAlert.addAction(okAction)
            self.presentViewController(myAlert, animated: true, completion: nil)
        }

    }
//
//    func rotateImageAppropriately(imageToRotate: UIImage) -> UIImage {
//        var imageRef = imageToRotate.CGImage
//        var properlyRotatedImage: UIImage!
//        if (imageOrientationWhenAddedToScreen == 0) {
//            //Don't rotate the image
//            properlyRotatedImage = imageToRotate
//        }else if imageOrientationWhenAddedToScreen == 3 {
//            //We need to rotate the image back to a 3
//            properlyRotatedImage = UIImage(CGImage: imageRef!, scale: 1.0, orientation: 3)
//        }else if imageOrientationWhenAddedToScreen == 1 {
//            //We need to rotate the image back to a 1
//            properlyRotatedImage = UIImage(CGImage: imageRef!, scale: 1.0, orientation: 1)
//        }
//        return properlyRotatedImage
//    }
    
    func recheablityCheck()-> Bool
    {
        let net = NetworkReachabilityManager()
        net?.startListening()
        
        if  net?.isReachable ?? false
        {
            
            if ((net?.isReachableOnEthernetOrWiFi) != nil)
            {
                return true
            }
            else if(net?.isReachableOnWWAN)!
            {
                return true
            }
        }
        else
        {
            //// print("no connection")
            return false
        }
        return false
    }
    
    
    @IBAction func EnrollButtonTapped(sender: AnyObject) {
        performSegueWithIdentifier("EnrollScreen", sender: nil)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "searchAndMatchResult")
        {
            let searchAndMatchPhoto : matchResultViewController =  (segue.destinationViewController as! matchResultViewController)
            searchAndMatchPhoto.imageToBeSearchedAndMatch = imageToBeSearched
            searchAndMatchPhoto.respDict = respDictOld
            searchAndMatchPhoto.noOfData = noOfMatch
            searchAndMatchPhoto.lattitudeSearch = lattitude
            searchAndMatchPhoto.longitudeSearch = longitude
        }
    }
}
