//
//  UserData+CoreDataProperties.swift
//  CORVUS
//
//  Created by Riken Shah on 13/09/16.
//  Copyright © 2016 HPL. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension UserData {

    @NSManaged var age: String?
    @NSManaged var birth_palce: String?
    @NSManaged var citizenship: String?
    @NSManaged var firstName: String?
    @NSManaged var height: String?
    @NSManaged var lastName: String?
    @NSManaged var newImage: String?
    @NSManaged var oldImage: String?
    @NSManaged var score: String?
    @NSManaged var weight: String?
    @NSManaged var obj_Id: String?

}
