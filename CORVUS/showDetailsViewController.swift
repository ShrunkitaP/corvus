//
//  showDetailsViewController.swift
//  CORVUS
//
//  Created by Riken Shah on 19/09/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit

class showDetailsViewController: UIViewController,  UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var detailsTableView: UITableView!
    var detailsArr : NSArray = NSArray()
    var rowOfDetail = Int()
    //var google : String = String()
    var firstLabelContains = ["First Name","Last Name","Date of Birth","Country of Birth","Citizenship","Gender","Race","Height","Weight","Hair Color","Eye Color","Score","Enroll Date","Date Seen"]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.detailsTableView.separatorStyle = UITableViewCellSeparatorStyle.None
        //self.navigationController?.navigationBar.hidden = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: table view functions
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return firstLabelContains.count+3
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        //cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        let User_data = detailsArr.objectAtIndex(rowOfDetail)
        if(indexPath.row == 0) {
            let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.FView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)
            cell.SView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)
            cell.FLabel.text = firstLabelContains[indexPath.row]
            cell.SLabel.text = (User_data.valueForKey("firstName")) as? String
            return cell
        }
        else if(indexPath.row == 1){
             let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.FView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.FLabel.text = firstLabelContains[indexPath.row]
            cell.SView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.SLabel.text = (User_data.valueForKey("lastName")) as? String
            return cell
        }
        else if(indexPath.row == 2){
             let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.FLabel.text = firstLabelContains[indexPath.row]
            cell.SLabel.text = (User_data.valueForKey("dob")) as? String
            cell.FView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)
            cell.SView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)

//            let beforeage = (User_data.valueForKey("age")) as? String
//            let dateFormatter = NSDateFormatter()
//            dateFormatter.dateFormat =  "MM/dd/yyyy"
//            let date =  dateFormatter.dateFromString(beforeage!)
//            let age  = String(calculateAge(date!))
//            cell.SLabel.text = age
            return cell
        }
        else if(indexPath.row == 3){
             let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.FView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.FLabel.text = firstLabelContains[indexPath.row]
            cell.SView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.SLabel.text = (User_data.valueForKey("birth_palce")) as? String
            return cell
                    }
        else if(indexPath.row == 4){
             let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.FLabel.text = firstLabelContains[indexPath.row]
             cell.SLabel.text = (User_data.valueForKey("citizenship")) as? String
            cell.FView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)
            cell.SView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)

            return cell
        }
        else if(indexPath.row == 5){
             let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.FView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.FLabel.text = firstLabelContains[indexPath.row]
            cell.SView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.SLabel.text = (User_data.valueForKey("sex")) as? String
            return cell
        }
        else if(indexPath.row == 6){
             let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.FLabel.text = firstLabelContains[indexPath.row]
             cell.SLabel.text = (User_data.valueForKey("race")) as? String
            cell.FView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)
            cell.SView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)

            return cell
        }
        else if(indexPath.row == 7){
             let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.FView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.FLabel.text = firstLabelContains[indexPath.row]
            cell.SView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            if((User_data.valueForKey("height")) as? String == "-")
            {
                cell.SLabel.text = (User_data.valueForKey("height")) as? String
            }
            else
            {
            cell.SLabel.text = ((User_data.valueForKey("height")) as? String)!+"\""
            }
            return cell
            
        }
        else if(indexPath.row == 8){
             let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.FLabel.text = firstLabelContains[indexPath.row]
             cell.SLabel.text = (User_data.valueForKey("weight")) as? String
            cell.FView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)
            cell.SView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)

            return cell
        }
        else if(indexPath.row == 9){
             let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.FView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.FLabel.text = firstLabelContains[indexPath.row]
            cell.SView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.SLabel.text = (User_data.valueForKey("hair_color")) as? String
            return cell
                   }
        else if(indexPath.row == 10){
             let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.FLabel.text = firstLabelContains[indexPath.row]
            cell.SLabel.text = (User_data.valueForKey("eye_color")) as? String
            cell.FView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)
            cell.SView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)

            return cell
        }
         else if(indexPath.row == 11){
             let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.FView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.FLabel.text = firstLabelContains[indexPath.row]
            cell.SView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
             cell.SLabel.text = (User_data.valueForKey("score")) as? String
            return cell
        }
        else if(indexPath.row == 12){
             let cellGps = tableView.dequeueReusableCellWithIdentifier("showDetailsGps", forIndexPath: indexPath) as! show_DetailsGpsTableViewCell
            cellGps.selectionStyle = UITableViewCellSelectionStyle.None
            cellGps.gpsLabel.text = "Enroll GPS Location"
            cellGps.GpsView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)
            cellGps.gpsBtnView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)
            let gbl_val = (User_data.valueForKey("google_map")) as? String
            if(gbl_val == "No GPS available"){
                cellGps.gpsBtn.setTitle("  No GPS available", forState: .Normal)
            }
            else{
                let latt = (User_data.valueForKey("lattitude")) as! Double
                let longi = (User_data.valueForKey("longitude")) as! Double
                cellGps.gpsBtn.setTitleColor(UIColor.blueColor(), forState: .Normal)
                cellGps.gpsBtn.setTitle("\(latt) \(longi)", forState: .Normal)
            cellGps.gpsBtn.addTarget(self, action: Selector("GPSDetailsButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
            }
            return cellGps
        } else if(indexPath.row == 13){
            let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.FView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.FLabel.text = firstLabelContains[indexPath.row - 1]
            cell.SView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.SLabel.text = (User_data.valueForKey("enroll_date")) as? String
            return cell
        }else if(indexPath.row == 14){
            let cell = tableView.dequeueReusableCellWithIdentifier("showDetails", forIndexPath: indexPath) as! show_DetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            //cell.FView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.FLabel.text = firstLabelContains[indexPath.row - 1]
            //cell.SView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cell.SLabel.text = (User_data.valueForKey("search_date")) as? String
            cell.FView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)
            cell.SView.backgroundColor = UIColor(red: 208.0/255.0, green: 216.0/255.0, blue: 232.0/255.0, alpha: 1)

            return cell
        }else if(indexPath.row == 15){
            let cellGps = tableView.dequeueReusableCellWithIdentifier("showDetailsGps", forIndexPath: indexPath) as! show_DetailsGpsTableViewCell
            cellGps.selectionStyle = UITableViewCellSelectionStyle.None
            cellGps.GpsView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
            cellGps.gpsLabel.text = "Search GPS Location"
            //cellGps.gpsBtnView.backgroundColor
            cellGps.gpsBtnView.backgroundColor = UIColor(red: 224.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
             let lattSearch_val = (User_data.valueForKey("lattitudeSearch")) as! Double
             let longSearch_val = (User_data.valueForKey("longitudeSearch")) as! Double
            if(lattSearch_val == 0 && longSearch_val == 0){
                cellGps.gpsBtn.setTitle("  No GPS available", forState: .Normal)
            }
            else{
                cellGps.gpsBtn.setTitleColor(UIColor.blueColor(), forState: .Normal)
                cellGps.gpsBtn.setTitle("\(lattSearch_val) \(longSearch_val)", forState: .Normal)
                cellGps.gpsBtn.addTarget(self, action: Selector("GPSSearchButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
            }
            return cellGps
        }

        else{
            let cellSign = tableView.dequeueReusableCellWithIdentifier("showDetailsSign", forIndexPath: indexPath) as! show_DetailsSignTableViewCell
             cellSign.selectionStyle = UITableViewCellSelectionStyle.None
            let gbl_val = (User_data.valueForKey("signature")) as? String
            if(gbl_val == "No image found")
                {
                    cellSign.signImg.image = UIImage(named:"noImage")
                }
                else
                {
                    cellSign.signImg.image = getImage(gbl_val!)
                }
            return cellSign
        }
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        if(indexPath.row == 16){
            let deviceType = UIDevice.currentDevice().model
            if (deviceType == "iPhone") {
                //print("iphone")
                 return 120.0
            }
            else{
                //print("ipad")
                return 170.0
            }
           
        }
        else if(indexPath.row == 3 || indexPath.row == 5){
            let deviceType = UIDevice.currentDevice().model
            if (deviceType == "iPhone") {
                //print("iphone")
                return 68.0
            }
            else{
                //print("ipad")
                return 38.0
            }
        }
        else if(indexPath.row == 4){
            let deviceType = UIDevice.currentDevice().model
            if (deviceType == "iPhone") {
                //print("iphone")
                return 68.0
            }
            else{
                //print("ipad")
                return 38.0
            }
        }else if(indexPath.row == 12 || indexPath.row == 15){
            let deviceType = UIDevice.currentDevice().model
            if (deviceType == "iPhone") {
                //print("iphone")
                return 68.0
            }
            else{
                //print("ipad")
                return 38.0
            }
        }else if(indexPath.row == 2){
            let deviceType = UIDevice.currentDevice().model
            if (deviceType == "iPhone") {
                //print("iphone")

                if((UIScreen.mainScreen().nativeBounds.height == 960 || UIScreen.mainScreen().nativeBounds.height == 1136))
                {
                    return 68.0
                }
                else
                {
                  return 38.0
                }
                
            }
            else{
                //print("ipad")
                return 38.0
            }
        }
            

        else{
            return 38.0
        }
    }

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         let User_data = detailsArr.objectAtIndex(rowOfDetail)
        let old_img = (User_data.valueForKey("oldImage")) as! String
        let new_img = (User_data.valueForKey("newImage")) as! String
        var headerView : UIView?
        headerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 150))
        headerView?.backgroundColor = UIColor.whiteColor()
        let searchLbl = UILabel()
                searchLbl.text = "Search"
        searchLbl.textAlignment = .Center
        searchLbl.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
        let matchLbl = UILabel()
        matchLbl.text = "Match"
        matchLbl.textAlignment = .Center
        matchLbl.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
        searchLbl.frame = CGRectMake(20, 0, (tableView.frame.size.width/2) - 40 , 20)
        matchLbl.frame = CGRectMake((tableView.frame.size.width/2)+20, 0, (tableView.frame.size.width/2) - 40, 20)
        
        let searchImage = UIImageView()
        searchImage.frame = CGRectMake(20, 30, (tableView.frame.size.width/2) - 40, 130)
        searchImage.layer.cornerRadius = 10
        searchImage.clipsToBounds = true
        searchImage.contentMode = UIViewContentMode.ScaleAspectFit
        searchImage.image = getImage(old_img)
        let matchImage = UIImageView()
        matchImage.frame = CGRectMake((tableView.frame.size.width/2)+20, 30, (tableView.frame.size.width/2) - 40, 130)
        matchImage.layer.cornerRadius = 10
        matchImage.clipsToBounds = true
        matchImage.contentMode = UIViewContentMode.ScaleAspectFit
        matchImage.image = getImage(new_img)
        
        headerView?.addSubview(searchLbl)
        headerView?.addSubview(matchLbl)
        headerView?.addSubview(searchImage)
        headerView?.addSubview(matchImage)
        
        return headerView
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 180.0
    }
    func GPSSearchButtonClicked(sender : UIButton){
        let User_data = detailsArr.objectAtIndex(rowOfDetail)
        let googleLatt = User_data.valueForKey("lattitudeSearch") as! Double
        let googleLong = User_data.valueForKey("longitudeSearch") as! Double
        let googleURL = "http://maps.google.com/maps?&z=20&q=\(googleLatt),\(googleLong)&ll=\(googleLatt),\(googleLong)"
//        //print(googleURL)
        let openLink = NSURL(string : googleURL)
        UIApplication.sharedApplication().openURL(openLink!)
    }
    
// MARK: gps button clicked
    func GPSDetailsButtonClicked(sender : UIButton){
        let User_data = detailsArr.objectAtIndex(rowOfDetail)
        let google = User_data.valueForKey("google_map") as? String
        let openLink = NSURL(string : google!)
        UIApplication.sharedApplication().openURL(openLink!)
        
    }
    // MARK: return image from path
    func getImage(imagePath:String) -> UIImage{
        let fileManager = NSFileManager.defaultManager()
        let imagePAth = imagePath
        var image = UIImage()
        if fileManager.fileExistsAtPath(imagePAth){
            image = UIImage(contentsOfFile: imagePAth)!
        }
        return image
    }
    @IBAction func enrollButton(sender: AnyObject) {
        self.performSegueWithIdentifier("enrollFrmDetls", sender: nil)
    }
    // MARK: home button action
    @IBAction func homeButton(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func longPressHomeGesture(sender: UILongPressGestureRecognizer) {
        if (sender.state == .Began){
            let destination = self.storyboard!.instantiateViewControllerWithIdentifier("openFileViewController") as! openFileViewController
            self.navigationController!.pushViewController(destination, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
