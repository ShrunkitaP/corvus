//
//  EnrollDetailsViewController.swift
//  CORVUS
//
//  Created by Riken Shah on 30/08/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit
import Alamofire

class EnrollDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    //var locationManager = CLLocationManager()
    var picker:UIImagePickerController?=UIImagePickerController()
    var imageSelected = UIImage()
    var popup : KLCPopup?
    var smallTableView : UITableView!
    var pickerView :UIPickerView!
    var dateView : UIDatePicker!
    @IBOutlet weak var myDetailsTableView: UITableView!
    let obj_WebService = WebService()
    var sexArray:NSMutableArray = NSMutableArray()
    var xml_dict : NSDictionary = NSDictionary()
    var nameOfPerson : String = String()
    var sirnameOfPerson : String = String()
    var raceOfPerson : String = "Unknown"
    var genderOfPerson : String = "Unknown"
    var heightOfPerson : String = "Unknown"
    var weightOfPerson : String = "Unknown"
    var dobOfPerson : String = String()
    var eyeOfPerson : String = "Unknown"
    var hairOfPerson : String = "Unknown"
    var countryOfPerson : String = "Unknown"
    var citizenshipOfPerson : String = "Unknown"
    var LongitudeDegreeEn = Int()
    var LatitudeDegreeEn = Int()
    var LongitudeMinuteEn = Int()
    var LatitudeMinuteEn = Int()
    var LongitudeSecondEn = Int()
    var LatitudeSecondEn = Int()
    var img_strEn : String = String()
    var appdelegate: AppDelegate?
    var tagOfButton : Int = Int()
    var height : Bool?
    var alertview = UIView()
    var scrollVal = "Unknown"
    var toScroll = false
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.myDetailsTableView.separatorStyle = UITableViewCellSeparatorStyle.None
        appdelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        //let xml_str = NSBundle.mainBundle().pathForResource("SurveillanceConsole.config", ofType: "xml")!
        let xml_str = (appdelegate?.readFromContainer())!
        //let xml_data = NSData(contentsOfFile: xml_str)
        let xml_data = xml_str.dataUsingEncoding(NSUTF8StringEncoding)
        do
        {
            xml_dict =  try XMLReader.dictionaryForXMLData(xml_data)
            //print(xml_dict)
        }
        catch let error as NSError
        {
            //print(error)
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        picker?.delegate=self
        //scrollVal = 0
    }
        // Do any additional setup after loading the view.
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        let value = UIInterfaceOrientation.Portrait.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        self.navigationController?.navigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func shouldAutorotate() -> Bool {
        return false
    }

    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == myDetailsTableView){
        return 1
        }
        else
        {
            return self.sexArray.count
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if(tableView == myDetailsTableView){
        let cell = tableView.dequeueReusableCellWithIdentifier("detailsCell", forIndexPath: indexPath) as! EnrollDetailsTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.firstName.textAlignment = .Left
        cell.lastName.textAlignment = .Left
        cell.weightButton.setTitle("  \(weightOfPerson)", forState: .Normal)
        cell.weightButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
        cell.sexButton.setTitle("  \(genderOfPerson)", forState: .Normal)
        cell.sexButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
        cell.heightButton.setTitle("  \(heightOfPerson)", forState: .Normal)
        cell.heightButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
        cell.raceButton.setTitle("  \(raceOfPerson)", forState: .Normal)
        cell.raceButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
        cell.eyeButton.setTitle("  \(eyeOfPerson)", forState: .Normal)
        cell.eyeButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
        cell.hairButton.setTitle("  \(hairOfPerson)", forState: .Normal)
        cell.hairButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
        cell.countryOfBirthButton.setTitle(countryOfPerson, forState: .Normal)
        cell.countryOfBirthButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
        cell.citizenshipButton.setTitle(citizenshipOfPerson, forState: .Normal)
        cell.citizenshipButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
        cell.raceButton.addTarget(self, action: Selector("raceButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
        cell.sexButton.addTarget(self, action: Selector("sexButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
        cell.heightButton.addTarget(self, action: Selector("heightButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
        cell.weightButton.addTarget(self, action: Selector("weightButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
        cell.dobButton.addTarget(self, action: Selector("dobButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
        cell.eyeButton.addTarget(self, action: Selector("eyeButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
        cell.hairButton.addTarget(self, action: Selector("hairButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
        cell.countryOfBirthButton.addTarget(self, action: Selector("countryButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
        cell.citizenshipButton.addTarget(self, action: Selector("citizenshipButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
        return cell
    }
    else
    {
        smallTableView.registerNib(UINib(nibName: "pickerTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadRow")
        //alertview.frame.size.height = smallTableView.frame.size.height
        let cell: pickerTableViewCell! = tableView.dequeueReusableCellWithIdentifier("LoadRow",forIndexPath: indexPath) as? pickerTableViewCell
        var str : String = String()
        smallTableView.separatorStyle = UITableViewCellSeparatorStyle.None
        if(height == true)
        {
            if(indexPath.row == 0)
            {
                if(scrollVal != "Unknown" && toScroll == true)
                {
                    //print(scrollVal)
                    let scrollInd = sexArray.indexOfObject(scrollVal)
                    let indexPath1 = NSIndexPath(forRow: scrollInd, inSection: 0)
                    smallTableView.scrollToRowAtIndexPath(indexPath1, atScrollPosition: .Top, animated: false)
                    toScroll = false
                    //cell.backgroundColor = UIColor.darkGrayColor()
                }
            }
            str = sexArray[indexPath.row] as! String
            cell.TitleLabel.text = str
        }
        else
        {
            if(indexPath.row == 0)
                {
                    if(scrollVal != "Unknown" && toScroll == true)
                    {
                        //print(scrollVal)
                        let scrollInd = sexArray.indexOfObject(scrollVal)
                        let indexPath1 = NSIndexPath(forRow: scrollInd, inSection: 0)
                        smallTableView.scrollToRowAtIndexPath(indexPath1, atScrollPosition: .Top, animated: false)
                        toScroll = false
                    }
                }
            str = sexArray.objectAtIndex(indexPath.row) as! String
            cell.TitleLabel.text = str
        }
//        if(scrollVal == cell.TitleLabel.text)
//        {
//            cell.TitleLabel.font = UIFont.boldSystemFontOfSize(17)
//        }
        return cell
    }
}
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        if(tableView == myDetailsTableView)
        {
            let deviceType = UIDevice.currentDevice().model
            if (deviceType == "iPhone")
            {
                //print("iphone")
                return 483.0
            }
            else
            {
                //print("ipad")
                return 395.0
            }
        }
        else
        {
           return 40.0
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(tableView == myDetailsTableView)
        {
        var headerView : UIView?
        headerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 2))
        headerView?.backgroundColor = UIColor.whiteColor()
        return headerView
        }
        else
        {
            return nil
        }
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(tableView == myDetailsTableView)
        {
        return 2.0
        }
        else{
            return 0.0
        }
    }
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        if(tableView == myDetailsTableView)
        {
        var footerView : UIView?
        footerView = UIView(frame: CGRectMake(14, 0, tableView.frame.size.width, 200))
        footerView?.backgroundColor = UIColor.whiteColor()
        
        let image = UIImage(named: "signature") as UIImage?
        let signbutton = UIButton(type: UIButtonType.Custom) as UIButton
        signbutton.frame = CGRectMake((tableView.frame.size.width/2) - 30 , 20, 60, 60)
        //signbutton.setImage(image, forState: .Normal)
        signbutton.setBackgroundImage(image, forState: .Normal)
        signbutton.addTarget(self, action: "signatureButtonTapped:", forControlEvents: .TouchUpInside)
        
        let signatureLbl = UILabel()
        signatureLbl.frame = CGRectMake((tableView.frame.size.width/2) - 40, 90, 100, 30)
        signatureLbl.text = "Signature"
        //signatureLbl.text.
        signatureLbl.textColor = UIColor(red: 23/255.0, green: 54/255.0, blue: 93/255.0, alpha: 1.0)
        
        let enrollBtn = UIButton(type: UIButtonType.Custom) as UIButton
        let deviceType = UIDevice.currentDevice().model
        if (deviceType == "iPhone") {
            //print("iphone")
            enrollBtn.frame = CGRectMake(20,150, 130, 45)

        }
        else{
            //print("ipad")
            enrollBtn.frame = CGRectMake((tableView.frame.size.width/2)-150,150, 130, 45)

        }
        enrollBtn.layer.cornerRadius = 5
        enrollBtn.clipsToBounds = true

        enrollBtn.setTitle("Enroll", forState: .Normal)
        enrollBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        enrollBtn.backgroundColor = UIColor(red: 100/255.0, green: 149/255.0, blue: 237/255.0, alpha: 1.0)
        enrollBtn.addTarget(self, action: "enrollButtonTapped:", forControlEvents: .TouchUpInside)
        
        let cancelBtn = UIButton(type: UIButtonType.Custom) as UIButton
        if (deviceType == "iPhone") {
            //print("iphone")
            cancelBtn.frame = CGRectMake(tableView.frame.size.width/2 + 20,150, 130, 45)
        }
        else{
            //print("ipad")
            cancelBtn.frame = CGRectMake(tableView.frame.size.width/2 + 20,150, 130, 45)
            
        }
        cancelBtn.layer.cornerRadius = 5
        cancelBtn.clipsToBounds = true
        cancelBtn.setTitle("Cancel", forState: .Normal)
        cancelBtn.setTitleColor(UIColor.grayColor(), forState: .Normal)
        cancelBtn.backgroundColor = UIColor(red: 229/255.0, green: 229/255.0, blue: 229/255.0, alpha: 1.0)
        cancelBtn.addTarget(self, action: "cancelButtonTapped:", forControlEvents: .TouchUpInside)
        footerView?.addSubview(signbutton)
        footerView?.addSubview(signatureLbl)
        footerView?.addSubview(enrollBtn)
        footerView?.addSubview(cancelBtn)
        return footerView
    }
        else
        {
            return nil
        }
    }
    
     func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
     {
        if(tableView == myDetailsTableView)
        {
        return 200.0
        }
        else
        {
            return 0.0
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let newIndexPath = indexPath.row
        if(tableView != myDetailsTableView)
        {
            //let btn_obj =
            scrollVal = ""
            ////print(btn_obj)
            popup?.dismiss(true)
            switch tagOfButton
            {
                case 1:
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
                //let picker_row = pickerView.selectedRowInComponent(0)
                genderOfPerson = sexArray.objectAtIndex(newIndexPath) as! String
                scrollVal = genderOfPerson
                cell.sexButton.setTitle("  \(genderOfPerson)", forState: .Normal)
                cell.sexButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
                break
                case 2:
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
                //let picker_row = pickerView.selectedRowInComponent(0)
                heightOfPerson = sexArray.objectAtIndex(newIndexPath) as! String
                scrollVal = heightOfPerson
                height = false
                cell.heightButton.setTitle("  \(heightOfPerson)", forState: .Normal)
                cell.heightButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
                break
                case 3:
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
                //let picker_row = pickerView.selectedRowInComponent(0)
                weightOfPerson = sexArray.objectAtIndex(newIndexPath) as! String
                scrollVal = weightOfPerson
                height = false
                cell.weightButton.setTitle("  \(weightOfPerson)", forState: .Normal)
                cell.weightButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
                break
                case 4:
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
                //let picker_row = pickerView.selectedRowInComponent(0)
                raceOfPerson = sexArray.objectAtIndex(newIndexPath) as! String
                scrollVal = raceOfPerson
                cell.raceButton.setTitle("  \(raceOfPerson)", forState: .Normal)
                cell.raceButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
                break
                
                case 5:
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
                //let picker_row = pickerView.selectedRowInComponent(0)
                eyeOfPerson = sexArray.objectAtIndex(newIndexPath) as! String
                scrollVal = eyeOfPerson
                cell.eyeButton.setTitle("  \(eyeOfPerson)", forState: .Normal)
                cell.eyeButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
                break
                case 6:
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
                //let picker_row = pickerView.selectedRowInComponent(0)
                hairOfPerson = sexArray.objectAtIndex(newIndexPath) as! String
                scrollVal = hairOfPerson
                cell.hairButton.setTitle("  \(hairOfPerson)", forState: .Normal)
                cell.hairButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
                break
                case 7:
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
                //let picker_row = pickerView.selectedRowInComponent(0)
                countryOfPerson = sexArray.objectAtIndex(newIndexPath) as! String
                scrollVal = countryOfPerson
//                if let index = find(items, groupNoToScroll)
//                {
//                    let indexPath = NSIndexPath(forRow: index, inSection: 0)
//                    tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
//                }
                cell.countryOfBirthButton.setTitle(countryOfPerson, forState: .Normal)
                cell.countryOfBirthButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
                break
                case 8:
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
                //let picker_row = pickerView.selectedRowInComponent(0)
                citizenshipOfPerson = sexArray.objectAtIndex(newIndexPath) as! String
                scrollVal = citizenshipOfPerson
                cell.citizenshipButton.setTitle(citizenshipOfPerson, forState: .Normal)
                cell.citizenshipButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
                break
                
                default:
                break
            }
        }
    }
    
    func sexButtonClicked(sender : UIButton)
    {
        view.endEditing(true)
        sexArray = NSMutableArray()
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
        //cell.PickerDetails.hidden = false
        alertview = NSBundle.mainBundle().loadNibNamed("pickerView", owner: self, options: nil)[0] as! UIView
       // alertview.backgroundColor = UIColor.whiteColor()
        let count = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(3).objectForKey("Option")?.count)
       // sexArray = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(3).objectForKey("Option"))! as! NSArray
        for i in 0...count!-1
        {
            sexArray.addObject((xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(3).objectForKey("Option")!.objectAtIndex(i).objectForKey("text"))! as! String)
        }
        sexArray.removeObjectAtIndex(0)
        sexArray.sortUsingSelector("localizedCaseInsensitiveCompare:")
        let ind = sexArray.indexOfObject("Unknown")
        sexArray.removeObjectAtIndex(ind)
        sexArray.insertObject("Unknown", atIndex: 0)
        //print(sexArray)
        smallTableView = (alertview.viewWithTag(1)! as! UITableView)
        smallTableView.tableFooterView = UIView()
        tagOfButton = sender.tag
        alertview.frame.size.height = CGFloat(sexArray.count) * 40.0
        //let submit = (alertview.viewWithTag(2)! as! UIButton)
        //submit.addTarget(self, action: #selector(pickerClicked(_:)), forControlEvents: .TouchUpInside)
        //submit.accessibilityIdentifier = String(sender.tag)
        popup = KLCPopup(contentView: alertview , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        //popup?.
        popup!.show()
    }
    func raceButtonClicked(sender : UIButton)
    {
        view.endEditing(true)
        sexArray = NSMutableArray()
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
        alertview = NSBundle.mainBundle().loadNibNamed("pickerView", owner: self, options: nil)[0] as! UIView
         let count = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(4).objectForKey("Option")?.count)
        for i in 0...count!-1 {
            sexArray.addObject((xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(4).objectForKey("Option")!.objectAtIndex(i).objectForKey("text"))! as! String)
        }
        sexArray.removeObjectAtIndex(0)
        sexArray.sortUsingSelector("localizedCaseInsensitiveCompare:")
        let ind = sexArray.indexOfObject("Unknown")
        sexArray.removeObjectAtIndex(ind)
        sexArray.insertObject("Unknown", atIndex: 0)
        //print(sexArray)
        smallTableView = (alertview.viewWithTag(1)! as! UITableView)
        smallTableView.tableFooterView = UIView()
        tagOfButton = sender.tag
        alertview.frame.size.height = CGFloat(sexArray.count) * 40.0
        //pickerView = (alertview.viewWithTag(1)! as! UIPickerView)
        //let submit = (alertview.viewWithTag(2)! as! UIButton)
        //submit.addTarget(self, action: #selector(pickerClicked(_:)), forControlEvents: .TouchUpInside)
        //submit.accessibilityIdentifier = String(sender.tag)
        popup = KLCPopup(contentView: alertview , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        popup!.show()
        
    }

    func eyeButtonClicked(sender : UIButton)
    {
        view.endEditing(true)
        sexArray = NSMutableArray()
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
        //cell.PickerDetails.hidden = false
        alertview = NSBundle.mainBundle().loadNibNamed("pickerView", owner: self, options: nil)[0] as! UIView
        // alertview.backgroundColor = UIColor.whiteColor()
        let count = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(2).objectForKey("Option")?.count)
        for i in 0...count!-1 {
            sexArray.addObject((xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(2).objectForKey("Option")!.objectAtIndex(i).objectForKey("text"))! as! String)
        }
        sexArray.removeObjectAtIndex(0)
        sexArray.sortUsingSelector("localizedCaseInsensitiveCompare:")
        let ind = sexArray.indexOfObject("Unknown")
        sexArray.removeObjectAtIndex(ind)
        sexArray.insertObject("Unknown", atIndex: 0)
        //pickerView = (alertview.viewWithTag(1)! as! UIPickerView)
        //let submit = (alertview.viewWithTag(2)! as! UIButton)
        //submit.addTarget(self, action: #selector(pickerClicked(_:)), forControlEvents: .TouchUpInside)
        //submit.accessibilityIdentifier = String(sender.tag)
        smallTableView = (alertview.viewWithTag(1)! as! UITableView)
        tagOfButton = sender.tag
        alertview.frame.size.height = CGFloat(sexArray.count) * 40.0
        popup = KLCPopup(contentView: alertview , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
         popup!.show()

    }
    func hairButtonClicked(sender : UIButton)
    {
        view.endEditing(true)
        sexArray = NSMutableArray()
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
        //cell.PickerDetails.hidden = false
        alertview = NSBundle.mainBundle().loadNibNamed("pickerView", owner: self, options: nil)[0] as! UIView
        // alertview.backgroundColor = UIColor.whiteColor()
        let count = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(1).objectForKey("Option")?.count)
        for i in 0...count!-1 {
            sexArray.addObject((xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(1).objectForKey("Option")!.objectAtIndex(i).objectForKey("text"))! as! String)
        }
        sexArray.removeObjectAtIndex(0)
        sexArray.sortUsingSelector("localizedCaseInsensitiveCompare:")
        let ind = sexArray.indexOfObject("Unknown")
        sexArray.removeObjectAtIndex(ind)
        sexArray.insertObject("Unknown", atIndex: 0)
        //pickerView = (alertview.viewWithTag(1)! as! UIPickerView)
        //let submit = (alertview.viewWithTag(2)! as! UIButton)
        //submit.addTarget(self, action: #selector(pickerClicked(_:)), forControlEvents: .TouchUpInside)
        //submit.accessibilityIdentifier = String(sender.tag)
        //        msg.text = "Please enter password!
        tagOfButton = sender.tag
        smallTableView = (alertview.viewWithTag(1)! as! UITableView)
        alertview.frame.size.height = CGFloat(sexArray.count) * 40.0
        popup = KLCPopup(contentView: alertview , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        popup!.show()
    }
    
    func countryButtonClicked(sender : UIButton)
    {
        scrollVal = countryOfPerson
        view.endEditing(true)
        sexArray = NSMutableArray()
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
        //cell.PickerDetails.hidden = false
        alertview = NSBundle.mainBundle().loadNibNamed("pickerView", owner: self, options: nil)[0] as! UIView
        // alertview.backgroundColor = UIColor.whiteColor()
        let count = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(0).objectForKey("Option")?.count)
        for i in 0...count!-1 {
            sexArray.addObject((xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(0).objectForKey("Option")!.objectAtIndex(i).objectForKey("text"))! as! String)
        }
        //sexArray = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(0).objectForKey("Option"))! as! NSArray
        //pickerView.reloadAllComponents()
        sexArray.removeObjectAtIndex(0)
        sexArray.sortUsingSelector("localizedCaseInsensitiveCompare:")
        sexArray.insertObject("Unknown", atIndex: 0)
        //pickerView = (alertview.viewWithTag(1)! as! UIPickerView)
        //let submit = (alertview.viewWithTag(2)! as! UIButton)
        //submit.addTarget(self, action: #selector(pickerClicked(_:)), forControlEvents: .TouchUpInside)
        //submit.accessibilityIdentifier = String(sender.tag)
        toScroll = true
        tagOfButton = sender.tag
        smallTableView = (alertview.viewWithTag(1)! as! UITableView)
        popup = KLCPopup(contentView: alertview , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        popup!.show()
    }
    
    func citizenshipButtonClicked(sender : UIButton)
    {
          scrollVal = citizenshipOfPerson
        view.endEditing(true)
        sexArray = NSMutableArray()
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
        alertview = NSBundle.mainBundle().loadNibNamed("pickerView", owner: self, options: nil)[0] as! UIView
        // alertview.backgroundColor = UIColor.whiteColor()
        let count = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(0).objectForKey("Option")?.count)
        for i in 0...count!-1 {
            sexArray.addObject((xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(0).objectForKey("Option")!.objectAtIndex(i).objectForKey("text"))! as! String)
        }
        //sexArray = (xml_dict.objectForKey("ApplicationSettings")?.objectForKey("Options")?.objectAtIndex(0).objectForKey("Option"))! as! NSArray
        //pickerView.reloadAllComponents()
        sexArray.removeObjectAtIndex(0)
        sexArray.sortUsingSelector("localizedCaseInsensitiveCompare:")
        sexArray.insertObject("Unknown", atIndex: 0)
        //pickerView = (alertview.viewWithTag(1)! as! UIPickerView)
        //let submit = (alertview.viewWithTag(2)! as! UIButton)
        //submit.addTarget(self, action: #selector(pickerClicked(_:)), forControlEvents: .TouchUpInside)
        //submit.accessibilityIdentifier = String(sender.tag)
        toScroll = true
        tagOfButton = sender.tag
        smallTableView = (alertview.viewWithTag(1)! as! UITableView)
        popup = KLCPopup(contentView: alertview , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        popup!.show()

    }
    
    func heightButtonClicked(sender : UIButton){
        scrollVal = heightOfPerson
        view.endEditing(true)
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
        alertview = NSBundle.mainBundle().loadNibNamed("pickerView", owner: self, options: nil)[0] as! UIView
        sexArray = ["Unknown","3'0\"","3'1\"","3'2\"","3'3\"","3'4\"","3'5\"","3'6\"","3'7\"","3'8\"","3'9\"","3'10\"","3'11\"","4'0\"","4'1\"","4'2\"","4'3\"","4'4\"","4'5\"","4'6\"","4'7\"","4'8\"","4'9\"","4'10\"","4'11\"","5'0\"","5'1\"","5'2\"","5'3\"","5'4\"","5'5\"","5'6\"","5'7\"","5'8\"","5'9\"","5'10\"","5'11\"","6'0\"","6'1\"","6'2\"","6'3\"","6'4\"","6'5\"","6'6\"","6'7\"","6'8\"","6'9\"","6'10\"","6'11\"","7'0\"","7'1\"","7'2\"","7'3\"","7'4\"","7'5\"",]
        height = true
        //pickerView = (alertview.viewWithTag(1)! as! UIPickerView)
        //let submit = (alertview.viewWithTag(2)! as! UIButton)
        //submit.addTarget(self, action: #selector(pickerClicked(_:)), forControlEvents: .TouchUpInside)
        //        msg.text = "Please enter password!
        //submit.accessibilityIdentifier = String(sender.tag)
        toScroll = true
        tagOfButton = sender.tag
        smallTableView = (alertview.viewWithTag(1)! as! UITableView)
        popup = KLCPopup(contentView: alertview , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        
        popup!.show()
        //height = false

    }
    func weightButtonClicked(sender : UIButton){
        scrollVal = weightOfPerson
        view.endEditing(true)
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
        alertview = NSBundle.mainBundle().loadNibNamed("pickerView", owner: self, options: nil)[0] as! UIView
        sexArray = ["Unknown","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108","109","110","111","112","113","114","115","116","117","118","119","120","121","122","123","124","125","126","127","128","129","130","131","132","133","134","135","136","137","138","139","140","141","142","143","144","145","146","147","148","149","150","151","153","154","155","156","157","158","159","160","161","162","163","164","165","166","167","168","169","170","171","172","173","174","175","176","177","178","179","180","181","182","183","184","185","186","187","188","189","190","191","192","193","194","195","196","197","198","199","200","201","202","203","204","205","206","207","208","209","210","211","212","213","214","215","216","217","218","219","220","221","222","223","224","225","226","227","228","229","230","231","232","233","234","235","236","237","238","239","240","241","242","243","244","245","246","247","248","249","250","251","253","254","255","256","257","258","259","260","261","262","263","264","265","266","267","268","269","270","271","272","273","274","275","276","277","278","279","280","281","282","283","284","285","286","287","288","289","290","291","292","293","294","295","296","297","298","299","300"]
        height = true
        //pickerView = (alertview.viewWithTag(1)! as! UIPickerView)
        //let submit = (alertview.viewWithTag(2)! as! UIButton)
        //submit.addTarget(self, action: #selector(pickerClicked(_:)), forControlEvents: .TouchUpInside)
        //        msg.text = "Please enter password!
        //submit.accessibilityIdentifier = String(sender.tag)
        toScroll = true
        tagOfButton = sender.tag
        smallTableView = (alertview.viewWithTag(1)! as! UITableView)
        popup = KLCPopup(contentView: alertview , showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        
        popup!.show()
    }
    
//    func pickerClicked(sender:AnyObject)
//    {
//        let btn_obj = Int(sender.accessibilityIdentifier!!)!
//        popup?.dismiss(true)
//        
//        switch btn_obj
//        {
//        case 1:
//            let indexPath = NSIndexPath(forRow: 0, inSection: 0)
//            let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
//            let picker_row = pickerView.selectedRowInComponent(0)
////             genderOfPerson = sexArray.objectAtIndex(picker_row).objectForKey("text") as! String
//             genderOfPerson = sexArray.objectAtIndex(picker_row) as! String
//            cell.sexButton.setTitle("  \(genderOfPerson)", forState: .Normal)
//            cell.sexButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
//            break
//        case 3:
//            let indexPath = NSIndexPath(forRow: 0, inSection: 0)
//            let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
//            let picker_row = pickerView.selectedRowInComponent(0)
//            weightOfPerson = sexArray.objectAtIndex(picker_row) as! String
//            height = false
//            cell.weightButton.setTitle("  \(weightOfPerson)", forState: .Normal)
//            cell.weightButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
//            break
//        case 4:
//            let indexPath = NSIndexPath(forRow: 0, inSection: 0)
//            let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
//            let picker_row = pickerView.selectedRowInComponent(0)
//            raceOfPerson = sexArray.objectAtIndex(picker_row) as! String
//            cell.raceButton.setTitle("  \(raceOfPerson)", forState: .Normal)
//            cell.raceButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
//            break
//            
//        case 5:
//            let indexPath = NSIndexPath(forRow: 0, inSection: 0)
//            let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
//            let picker_row = pickerView.selectedRowInComponent(0)
//            eyeOfPerson = sexArray.objectAtIndex(picker_row) as! String
//            cell.eyeButton.setTitle("  \(eyeOfPerson)", forState: .Normal)
//            cell.eyeButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
//            break
//        case 6:
//            let indexPath = NSIndexPath(forRow: 0, inSection: 0)
//            let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
//            let picker_row = pickerView.selectedRowInComponent(0)
//            hairOfPerson = sexArray.objectAtIndex(picker_row) as! String
//            cell.hairButton.setTitle("  \(hairOfPerson)", forState: .Normal)
//            cell.hairButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
//            break
//        case 7:
//            let indexPath = NSIndexPath(forRow: 0, inSection: 0)
//            let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
//            let picker_row = pickerView.selectedRowInComponent(0)
//            countryOfPerson = sexArray.objectAtIndex(picker_row) as! String
//            cell.countryOfBirthButton.setTitle(countryOfPerson, forState: .Normal)
//            cell.countryOfBirthButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
//            break
//           }
    func dobButtonClicked(sender : UIButton) {
         view.endEditing(true)
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        
        let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
        let alertview = NSBundle.mainBundle().loadNibNamed("datePickerView", owner: self, options: nil)[0]
        dateView = (alertview.viewWithTag(1)! as! UIDatePicker)
        let submitDate = (alertview.viewWithTag(2)! as! UIButton)
        submitDate.addTarget(self, action: Selector("submitDateForDtPkr:"), forControlEvents: UIControlEvents.TouchUpInside)
        popup = KLCPopup(contentView: alertview as! UIView, showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        popup!.show()

    }
    func submitDateForDtPkr(sender : AnyObject){
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        
        let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
         popup?.dismiss(true)
                dateView.datePickerMode = UIDatePickerMode.Date
        self.dateView.maximumDate = NSDate(timeIntervalSinceNow: -86400)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dobOfPerson = dateFormatter.stringFromDate(dateView.date)
        //print(dobOfPerson)
        cell.birthdateTxt.text = dobOfPerson
        //cell.dobButton.setTitle(String(dobOfPerson), forState: .Normal)
        //cell.dobButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
        
    }
//    internal func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
//    {
//        return 1
//    }
//    
//    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
//    {
//        return self.sexArray.count
//    }
//    internal func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
//    {
//        var str : String = String()
//        if(height == true)
//            
//        {
//            str = sexArray[row] as! String
//            
//        }
//        else{
//        //str = sexArray.objectAtIndex(row).objectForKey("text") as! String
//         str = sexArray.objectAtIndex(row) as! String
//           // str = " "
//            //return str
//        }
//        
//        return str
//    }
//    
//    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
//        let label = UILabel()
//        label.font = UIFont(name: "System", size: 15)
//        label.textAlignment = NSTextAlignment.Center
////        label.lineBreakMode = .ByWordWrapping;
////        label.numberOfLines = 0;
//        label.text = sexArray.objectAtIndex(row) as? String
////        label.sizeToFit()
//        return label
//    }
//    
//    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
//        let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
//        cell.sexButton.setTitle(sexArray[row], forState: .Normal)
//        cell.sexButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
//        //cell.PickerDetails.hidden = true
//    }
//    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
//        return 40.0
//    }
    
    func cancelButtonTapped(sender:UIButton!)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func signatureButtonTapped(sender:UIButton!)
    {
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
        nameOfPerson = cell.firstName.text!
        //print(nameOfPerson)
        sirnameOfPerson = cell.lastName.text!
        //print(sirnameOfPerson)
        if(heightOfPerson != "Unknown"){
        var fullArr = heightOfPerson.componentsSeparatedByString("'")
        let firstDigit = fullArr[0]
        let second = fullArr[1]
        var secondArr = second.componentsSeparatedByString("\"")
        let secondDigit = secondArr[0]
        heightOfPerson = firstDigit+"0"+secondDigit
        }
        dobOfPerson = cell.birthdateTxt.text!
        let dtVal = dateValidator(dobOfPerson)
        if(dtVal == true || dobOfPerson.isEmpty || dobOfPerson == "?") {
            if(dobOfPerson.isEmpty){
                if(nameOfPerson.isEmpty){
                    nameOfPerson = "?"
                }
                if(sirnameOfPerson.isEmpty){
                    sirnameOfPerson = "?"
                }
                if(dobOfPerson.isEmpty){
                    dobOfPerson = "?"
                }
                let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
               // appdelegate.shouldSupportAllOrientation = true
                appdelegate.shouldSupportAllOrientation = true
                self.performSegueWithIdentifier("signature", sender: nil)
            }
            else
            {
            let datemaxVal = calcAge(dobOfPerson)
            ////print(datemaxVal)
            if(datemaxVal <= 0){
                let Cust = CustomClass()
                var alert = UIAlertController( )
                alert = Cust.displayMyAlertMessage("Invalid Date Selection")
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
            if(nameOfPerson.isEmpty){
                nameOfPerson = "?"
            }
            if(sirnameOfPerson.isEmpty){
                sirnameOfPerson = "?"
            }
            if(dobOfPerson.isEmpty){
                dobOfPerson = "?"
            }
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appdelegate.shouldSupportAllOrientation = true
        self.performSegueWithIdentifier("signature", sender: nil)
                }
            }
        }
        else
        {
            let Cust = CustomClass()
            var alert = UIAlertController( )
            alert = Cust.displayMyAlertMessage("Enter valid date Format")
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func calcAge (birthday: String) -> NSInteger
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat =  "MM/dd/yyyy"
        let dateToSave =  dateFormatter.dateFromString(birthday)
        let age = NSCalendar.currentCalendar().components(.Year, fromDate: dateToSave!, toDate: NSDate(), options: []).year
        if(age == 0){
            //return age
            let ageMonth = NSCalendar.currentCalendar().components(.Month, fromDate: dateToSave!, toDate: NSDate(), options: []).month
            if (ageMonth == 0) {
                let ageDay = NSCalendar.currentCalendar().components(.Day, fromDate: dateToSave!, toDate: NSDate(), options: []).day
                return ageDay
            }
            else{
                return ageMonth
            }
        }else
        {
            return age
        }
    }
//    func calculateAgeInMonths (birthday: String) -> NSInteger
//    {
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat =  "MM/dd/yyyy"
//        let dateToSave =  dateFormatter.dateFromString(birthday)
//        return NSCalendar.currentCalendar().components(.Month, fromDate: dateToSave!, toDate: NSDate(), options: []).month
//    }
//    func calculateAgeInDays (birthday: String) -> NSInteger
//    {
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat =  "MM/dd/yyyy"
//        let dateToSave =  dateFormatter.dateFromString(birthday)
//        return NSCalendar.currentCalendar().components(.Day, fromDate: dateToSave!, toDate: NSDate(), options: []).day
//    }
    
    func dateValidator(dateFromUser: String) -> Bool {
        let dateFormat =  NSDateFormatter()
        dateFormat.dateFormat = "MM/dd/yyyy"
        let date : NSDate? = dateFormat.dateFromString(dateFromUser)
        if date != nil
        {
            //print("valid")
            return true
        }
        else
        {
            //print("invalid")
            return false
        }
    }
    
    func recheablityCheck()-> Bool
    {
        let net = NetworkReachabilityManager()
        net?.startListening()
        
        if  net?.isReachable ?? false
        {
            
            if ((net?.isReachableOnEthernetOrWiFi) != nil)
            {
                return true
            }
            else if(net?.isReachableOnWWAN)!
            {
                return true
            }
        }
        else
        {
            //// print("no connection")
            return false
        }
        return false
    }

    
    func enrollButtonTapped(sender:UIButton!)
    {
        if recheablityCheck() == true
        {
            enrollWithNoErrors()
        }
        else
        {
            let myAlert = UIAlertController(title: "Internet connection FAILED", message: "Your device is not connected to internet", preferredStyle: UIAlertControllerStyle.Alert);
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil);
            myAlert.addAction(okAction)
            self.presentViewController(myAlert, animated: true, completion: nil)
        }

            }
    
    func enrollWithNoErrors(){
        //print(genderOfPerson)
        //print(heightOfPerson)
        //print(weightOfPerson)
        //print(eyeOfPerson)
        //print(hairOfPerson)
        //print(countryOfPerson)
        //print(citizenshipOfPerson)
        //print(heightOfPerson)
        if(heightOfPerson != "Unknown" && heightOfPerson.characters.contains("'")){
        var fullArr = heightOfPerson.componentsSeparatedByString("'")
        let firstDigit = fullArr[0]
        let second = fullArr[1]
        var secondArr = second.componentsSeparatedByString("\"")
        let secondDigit = secondArr[0]
        heightOfPerson = firstDigit+"0"+secondDigit
        //print(heightOfPerson)
        }
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let cell = myDetailsTableView.cellForRowAtIndexPath(indexPath) as! EnrollDetailsTableViewCell!
        nameOfPerson = cell.firstName.text!
        //print(nameOfPerson)
        sirnameOfPerson = cell.lastName.text!
        //print(sirnameOfPerson)
        //print(raceOfPerson)
        dobOfPerson = cell.birthdateTxt.text!
        let dtVal = dateValidator(dobOfPerson)
//        let dateMax = calculateAge(dobOfPerson)
//        var dateValueToTest = true
//        if(dateMax < 0){
//            dateValueToTest = false
//        }else if(dateMax == 0){
//            
//        }
        
        if(dtVal == true || dobOfPerson.isEmpty) {
            if(dobOfPerson.isEmpty){
                if(nameOfPerson.isEmpty){
                    nameOfPerson = "?"
                }
                if(sirnameOfPerson.isEmpty){
                    sirnameOfPerson = "?"
                }
                if(dobOfPerson.isEmpty){
                    dobOfPerson = "?"
                }
                appdelegate!.showHUD(self.view)
                let dispatchQueue : dispatch_queue_t = dispatch_queue_create("dispatchQueue", nil)
                dispatch_async(dispatchQueue) {
                    self.enrollUserWithDetails()
                }

            }
            else {
            let datemaxVal = calcAge(dobOfPerson)
            //print(datemaxVal)
            if(datemaxVal <= 0){
                let Cust = CustomClass()
                var alert = UIAlertController( )
                alert = Cust.displayMyAlertMessage("Invalid Date Selection")
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
            if(nameOfPerson.isEmpty){
                nameOfPerson = "?"
            }
            if(sirnameOfPerson.isEmpty){
                sirnameOfPerson = "?"
            }
            if(dobOfPerson.isEmpty){
                dobOfPerson = "?"
            }
             appdelegate!.showHUD(self.view)
             let dispatchQueue : dispatch_queue_t = dispatch_queue_create("dispatchQueue", nil)
            dispatch_async(dispatchQueue) {
                self.enrollUserWithDetails()
            }
                }
            }
        }
        else{
            let Cust = CustomClass()
            var alert = UIAlertController( )
            alert = Cust.displayMyAlertMessage("Enter valid date Format")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
    }
    
    func enrollUserWithDetails(){
        var LatitudeDeg : String = String()
        var LatitudeMin : String = String()
        var LatitudeSec : String = String()
        var LongitudeDeg : String = String()
        var LongitudeMin : String = String()
        var LongitudeSec : String = String()
        if(LatitudeDegreeEn == 0 && LatitudeMinuteEn == 0 && LatitudeSecondEn == 0 && LongitudeDegreeEn == 0 && LongitudeMinuteEn == 0 && LongitudeSecondEn == 0){
            LatitudeDeg = "?"
            LatitudeMin = "?"
            LatitudeSec = "?"
            LongitudeDeg = "?"
            LongitudeMin = "?"
            LongitudeSec = "?"
        }else{
            LatitudeDeg = String(LatitudeDegreeEn)
            LatitudeMin = String(LatitudeMinuteEn)
            LatitudeSec = String(LatitudeSecondEn)
            LongitudeDeg = String(LongitudeDegreeEn)
            LongitudeMin = String(LongitudeMinuteEn)
            LongitudeSec = String(LongitudeSecondEn)
        }
        obj_WebService.sendResponse(img_strEn,firstName:nameOfPerson,lastName:sirnameOfPerson,dob:dobOfPerson,pob:countryOfPerson,citizenship:citizenshipOfPerson,gender:genderOfPerson,race:raceOfPerson ,height:heightOfPerson,weight:weightOfPerson,eye:eyeOfPerson,hair:hairOfPerson,LatitudeDegree:LatitudeDeg,LatitudeMinute:LatitudeMin,LatitudeSecond:LatitudeSec,LongitudeDegree:LongitudeDeg,LongitudeMinute:LongitudeMin,LongitudeSecond:LongitudeSec){ (block) in
            //print(block);
            self.appdelegate?.dismissView(self.view)
            if(block == nil){
                let Cust = CustomClass()
                var alert = UIAlertController()
                alert = Cust.displayMyAlertMessage("Not able to connect to Server")
                self.presentViewController(alert, animated: true, completion: nil)
                self.appdelegate?.dismissView(self.view)
            }
            else
            {
            let success = block!.objectForKey("s:Envelope")?.objectForKey("s:Body")?.objectForKey("ProcessEnrollResponse")?.objectForKey("ProcessEnrollResult")?.objectForKey("Code")?.objectForKey("text") as? String
            if(success == "enroll.success"){
//                let Cust = CustomClass()
//                var alert = UIAlertController()
//                alert = Cust.displayMyAlertMessage("Enroll request successfully processed")
//                self.presentViewController(alert, animated: true, completion: nil)
                let alertController = UIAlertController(title: "Success", message: "Enroll request successfully processed", preferredStyle: UIAlertControllerStyle.Alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
                    let destination = self.storyboard!.instantiateViewControllerWithIdentifier("searchViewController") as! searchViewController
                    self.navigationController!.pushViewController(destination, animated: true)

                })
                alertController.addAction(okAction)
                self.presentViewController(alertController, animated: true, completion:{})
            }
            else
            {
                let alertController = UIAlertController(title: "Fail", message: "Try again with another image?", preferredStyle: UIAlertControllerStyle.Alert)
                let somethingAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in //print("Yes")
                    let alertview = NSBundle.mainBundle().loadNibNamed("ChooseImageView", owner: self, options: nil)[0]
                    self.popup = KLCPopup(contentView: alertview as! UIView, showType: .BounceIn, dismissType: .BounceOut, maskType: .Dimmed, dismissOnBackgroundTouch: false, dismissOnContentTouch: false)
                    let camera = (alertview.viewWithTag(1)! as! UIButton)
                    camera.addTarget(self, action: #selector(self.cameraClicked(_:)), forControlEvents: .TouchUpInside)
                    let gallary = (alertview.viewWithTag(2)! as! UIButton)
                    gallary.addTarget(self, action: #selector(self.gallaryClicked(_:)), forControlEvents: .TouchUpInside)
                    let cancelImgSelection = (alertview.viewWithTag(3)! as! UIButton)
                    cancelImgSelection.addTarget(self, action: #selector(self.cancelImgSelectionClicked(_:)), forControlEvents: .TouchUpInside)
                    self.popup!.show()
                    
                })
                
                let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in //print("No")
                    self.navigationController?.popViewControllerAnimated(true)
                })
                
                alertController.addAction(somethingAction)
                alertController.addAction(cancelAction)
                self.presentViewController(alertController, animated: true, completion:{})
            }
        }
        }

    }
    // MARK: - cancel button

    func cancelImgSelectionClicked(sender:AnyObject){
         popup?.dismiss(true)
    }
    // MARK: - gallary button
    func gallaryClicked(sender:AnyObject){
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        presentViewController(picker!, animated: true, completion: nil)
        popup?.dismiss(true)
        }
    // MARK: - camera button
    func cameraClicked(sender:AnyObject){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerControllerSourceType.Camera
            picker!.cameraCaptureMode = .Photo
            presentViewController(picker!, animated: true, completion: nil)
            popup?.dismiss(true)
        }else{
             popup?.dismiss(true)
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .Alert)
            let ok = UIAlertAction(title: "OK", style:.Default, handler: nil)
            alert.addAction(ok)
            presentViewController(alert, animated: true, completion: nil)
        }

    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        var chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        //imageView.contentMode = .ScaleAspectFit
        //imageSelected = chosenImage
        chosenImage = fixOrientation(chosenImage)
        let dataComp = UIImageJPEGRepresentation(chosenImage, 1)
        let imageSize: Int = dataComp!.length
        let img = Double(imageSize)/1024.0
        //print(img)
        img_strEn = dataComp!.base64EncodedStringWithOptions([])
        imageSelected = UIImage(data: dataComp!)!
        if(img > 500){
            let dataComp = UIImageJPEGRepresentation(chosenImage, 0.5)
            img_strEn = dataComp!.base64EncodedStringWithOptions([])
            //let imageSize1: Int = dataComp1!.length
            //let img1 = Double(imageSize1)
            //print(img1 / 1024.0)
            imageSelected = UIImage(data: dataComp!)!
        }
        dismissViewControllerAnimated(true, completion:
            {
                //self.appdelegate!.showHUD(self.view)
//                self.img_strEn = self.encodeToBase64String(self.imageSelected)
                self.enrollWithNoErrors()
            }
        )
    }
    func fixOrientation(img:UIImage) -> UIImage {
        if (img.imageOrientation == UIImageOrientation.Up) {
            return img;
        }
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale);
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.drawInRect(rect)
        let normalizedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return normalizedImage;
    }
    
    func encodeToBase64String(image: UIImage) -> String {
        
        let imageData = UIImagePNGRepresentation(image)
        let imag_str:String = imageData!.base64EncodedStringWithOptions([])
        ////print(imag_str)
        return imag_str
    }
    
    
//    @IBAction func homeLongPress(sender: UILongPressGestureRecognizer) {
//        if (sender.state == .Began){
//            let destination = self.storyboard!.instantiateViewControllerWithIdentifier("openFileViewController") as! openFileViewController
//            self.navigationController!.pushViewController(destination, animated: true)
//        }
//    }
    
    @IBAction func homeButonTapped(sender: AnyObject) {
        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("homeScreenViewController") as! homeScreenViewController
        self.navigationController!.pushViewController(destination, animated: true)
    }
    @IBAction func searchButtonTapped(sender: AnyObject) {
        let destination = self.storyboard!.instantiateViewControllerWithIdentifier("searchViewController") as! searchViewController
        self.navigationController!.pushViewController(destination, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "signature"
        {
            let Sign_VC: signatureViewController = (segue.destinationViewController as! signatureViewController)
            Sign_VC.img_strSg = img_strEn
            Sign_VC.LongitudeDegreeSg = LongitudeDegreeEn
            Sign_VC.LatitudeDegreeSg = LatitudeDegreeEn
            Sign_VC.LongitudeMinuteSg = LongitudeMinuteEn
            Sign_VC.LatitudeMinuteSg = LatitudeMinuteEn
            Sign_VC.LongitudeSecondSg = LongitudeSecondEn
            Sign_VC.LatitudeSecondSg = LatitudeSecondEn
            Sign_VC.nameOfPersonSg = nameOfPerson
            Sign_VC.sirnameOfPersonSg = sirnameOfPerson
            Sign_VC.genderOfPersonSg = genderOfPerson
            Sign_VC.heightOfPersonSg = heightOfPerson
            Sign_VC.weightOfPersonSg = weightOfPerson
            Sign_VC.dobOfPersonSg = dobOfPerson
            Sign_VC.eyeOfPersonSg = eyeOfPerson
            Sign_VC.hairOfPersonSg = hairOfPerson
            Sign_VC.countryOfPersonSg = countryOfPerson
            Sign_VC.citizenshipOfPersonSg = citizenshipOfPerson
            Sign_VC.raceOfPersonSg = raceOfPerson
        }
    }    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
