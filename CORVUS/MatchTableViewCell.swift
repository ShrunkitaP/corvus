//
//  MatchTableViewCell.swift
//  CORVUS
//
//  Created by Riken Shah on 29/08/16.
//  Copyright © 2016 HPL. All rights reserved.
//

import UIKit

class MatchTableViewCell: UITableViewCell {

    @IBOutlet weak var signImage: UIImageView!
    @IBOutlet weak var dtSeenLabel: UILabel!
    @IBOutlet weak var perfectAge: UILabel!
    @IBOutlet weak var matchImage: UIImageView!
    @IBOutlet weak var searchImage: UIImageView!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var eyeLabel: UILabel!
    @IBOutlet weak var hairLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var citizenLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    //@IBOutlet weak var raceLabel: UILabel!
    @IBOutlet weak var gpsBtn: UIButton!
    @IBOutlet weak var mailButton: UIButton!
    @IBOutlet weak var dateSearchedLbl: UILabel!
    @IBOutlet weak var gpsSearched: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
