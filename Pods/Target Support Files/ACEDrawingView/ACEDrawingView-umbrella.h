#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "ACEDrawingTools.h"
#import "ACEDrawingView.h"

FOUNDATION_EXPORT double ACEDrawingViewVersionNumber;
FOUNDATION_EXPORT const unsigned char ACEDrawingViewVersionString[];

